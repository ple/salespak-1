package com.softpak.salespak.configuration;

import com.bugsnag.Bugsnag;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BugsnagConfig {

    @Value("${bugsnag.token}")
    String token;

    @Value("${bugsnag.releaseStage}")
    String releaseStage;

    @Bean
    public Bugsnag bugsnag() {
        Bugsnag bugsnag = new Bugsnag(token);
        bugsnag.setReleaseStage(releaseStage);
        bugsnag.setNotifyReleaseStages("stage", "production");

        return bugsnag;
    }
}
