package com.softpak.salespak.logging;

import com.softpak.salespak.configuration.BugsnagConfig;
import com.softpak.salespak.security.SoftPakDataPrinciple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component("BugsnagErrorLogging")
public class BugsnagErrorLogging implements AbstractErrorLogging {

    @Autowired
    BugsnagConfig bugsnagConfig;

    public void handleLogging(Throwable t) {

        SoftPakDataPrinciple softPakDataPrinciple = (SoftPakDataPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        bugsnagConfig.bugsnag().addCallback(report -> {
            report.setUserName(softPakDataPrinciple.getUsername());
            report.addToTab("User", "dataset", softPakDataPrinciple.getDataset());
        });
        bugsnagConfig.bugsnag().notify(t);
    }
}
