package com.softpak.salespak.logging;

public interface AbstractErrorLogging {
    void handleLogging(Throwable t);
}
