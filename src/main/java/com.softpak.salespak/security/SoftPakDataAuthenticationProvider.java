package com.softpak.salespak.security;

import com.softpak.access.data.SecurityInfo;
import com.softpak.salespak.data.SecurityDataManager;
import com.softpak.salespak.logging.AbstractErrorLogging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.ArrayList;

@Component
public class SoftPakDataAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    SecurityDataManager securityDataManager;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Resource(name = "${config.errorLogger}")
    private AbstractErrorLogging errorLogging;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        SoftPakDataPrinciple softPakDataPrinciple = new SoftPakDataPrinciple();
        softPakDataPrinciple.setUsername(authentication.getName().toUpperCase());
        softPakDataPrinciple.setPassword(authentication.getCredentials().toString());

        String dataset = securityDataManager.dataset(softPakDataPrinciple.getUsername(), softPakDataPrinciple.getPassword());

        if (dataset == null || dataset.trim().equals("")) {
            throw  new DisabledException("Unable to retrieve account information. Please contact support for assistance.");
        }

        try {
            softPakDataPrinciple.setDataset(dataset);
            SecurityDataManager.SecurityDataApi securityDataApi = securityDataManager.connect(softPakDataPrinciple);

            SecurityInfo securityInfo = securityDataApi.getUserSecurity(softPakDataPrinciple.getUsername());
            securityDataApi.disconnect();

            if (!securityInfo.isAllowSalesPakLogon() || !securityInfo.hasSalesPakModule()) {
                AuthenticationException error = new DisabledException("Unauthorized. Please contact support for assistance.");
                throw  error;
            }

            return new UsernamePasswordAuthenticationToken(softPakDataPrinciple, softPakDataPrinciple.getPassword(), new ArrayList<>());
        } catch (Exception e) {
            errorLogging.handleLogging(e);
            throw  new DisabledException("Please contact support for assistance.");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
