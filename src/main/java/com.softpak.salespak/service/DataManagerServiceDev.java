package com.softpak.salespak.service;

import com.softpak.access.AbstractDataAPI;
import com.softpak.salespak.security.SoftPakDataPrinciple;
import org.springframework.stereotype.Component;

@Component("Dev")
public class DataManagerServiceDev extends DataManagerService {

    public AbstractDataAPI connectApi(AbstractDataAPI api, SoftPakDataPrinciple user) {
        try {
            if (api.connect(this.system, user.getDataset(), this.user, this.pass))
                return api;

        } catch (Exception e) {
            e.printStackTrace();
            errorLogging.handleLogging(e);
        }

        return null;
    }
}
