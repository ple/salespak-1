package com.softpak.salespak.service;

import com.softpak.salespak.logging.AbstractErrorLogging;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Component
public class UploadService {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Resource(name = "${config.errorLogger}")
    private AbstractErrorLogging errorLogging;

    public static String randomHash(String name) {
        return UUID.randomUUID().toString().replace("-", "") + getFileExtension(name);
    }

    public static String getFileExtension(String name) {
        return name.substring(name.lastIndexOf("."));
    }

    public boolean uploadFile( MultipartFile file, String pathName, String fileName) {
        try {
            byte[] bytes = file.getBytes();
            Path directory = Paths.get(pathName);

            if (Files.notExists(directory)) {
                Files.createDirectories(directory);
            }

            Path path = Paths.get(pathName + "\\" + fileName);
            Files.write(path, bytes);
            return true;

        } catch (IOException e) {
            errorLogging.handleLogging(e);
            return false;
        }
    }

    public boolean uploadFile(InputStream in, String pathName, String fileName) {
        try {
            Files.copy(in, Paths.get(pathName + "\\" + fileName), StandardCopyOption.REPLACE_EXISTING);
            return true;
        } catch (IOException e) {
            errorLogging.handleLogging(e);
            return false;
        }
    }
}
