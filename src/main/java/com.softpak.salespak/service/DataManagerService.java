package com.softpak.salespak.service;

import com.softpak.access.AbstractDataAPI;
import com.softpak.salespak.data.DataApi;
import com.softpak.salespak.logging.AbstractErrorLogging;
import com.softpak.salespak.security.SoftPakDataPrinciple;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Arrays;

public abstract class DataManagerService {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Resource(name = "${config.errorLogger}")
    AbstractErrorLogging errorLogging;

    @Value("${dataApi.system}")
    protected String system;
    @Value("${dataApi.user}")
    protected String user;
    @Value("${dataApi.pass}")
    protected String pass;

    public abstract AbstractDataAPI connectApi(AbstractDataAPI api, SoftPakDataPrinciple user);

    public Runnable disconnect(DataApi... apis) {
        return () -> {
            try {
                Arrays.stream(apis).forEach(DataApi::disconnect);
            } catch (Exception e) {
                e.printStackTrace();
                errorLogging.handleLogging(e);
            }
        };
    }

    public String getSystem() {
        return this.system;
    }

    public String getUser() {
        return this.user;
    }
}
