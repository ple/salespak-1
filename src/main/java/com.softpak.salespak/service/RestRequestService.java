package com.softpak.salespak.service;

import com.softpak.salespak.model.RestCall;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class RestRequestService {

    public RestCall get(RestCall restCall) {
        HttpEntity<String> requestEntity = new HttpEntity<>("", restCall.getHeaders());
        ResponseEntity<String> responseEntity = restCall.getRest().exchange(restCall.getUrl(), HttpMethod.GET, requestEntity, String.class);
        restCall.setResponseEntity(responseEntity);

        return restCall;
    }

    public RestCall post(RestCall restCall) {
        HttpEntity<String> requestEntity = new HttpEntity<>(restCall.getReqBody().toString(), restCall.getHeaders());

        ResponseEntity<String> responseEntity = restCall.getRest().exchange(
                restCall.getUrl(),
                HttpMethod.POST,
                requestEntity,
                String.class);

        restCall.setResponseEntity(responseEntity);

        return restCall;
    }
}
