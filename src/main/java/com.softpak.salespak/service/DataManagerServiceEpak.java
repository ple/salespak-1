package com.softpak.salespak.service;

import com.softpak.access.AbstractDataAPI;
import com.softpak.salespak.data.DataApi;
import com.softpak.salespak.logging.AbstractErrorLogging;
import com.softpak.salespak.security.SoftPakDataPrinciple;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.UUID;

@Component("Epak")
public class DataManagerServiceEpak extends DataManagerService {

    public AbstractDataAPI connectApi(AbstractDataAPI api, SoftPakDataPrinciple user) {
        try {
            if (api.connect(this.system, user.getDataset(), this.user + user.getDataset(), this.pass))
                return api;

        } catch (Exception e) {
            e.printStackTrace();
            errorLogging.handleLogging(e);
        }

        return null;
    }
}
