package com.softpak.salespak.data;

import com.softpak.access.CompanyAPI;
import com.softpak.access.data.CompanyInfo;
import com.softpak.salespak.security.SoftPakDataPrinciple;
import com.softpak.salespak.service.DataManagerService;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.util.ArrayList;

@Component
public class CompanyDataManager implements AbstractDataManager {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Resource(name = "${config.dataManager}")
    DataManagerService dataManagerService;

    public CompanyDataApi connect(SoftPakDataPrinciple user) {

        CompanyAPI api = (CompanyAPI) dataManagerService.connectApi(new CompanyAPI(), user);

        return new CompanyDataApi(api);
    }

    public class CompanyDataApi implements DataApi {

        private final CompanyAPI api;

        private CompanyDataApi(CompanyAPI api) {
            this.api = api;
        }

        public void disconnect() {
            this.api.disconnect();
        }

        public String toString() {
            return "CompanyDataApi";
        }

        public Mono<ArrayList<CompanyInfo>> getCompanyList() {
            return Mono.just(api.getCompanyList());
        }
    }
}

