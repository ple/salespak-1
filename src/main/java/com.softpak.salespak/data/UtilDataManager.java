package com.softpak.salespak.data;

import com.softpak.access.UtilAPI;
import com.softpak.access.data.Code;
import com.softpak.access.data.MiscVariable;
import com.softpak.salespak.security.SoftPakDataPrinciple;
import com.softpak.salespak.service.DataManagerService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class UtilDataManager implements AbstractDataManager {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Resource(name = "${config.dataManager}")
    DataManagerService dataManagerService;

    public UtilDataApi connect(SoftPakDataPrinciple user) {

        UtilAPI api = (UtilAPI) dataManagerService.connectApi(new UtilAPI(), user);

        return new UtilDataApi(api);
    }

    public class UtilDataApi implements DataApi {

        private final UtilAPI api;

        public String toString() {
            return "UtilDataApi";
        }

        private UtilDataApi(UtilAPI api) {
            this.api = api;
        }

        public void disconnect() {
            this.api.disconnect();
        }

        public Function<Code, JSONObject> jsonObjectMapper() {
            return Code::getJsonObject;
        }

        public Function<Code, JSONObject> labelValueMapper() {
            return code -> new JSONObject()
                    .put("label", code.getDescription())
                    .put("value", code.getType())
                    .put("numberOfPickups", code.getNumberOfPickups());
        }

        public Function<Code, JSONObject> labelCodeMapper() {
            return code -> new JSONObject()
                    .put("label", code.getDescription())
                    .put("value", code.getCode());
        }

        public Mono<JSONArray> listMiscellaneousVariables(String userId, String company, Integer customer, Integer uid, String miscVarType) {
            return Flux.fromIterable(api.listMiscellaneousVariables(userId, company, customer, uid, miscVarType))
                    .map(MiscVariable::getJsonObject)
                    .collect(Collectors.toList())
                    .map(JSONArray::new);
        }

        public Mono<JSONArray> listCodeTypes(String codeType, Function<Code, JSONObject> mapper) {
            return Flux.fromIterable(api.listCodeTypes(codeType))
                    .map(mapper)
                    .collect(Collectors.toList())
                    .map(JSONArray::new);
        }

        public Mono<JSONArray> listStatusCodes(String codeType, Function<Code, JSONObject> mapper) {
            return Flux.fromIterable(api.listStatusCodes(codeType))
                    .map(mapper)
                    .collect(Collectors.toList())
                    .map(JSONArray::new);
        }

        public Mono<JSONArray> listChargeCodes(String userId, String application, String company) {
            return Flux.fromIterable(api.listChargeCodes(application, company))
                    .map(chargeCode -> new JSONObject()
                            .put("label", chargeCode.getDescription())
                            .put("size", chargeCode.getSizeCode())
                            .put("charge", chargeCode.getChargeCode())
                            .put("value", chargeCode.getDescription())
                            .put("capacity", chargeCode.getCapacity()))
                    .collect(Collectors.toList())
                    .map(JSONArray::new);
        }

        public Mono<JSONObject> getQuoteFields(String user, String application, String company) {
            return Mono.just(new JSONObject())
                    .flatMap(json -> this.listCodeTypes(CodeType.CODE_UNIT, this.labelValueMapper())
                            .map(list -> json.put("unitList", list)))
                    .flatMap(json -> this.listStatusCodes(CodeType.QUOTES_STATUSES, this.labelCodeMapper())
                            .map(list -> json.put("quoteStatuses", list)))
                    .flatMap(json -> this.listCodeTypes(CodeType.CODE_FREQ, this.labelValueMapper())
                            .map(list -> json.put("frequencyList", list)))
                    .flatMap(json -> this.listChargeCodes(user, application, company)
                            .map(list -> json.put("descriptionList", list)));
        }
    }
}