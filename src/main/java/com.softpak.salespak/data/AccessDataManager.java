package com.softpak.salespak.data;

import com.softpak.access.AccessAPI;
import com.softpak.access.data.ServerInfo;
import com.softpak.access.data.ServiceInfo;
import com.softpak.access.data.WebResponse;
import com.softpak.email.SoftPakEmail;
import com.softpak.email.config.MailServerConfig;
import com.softpak.salespak.security.SoftPakDataPrinciple;
import com.softpak.salespak.service.DataManagerService;
import com.softpak.util.URLData;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.Date;
import java.util.stream.Collectors;

//import com.softpak.email.SoftPakEmail;
//import com.softpak.email.config.MailServerConfig;

@Component
public class AccessDataManager implements AbstractDataManager {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Resource(name = "${config.dataManager}")
    DataManagerService dataManagerService;

    public AccessDataApi connect(SoftPakDataPrinciple user) {

        AccessAPI api = (AccessAPI) dataManagerService.connectApi(new AccessAPI(), user);

        return new AccessDataApi(api);
    }

    public class AccessDataApi implements DataApi {

        private final AccessAPI api;

        private AccessDataApi(AccessAPI api) {
            this.api = api;
        }

        public String toString() {
            return "AccessDataApi";
        }

        public void disconnect() {
            this.api.disconnect();
        }

        public Mono<JSONArray> listServices(String company, int account) {
            if (!api.isConnected())
                return Mono.error(new Exception(ResponseStatusValue.FAILED_CONNECTION));

            return Flux.fromIterable(api.getServices(company, account))
                    .map(ServiceInfo::getJsonObject)
                    .collect(Collectors.toList())
                    .map(JSONArray::new);
        }

        public String quotePDF(String command, String company, String dataset, String quoteId, String email) {
            ServerInfo serverInfo = api.getServerInfo("DOCUMENT", company);
            String redirectURL = serverInfo.formatAsURL() + "/";

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            String seconds = String.valueOf(calendar.get(Calendar.SECOND));
            String documentType = String.format("%10s", "QUOTE");

            if (seconds.length() < 2) {
                seconds = "0" + seconds;
            }

            String data = "data=" + URLData.encrypt(seconds + dataset + documentType + quoteId);

            if (command.equals("EMAIL")) {
                String emailAddress = email;
                redirectURL = redirectURL + "email?" + data + "&email=" + emailAddress;
            } else if (command.equals("VIEW")) {
                redirectURL = redirectURL + "display?" + data;
            } else if (command.equals("SIGN")) {
                redirectURL = redirectURL + "display?" + data;
            }

            return redirectURL;
        }

        public void sendEmail(String userId, WebResponse webResponse, String subject, String body) {
            String fromEmail = api.getCompanyEmail("SALESPAK", "");
            ServerInfo sInfo = api.getServerInfo(ServerInfo.EMAIL_RELAY, "");
            MailServerConfig msc = new MailServerConfig(sInfo.getAddress(), sInfo.getPort(), sInfo.getUser(),
                    sInfo.getPassword(), sInfo.getProtocol().toLowerCase().contains("ssl"));
            SoftPakEmail spm = new SoftPakEmail(msc);

            spm.sendEmail(fromEmail, webResponse.getMessage(), subject, body);
        }
    }
}

