package com.softpak.salespak.data;

public interface DataApi {
    void disconnect();
    String toString();
}
