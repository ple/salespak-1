package com.softpak.salespak.data;

import com.softpak.access.SalesAPI;
import com.softpak.access.data.*;
import com.softpak.salespak.logging.AbstractErrorLogging;
import com.softpak.salespak.security.SoftPakDataPrinciple;
import com.softpak.salespak.service.DataManagerService;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Component
public class SalesDataManager implements AbstractDataManager {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Resource(name = "${config.dataManager}")
    DataManagerService dataManagerService;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Resource(name = "${config.errorLogger}")
    AbstractErrorLogging errorLogging;

    public SalesDataApi connect(SoftPakDataPrinciple user) {

        SalesAPI api = (SalesAPI) dataManagerService.connectApi(new SalesAPI(), user);

        return new SalesDataApi(api);
    }

    public class SalesDataApi implements DataApi {

        private final SalesAPI api;

        public String toString() {
            return "SalesDataApi";
        }

        private SalesDataApi(SalesAPI api) {
            this.api = api;
        }

        public void disconnect() {
            this.api.disconnect();
        }

        public Mono<JSONObject> prospectData(String userId, int uid, JSONObject res) {
            if (uid == 0)
                return Mono.error(new Exception("Param 'uid' must be provided."));

            return Mono.just(api.getProspect(userId, uid))
                    .flatMap(webRes -> Flux.fromIterable(webRes.getContactList())
                            .map(contactId -> api.getContact(contactId))
                            .map(ContactData::getJsonObject)
                            .collect(Collectors.toList())
                            .flatMap(list -> Mono.just(
                                    res
                                            .put("client", webRes.getJsonObject())
                                            .put("contactData", list))
                            ));
        }

        public Mono<JSONObject> postUpdateProspect(String userId, ProspectData prospectData, JSONArray deletedAddressList,
                                                   JSONArray deletedContactList, JSONArray deletedPhoneList, JSONArray deletedNotesList) {
            return Mono.just(api.updateProspect(userId, prospectData, null, deletedAddressList,
                    deletedContactList, deletedPhoneList, deletedNotesList))
                    .map(webResponse -> new JSONObject()
                            .put(ResponseStatusKey.STATUS, webResponse.getStatus())
                            .put(ResponseStatusKey.MESSAGE, webResponse.getMessage())
                    );
        }

        public Mono<JSONObject> postCreateProspect(String userId, ProspectData prospectData) {
            return Mono.just(api.addProspect(userId, prospectData))
                    .map(WebResponse::getJsonObject);
        }

        public Mono<JSONObject> contactData(String userId, int uid, JSONObject res) {
            if (uid == 0)
                return Mono.error(new Exception("Param 'uid' must be provided."));

            return Mono.just(api.getContact(uid))
                    .map(webRes -> res.put("client", webRes.getJsonObject()));
        }

        public Mono<JSONArray> listContactOwners(String user, Integer id) {
            return Mono.just(api.listContactOwners(user, id))
                    .map(JSONArray::new);
        }

        public Mono<JSONObject> postUpdateContact(String userId, ContactData updateData, JSONArray deletedAddressList,
                                                  JSONArray deletedPhoneList, JSONArray deletedNotesList) {
            if (!api.isConnected())
                return Mono.error(new Exception(ResponseStatusValue.FAILED_CONNECTION));

            return Mono.just(api.updateContact(userId, updateData, deletedAddressList, deletedPhoneList, deletedNotesList))
                    .map(webResponse -> new JSONObject()
                            .put(ResponseStatusKey.STATUS, webResponse.getStatus())
                            .put(ResponseStatusKey.MESSAGE, webResponse.getMessage())
                    );
        }

        public Mono<JSONObject> postCreateContact(String userId, ContactData createDate) {
            if (!api.isConnected())
                return Mono.error(new Exception(ResponseStatusValue.FAILED_CONNECTION));

            return Mono.just(api.addContact(userId, createDate))
                    .map(WebResponse::getJsonObject);
        }

        public Mono<JSONObject> customerData(String user, String company, int uid, JSONObject res) {
            if (uid == 0)
                return Mono.error(new Exception("Param 'uid' must be provided."));

            return Mono.just(api.getCustomer(company, uid, user))
                    .flatMap(webRes -> Flux.fromIterable(webRes.getContactList())
                            .map(contactId -> api.getContact(contactId))
                            .map(ContactData::getJsonObject)
                            .collect(Collectors.toList())
                            .flatMap(list -> Mono.just(
                                    res
                                            .put("client", webRes.getJsonObject())
                                            .put("contactData", list))
                            ));
        }

        public Mono<JSONObject> listPricing(String user, String company, Integer customer) {
            return Mono.just(api.listPricing(user, company, customer))
                    .map(JSONObject::new);
        }

        public Mono<JSONArray> listActivity(String user, String company, Integer customer) {
            return Mono.just(api.listActivity(user, company, customer))
                    .map(JSONArray::new);
        }

        public Mono<JSONArray> customerReport(String user, String company, Integer customer) {
            return Mono.just(api.customerReport(user, company, customer))
                    .map(JSONArray::new);
        }

        public Mono<JSONObject> customerNotes(String user, String company, Integer customer) {
            return Mono.just(api.listCustomerNotes(user, company, customer))
                    .map(CustomerNote::getJsonObject);
        }

        public Mono<JSONObject> listMonthlyCharges(String user, String company, Integer customer) {
            return Mono.just(api.listMonthlyCharges(user, company, customer))
                    .map(JSONObject::new);
        }

        public Mono<JSONArray> getEventList(String userId, String status, Integer toYear, Integer toMonth, String type) {
            if (!api.isConnected())
                return Mono.error(new Exception(ResponseStatusValue.FAILED_CONNECTION));

            DateTime lastDayOfTheMonth = new DateTime(toYear, toMonth, 1, 23, 59).plusMonths(1).minusDays(1);
            DateTime toDate = new DateTime(toYear, toMonth, lastDayOfTheMonth.getDayOfMonth(), 23, 59);

            try {
                toDate = new DateTime(toYear, toMonth, new DateTime().getDayOfMonth(), 23, 59);
            } catch (Exception e) {
                errorLogging.handleLogging(e);
            }

            return this.processListEvents(Flux.fromIterable(api.listEvents(userId, status, type, userId, toDate)));
        }


        public Mono<JSONArray> getFilteredEventList(String userId, String status, String description, Integer toYear, Integer toMonth, Integer toDay, Integer fromYear, Integer fromMonth, Integer fromDay, String type) {
            if (!api.isConnected())
                return Mono.error(new Exception(ResponseStatusValue.FAILED_CONNECTION));

            DateTime toDate = new DateTime(toYear, toMonth, toDay, 23, 59);
            DateTime fromDate = new DateTime(fromYear, fromMonth, fromDay, 00, 00);

            try {
                toDate = new DateTime(toYear, toMonth, toDay, 23, 59);
                fromDate = new DateTime(fromYear, fromMonth, fromDay, 00, 00);
            } catch (Exception e) {
                errorLogging.handleLogging(e);
            }

            return this.processListEvents(Flux.fromIterable(api.listEvents(userId, status, type, description, userId, fromDate, toDate)));
        }

        public Mono<JSONArray> getProspectEvents(String userId, int id, String status, DateTime fromDate, DateTime toDate) {
            if (!api.isConnected())
                return Mono.error(new Exception(ResponseStatusValue.FAILED_CONNECTION));

            return this.processListEvents(Flux.fromIterable(api.listProspectEvents(userId, id, status, fromDate, toDate)));
        }

        public Mono<JSONArray> getCustomerEvents(String userId, String company, int customer, String status, String type, String description, String eventUser, DateTime fromDate, DateTime toDate) {
            if (!api.isConnected())
                return Mono.error(new Exception(ResponseStatusValue.FAILED_CONNECTION));

            return this.processListEvents(Flux.fromIterable(api.listEvents(userId, company, customer, status, type, description, eventUser, fromDate, toDate)));
        }

        public Mono<JSONArray> getEventListWeek(String userId, String status, String toYear, String toMonth, String fromYear, String fromMonth) {
            if (!api.isConnected())
                return Mono.error(new Exception(ResponseStatusValue.FAILED_CONNECTION));

            return this.processListEvents(Flux.fromIterable(api.listEvents(userId, status, "", "", userId,
                    new DateTime(Integer.valueOf(fromYear), Integer.valueOf(fromMonth), Integer.valueOf(1), 0, 0),
                    new DateTime(Integer.valueOf(toYear), Integer.valueOf(toMonth), Integer.valueOf(1), 0, 0))));
        }

        public Mono<JSONArray> processListEvents(Flux<Event> eventFlux) {
            return eventFlux
                    .map(Event::getJsonObject)
                    .collect(Collectors.toList())
                    .map(JSONArray::new);
        }

        public Mono<JSONObject> addEvent(String userId, String event, Consumer<WebResponse> forwardEmail) {
            if (!api.isConnected())
                return Mono.error(new Exception(ResponseStatusValue.FAILED_CONNECTION));

            return Mono.just(api.addEvent(userId, Event.fromJSON(event)))
                    .map(webResponse -> {
                        forwardEmail.accept(webResponse);
                        return webResponse.getJsonObject();
                    });
        }

        public Mono<JSONObject> updateEvent(String userId, String event, JSONArray deletedNotes) {
            if (!api.isConnected())
                return Mono.error(new Exception(ResponseStatusValue.FAILED_CONNECTION));

            return Mono.just(api.updateEvent(userId, Event.fromJSON(event), deletedNotes))
                    .map(WebResponse::getJsonObject);
        }

        public Mono<JSONObject> deleteEvent(String userId, int id) {
            if (!api.isConnected())
                return Mono.error(new Exception(ResponseStatusValue.FAILED_CONNECTION));

            return Mono.just(api.deleteEvent(userId, id))
                    .map(WebResponse::getJsonObject);
        }

        public Mono<JSONObject> getQuoteData(Integer quoteId) {
            return Mono.just(api.getQuoteData(quoteId))
                    .map(QuoteData::getJsonObject);
        }

        public QuoteData getRawQuoteData(Integer quoteId) {
            return api.getQuoteData(quoteId);
        }

        public Mono<JSONArray> getQuoteList(String userId, String company, int customer, int uid) {
            return Flux.fromIterable(api.listQuotes(userId, company, customer, uid))
                    .map(QuoteData::getJsonObject)
                    .collect(Collectors.toList())
                    .map(JSONArray::new);
        }

        public Mono<JSONObject> postCreateQuote(String userId, QuoteData data) {
            if (!api.isConnected())
                return Mono.error(new Exception(ResponseStatusValue.FAILED_CONNECTION));

            return Mono.just(api.addQuote(userId, data))
                    .map(webResponse -> new JSONObject()
                            .put(ResponseStatusKey.STATUS, webResponse.getStatus())
                            .put(ResponseStatusKey.MESSAGE, webResponse.getMessage())
                    );
        }

        public Mono<JSONObject> postUpdateQuote(String userId, int id, QuoteData data, JSONArray deletedContractNotes, JSONArray deletedOperationsNotes) {
            if (!api.isConnected())
                return Mono.error(new Exception(ResponseStatusValue.FAILED_CONNECTION));

            return Mono.just(api.updateQuote(userId, id, data, deletedContractNotes, deletedOperationsNotes))
                    .map(webResponse -> new JSONObject()
                            .put(ResponseStatusKey.STATUS, webResponse.getStatus())
                            .put(ResponseStatusKey.MESSAGE, webResponse.getMessage())
                    );
        }

        public Mono<JSONObject> deleteQuote(int id) {
            if (!api.isConnected())
                return Mono.error(new Exception(ResponseStatusValue.FAILED_CONNECTION));

            return Mono.just(api.deleteQuote(id))
                    .map(webResponse -> new JSONObject()
                            .put(ResponseStatusKey.STATUS, webResponse.getStatus())
                            .put(ResponseStatusKey.MESSAGE, webResponse.getMessage())
                    );
        }

        public Mono<JSONObject> getClientSearch(String userId, String searchType, String firstName, String lastName, String businessName, String email, String phoneType, String phoneNumber, String searchBy, String addressNumber, String direction, String street, String suffix, String city, String state, String zip, String companyNumber, String customerNumber) {
            if (!api.isConnected())
                return Mono.error(new Exception(ResponseStatusValue.FAILED_CONNECTION));

            return Mono.just(new JSONObject()
                    .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                    .flatMap(json -> this.clientSearch(userId, searchType, firstName, lastName, businessName, email, phoneType, phoneNumber, searchBy, addressNumber, direction, street, suffix, city, state, zip, companyNumber, customerNumber)
                            .map(list -> json.put("results", list)));
        }

        public Mono<JSONArray> clientSearch(String userId, String searchType, String firstName, String lastName, String businessName, String email, String phoneType, String phoneNumber, String searchBy, String addressNumber, String direction, String street, String suffix, String city, String state, String zip, String companyNumber, String customerNumber) {
            return Mono.just(api.search(userId, searchType, firstName, lastName, businessName, email, phoneType, phoneNumber, searchBy, addressNumber, direction, street, suffix, city, state, zip, companyNumber, customerNumber))
                    .map(JSONArray::new);
        }

        public String getForwardEventEmailBody(Event event, String fromUser) {
            StringBuilder emailBody = new StringBuilder();

            try {
                emailBody.append("A new event titled \"" + event.getDescription() + "\" ");
                emailBody.append("has been added to your calendar in SalesPak by " + fromUser + ".\n\n");

                if (event.isAllDay()) {
                    emailBody.append("This is an all day event scheduled to start on " + event.getEventStartDateUSA() + ".\n\n");
                } else {
                    emailBody
                            .append("This event is scheduled to start on " + event.getEventStartDateUSA() + " at " + event.getEventStartTime());
                    emailBody.append(" and is scheduled to end on " + event.getEventEndDateUSA() + " at " + event.getEventEndTime() + ".\n\n");
                }

                emailBody.append("This e-mail was automatically generated. Please do not reply.");
            } catch (Exception e) {
                errorLogging.handleLogging(e);
            }

            return emailBody.toString();
        }

        public WebResponse uploadSignedQuote(String user, String path, String fileName, int id) {
            return api.uploadSignedQuote(user, path, fileName, id);
        }

        public WebResponse deleteUploadSignedQuote(int id, String userId) {
            return api.deleteUploadedSignedQuote(id, userId);
        }

        public  Mono<JSONObject> addCustomerNote(String userid, String company, int customer, Note note) {
            if (!api.isConnected())
                return Mono.error(new Exception(ResponseStatusValue.FAILED_CONNECTION));

            return Mono.just(api.addCustomerNote(userid, company, customer, note))
                    .map(webResponse -> new JSONObject()
                            .put(ResponseStatusKey.STATUS, webResponse.getStatus())
                            .put(ResponseStatusKey.MESSAGE, webResponse.getMessage())
                    );
        }


        public Mono<JSONObject> getUserSettings(String userId) {
            if (!api.isConnected())
                return Mono.error(new Exception(ResponseStatusValue.FAILED_CONNECTION));

            return Mono.just(api.getUserSettings(userId))
                    .map(SalesPakSettings::getJsonObject);
        }

        public Mono<JSONObject> updateSettings(String userId, SalesPakSettings settings) {
            if (!api.isConnected())
                return Mono.error(new Exception(ResponseStatusValue.FAILED_CONNECTION));

            return Mono.just(api.updateSettings(userId, settings))
                    .map(webResponse -> new JSONObject()
                            .put(ResponseStatusKey.STATUS, webResponse.getStatus())
                            .put(ResponseStatusKey.MESSAGE, webResponse.getMessage())
                    );
        }

        public Mono<JSONObject> listNotes(String userId, String noteType, Integer id) {
            if (!api.isConnected())
                return Mono.error(new Exception(ResponseStatusValue.FAILED_CONNECTION));

            return Mono.just(api.listNotes(userId, noteType, id));
        }
    }
}

