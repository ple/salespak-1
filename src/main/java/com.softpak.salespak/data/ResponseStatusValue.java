package com.softpak.salespak.data;

public final class ResponseStatusValue {
    public static final String STATUS_OK = "OK";
    public static final String STATUS_NOK = "Not OK";
    public static final String FAILED_CONNECTION = "Unable to connectApi to API";
}
