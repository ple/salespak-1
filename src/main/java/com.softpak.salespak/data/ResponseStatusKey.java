package com.softpak.salespak.data;

public final class ResponseStatusKey {
    public static final String STATUS = "status";
    public static final String MESSAGE = "message";
    public static final String ID = "id";
}
