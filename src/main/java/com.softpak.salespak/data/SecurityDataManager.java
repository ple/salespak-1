package com.softpak.salespak.data;

import com.softpak.access.SecurityAPI;
import com.softpak.access.data.CompanyInfo;
import com.softpak.access.data.SecurityInfo;
import com.softpak.salespak.security.SoftPakDataPrinciple;
import com.softpak.salespak.service.DataManagerService;
import org.json.JSONArray;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.stream.Collectors;

@Component
public class SecurityDataManager implements AbstractDataManager {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Resource(name = "${config.dataManager}")
    DataManagerService dataManagerService;


    public SecurityDataApi connect(SoftPakDataPrinciple user) {
        SecurityAPI api = (SecurityAPI) dataManagerService.connectApi(new SecurityAPI(), user);
        return new SecurityDataApi(api);
    }

    public String dataset(String username, String password) {
        SecurityAPI securityAPI = new SecurityAPI();
        String dataset = securityAPI.getDatasetFromISeries(dataManagerService.getSystem(), username, password);

        try {
            securityAPI.disconnect();
        } catch (Exception e) {
            // Ok if error, connection already closed
        }

        return dataset;
    }

    public class SecurityDataApi implements DataApi {

        private final SecurityAPI api;

        public String toString() {
            return "SecurityDataApi";
        }

        private SecurityDataApi(SecurityAPI api) {
            this.api = api;
        }

        public void disconnect() {
            this.api.disconnect();
        }

        public String getDataset() {
            return this.api.getDataset();
        }

        public Mono<JSONArray> listUsers() {
            return Mono.just(api.listUsers());
        }

        public Mono<JSONArray> listSalesId() {
            return Mono.just(api.listSalesIds());
        }

        public Mono<JSONArray> listCompanies(String user, ArrayList<CompanyInfo> companyList) {
            return Mono.just(api.getUserSecurity(user))
                    .flatMap(securityInfo -> Flux.fromIterable(companyList)
                            .filter(company -> securityInfo.hasAllCompanyAccess() || securityInfo.getCompanyList().contains(company.getCompany()))
                            .map(CompanyInfo::getJsonObject)
                            .collect(Collectors.toList())
                            .map(JSONArray::new));
        }

        public SecurityInfo getUserSecurity(String user) {
            return api.getUserSecurity(user);
        }

        public Mono<String> getUserEmail(String userId) {
            if (!api.isConnected())
                return Mono.error(new Exception(ResponseStatusValue.FAILED_CONNECTION));

            return Mono.just(api.getUserSecurity(userId))
                    .map(SecurityInfo::getEmail);
        }
    }
}

