package com.softpak.salespak.data;

public final class CodeType {
    public static final String CODE_UNIT = "QUOTE_UNIT";
    public static final String CODE_FREQ = "QUOTE_FREQ";
    public static final String PHONE = "PHONE";
    public static final String ADDRESS_PROSPECT = "ADDRPROSP";
    public static final String ADDRESS_CONTACT = "ADDRCONT";
    public static final String CODE_BUSINESS_TYPE = "SIC";
    public static final String CODE_CURRENT_PROVIDER = "PROVIDER";
    public static final String CODE_MANAGEMENT_COMPANY = "MGTCOMP";
    public static final String CODE_PROSPECT_LEVEL = "PROSPLVL";
    public static final String CODE_SOURCE = "SRCPROSP";
    public static final String CODE_TERRITORY_ID = "TERRITORY";
    public static final String QUOTES_STATUSES = "QUOTESTS";
    public static final String CODE_SERVICE_TYPE = "SVCPROSP";

}