package com.softpak.salespak.data;

import com.softpak.salespak.security.SoftPakDataPrinciple;

public interface AbstractDataManager {

    DataApi connect(SoftPakDataPrinciple user);
}
