package com.softpak.salespak.controller;

import com.softpak.salespak.data.*;
import com.softpak.salespak.model.signrequest.SignRequestEvent;
import com.softpak.salespak.security.SoftPakDataPrinciple;
import com.softpak.salespak.service.DataManagerService;
import com.softpak.salespak.service.UploadService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

@RestController
@RequestMapping(path = "/webhook")
public class WebhookApiController extends AbstractController {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Resource(name = "${config.dataManager}")
    DataManagerService dataManagerService;

    @Autowired
    SalesDataManager salesDataManager;

    @Autowired
    private UploadService uploadService;

    @Value("${config.uploadPath}")
    private String uploadPath;

    @PostMapping(value = "/sign-request/{dataset}/{quoteId}")
    public ResponseEntity signRequestEvent(
            @RequestBody SignRequestEvent signRequestEvent,
            @PathVariable(value = "dataset") String dataset,
            @PathVariable(value = "quoteId") int quoteId) {

        SoftPakDataPrinciple softPakDataPrinciple = new SoftPakDataPrinciple();
        softPakDataPrinciple.setDataset(dataset);

        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(softPakDataPrinciple);

        String pathName = uploadPath + "\\" + dataset;
        String fileName = dataset + "-signed-quote-" + quoteId + ".pdf";
        String hashFileName = UploadService.randomHash(fileName);

        if (signRequestEvent.getEvent_type().equals("signed")) {
            try {
                URL url = new URL(signRequestEvent.getDocument().getPdf());
                InputStream in = url.openStream();
                uploadService.uploadFile(in, pathName, hashFileName);
                in.close();

                salesDataApi.uploadSignedQuote(dataManagerService.getUser(), hashFileName, fileName, quoteId);
                salesDataApi.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping(value = "/sign-request/test", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> postTest(@RequestBody SignRequestEvent signRequestEvent) {

        return this.processDataApi(
                Mono.just(new JSONObject(signRequestEvent))
        );
    }
}
