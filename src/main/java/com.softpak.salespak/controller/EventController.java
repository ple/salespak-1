package com.softpak.salespak.controller;

import com.softpak.access.data.Event;
import com.softpak.salespak.data.*;
import com.softpak.salespak.security.SoftPakDataPrinciple;
import com.softpak.salespak.service.DataManagerService;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@RestController
public class EventController extends AbstractController {

    @Autowired
    private SalesDataManager salesDataManager;

    @Autowired
    private UtilDataManager utilDataManager;

    @Autowired
    private SecurityDataManager securityDataManager;

    @Autowired
    private CompanyDataManager companyDataManager;

    @Autowired
    private AccessDataManager accessDataManager;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Resource(name = "${config.dataManager}")
    DataManagerService dataManagerService;

    @GetMapping(value = "/event/options", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> eventOptions(
            @AuthenticationPrincipal SoftPakDataPrinciple user
    ) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);
        UtilDataManager.UtilDataApi utilDataApi = utilDataManager.connect(user);
        SecurityDataManager.SecurityDataApi securityDataApi = securityDataManager.connect(user);

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .flatMap(json -> securityDataApi.listUsers()
                                .map(list -> json.put("users", list)))
                        .flatMap(json -> utilDataApi.listCodeTypes("SRCEVENT", utilDataApi.jsonObjectMapper())
                                .map(arr -> json.put("srcevent", arr)))
                        .flatMap(json -> utilDataApi.listCodeTypes("EVENT", utilDataApi.jsonObjectMapper())
                                .map(arr -> json.put("event", arr)))
                        .map(json -> json.put("user", user.toString()))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi, utilDataApi, securityDataApi));
    }

    @GetMapping(value = "/event/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getEventListWithFromDates(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @RequestParam(value = "status", defaultValue = "ALL") String status,
            @RequestParam(value = "type", defaultValue = "") String type,
            @RequestParam(value = "toYear") Integer toYear,
            @RequestParam(value = "toMonth") Integer toMonth,
            @RequestParam(value = "toDay", required = false) Integer toDay,
            @RequestParam(value = "fromYear", required = false) Integer fromYear,
            @RequestParam(value = "fromMonth", required = false) Integer fromMonth,
            @RequestParam(value = "fromDay", required = false) Integer fromDay,
            @RequestParam(value = "userId", defaultValue = "") String userId,
            @RequestParam(value = "description", defaultValue = "") String description
    ) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);
        UtilDataManager.UtilDataApi utilDataApi = utilDataManager.connect(user);
        if(userId.isEmpty()) {
            return this.processDataApi(
                    Mono.just(new JSONObject()
                            .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                            .flatMap(json -> salesDataApi.getEventList(user.toString(), status, toYear, toMonth, type)
                                    .map(arr -> json.put("events", arr)))
            ).doOnTerminate(dataManagerService.disconnect(salesDataApi, utilDataApi));
        }else{
            return this.processDataApi(
                    Mono.just(new JSONObject()
                            .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                            .flatMap(json -> salesDataApi.getFilteredEventList(userId, status, description, toYear, toMonth, toDay, fromYear, fromMonth, fromDay, type)
                            .map(arr -> json.put("events", arr)))
            ).doOnTerminate(dataManagerService.disconnect(salesDataApi, utilDataApi));
        }
    }



    @GetMapping(value = "/event/list/week", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getEventListWithFromDates(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @RequestParam(value = "status", defaultValue = "All") String status,
            @RequestParam(value = "fromYear") String fromYear,
            @RequestParam(value = "fromMonth") String fromMonth,
            @RequestParam(value = "toYear") String toYear,
            @RequestParam(value = "toMonth") String toMonth
    ) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .flatMap(json -> salesDataApi.getEventListWeek(user.toString(), status, toYear, toMonth, fromYear, fromMonth)
                                .map(arr -> json.put("events", arr)))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }

    @PostMapping(value = "/event/add", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> addEvent(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @RequestBody String event,
            @RequestParam(defaultValue = "false") boolean isForward ) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);
        AccessDataManager.AccessDataApi accessDataApi = accessDataManager.connect(user);

        DateTime dateTime = (new DateTime()).plusMonths(99);

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .flatMap(json -> salesDataApi.addEvent(user.toString(), event, webResponse -> {
                            if (isForward) {
                                accessDataApi.sendEmail(user.toString(), webResponse, "Forwarded SignRequestEvent Received in SalesPak", salesDataApi.getForwardEventEmailBody(Event.fromJSON(event), user.toString()));
                            }
                        })
                        .map(obj -> json.put("response", obj)))
                        .flatMap(json -> salesDataApi.getEventList(user.toString(), "ALL", dateTime.getYear(), dateTime.getMonthOfYear(), "")
                                .map(arr -> json.put("events", arr)))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi, accessDataApi));
    }

    @PostMapping(value = "/event/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> updateEvent(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @RequestBody String event

    ) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        DateTime dateTime = (new DateTime()).plusMonths(3);
		JSONObject eventData = new JSONObject(event);

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .flatMap(json -> salesDataApi.updateEvent(user.toString(), event, eventData.getJSONArray("deletedNotesList"))
                                .map(obj -> json.put("response", obj)))
//                        .flatMap(json -> salesDataApi.getEventList(user.toString(), "ALL", dateTime.getYear(), dateTime.getMonthOfYear(), "")
//                                .map(arr -> json.put("events", arr)))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }

    @DeleteMapping(value = "/event/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> deleteEvent(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @PathVariable Integer id

    ) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        DateTime dateTime = (new DateTime()).plusMonths(3);

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .flatMap(json -> salesDataApi.deleteEvent(user.toString(), id)
                                .map(obj -> json.put("response", obj)))
                        .flatMap(json -> salesDataApi.getEventList(user.toString(), "OPEN", dateTime.getYear(), dateTime.getMonthOfYear(), "")
                                .map(arr -> json.put("events", arr)))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }
}
