package com.softpak.salespak.controller;

import com.softpak.access.data.QuoteData;
import com.softpak.salespak.data.*;
import com.softpak.salespak.model.RestCall;
import com.softpak.salespak.security.SoftPakDataPrinciple;
import com.softpak.salespak.service.DataManagerService;
import com.softpak.salespak.service.RestRequestService;
import com.softpak.salespak.service.UploadService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@RestController
@RequestMapping(path = "/quote")
public class QuoteApiController extends AbstractController {
    @Autowired
    SalesDataManager salesDataManager;

    @Autowired
    private UtilDataManager utilDataManager;

    @Autowired
    SecurityDataManager securityDataManager;

    @Autowired
    CompanyDataManager companyDataManager;

    @Autowired
    AccessDataManager accessDataManager;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Resource(name = "${config.dataManager}")
    DataManagerService dataManagerService;

    @Value("${config.uploadPath}")
    private String uploadPath;

    @Value("${config.appPath}")
    private String appPath;

    @Value("${config.signRequestUrl}")
    private String signRequestUrl;

    @Value("${config.signRequestToken}")
    private String signRequestToken;

    @Autowired
    private UploadService uploadService;

    @Autowired
    private RestRequestService restRequestService;


    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getQuoteData(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @PathVariable(value = "id") int id) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);
        UtilDataManager.UtilDataApi utilDataApi = utilDataManager.connect(user);

        String application = "SALESPAK";

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .flatMap(json -> salesDataApi.getQuoteData(id)
                                .map(obj -> json.put("quote", obj)))
                        .flatMap(json -> utilDataApi.getQuoteFields(user.toString(), application, json.getJSONObject("quote").getString("company"))
                                .map(obj -> json.put("fields", obj)))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi, utilDataApi));
    }

    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getQuoteListProspect(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @RequestParam(value = "id", defaultValue = "0") int id,
            @RequestParam(value = "company", defaultValue = "") String company,
            @RequestParam(value = "customer", defaultValue = "0") int customer) {

        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);
        UtilDataManager.UtilDataApi utilDataApi = utilDataManager.connect(user);
        String application = "SALESPAK";

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .flatMap(json -> salesDataApi.getQuoteList(user.toString(), company, customer, id)
                                .map(arr -> json.put("quote", arr)))
                        .flatMap(json -> utilDataApi.getQuoteFields(user.toString(), application, company)
                                .map(obj -> json.put("fields", obj)))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi, utilDataApi));
    }

    @GetMapping(value = "/fields", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getQuoteFields(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @RequestParam(value = "application", defaultValue = "SALESPAK") String application,
            @RequestParam(value = "company", defaultValue = "") String company) {
        UtilDataManager.UtilDataApi utilDataApi = utilDataManager.connect(user);

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .flatMap(json -> utilDataApi.getQuoteFields(user.toString(), application, company)
                                .map(obj -> json.put("fields", obj)))
        ).doOnTerminate(dataManagerService.disconnect(utilDataApi));
    }

    @GetMapping(value = "/pdf/view", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getQuotePdfView(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @RequestParam(value = "command") String command,
            @RequestParam(value = "quoteId") String _quoteId,
            @RequestParam(value = "email", defaultValue = "") String email,
            @RequestParam(value = "company") String company) {
        AccessDataManager.AccessDataApi accessDataApi = accessDataManager.connect(user);
        final String quoteId = String.format("%011d", Integer.parseInt(_quoteId));

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .map(json -> json.put("url", accessDataApi.quotePDF(command.toUpperCase(), company.toUpperCase(), user.getDataset(), quoteId, email)))
        ).doOnTerminate(dataManagerService.disconnect(accessDataApi));
    }

    @GetMapping(value = "/pdf/email", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> sendEmail(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @RequestParam(value = "quoteId") String _quoteId,
            @RequestParam(value = "company") String company,
            @RequestParam(value = "email") String email) {

        AccessDataManager.AccessDataApi accessDataApi = accessDataManager.connect(user);
        final String quoteId = String.format("%011d", Integer.parseInt(_quoteId));

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .map(json -> {
                            String url = accessDataApi.quotePDF("EMAIL", company.toUpperCase(), user.getDataset(), quoteId, email);
                            RestCall restCall = new RestCall(url);
                            restRequestService.get(restCall);

                            json
                                    .put("status", restCall.getStatus())
                                    .put("res", restCall.getResBody());

                            return json;
                        })
        );
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> create(@AuthenticationPrincipal SoftPakDataPrinciple user, @RequestBody String quote) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        return this.processDataApi(
                salesDataApi.postCreateQuote(user.toString(), QuoteData.fromJSON(quote))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> update(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @RequestBody String _quote,
            @PathVariable int id) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        JSONObject quote = new JSONObject(_quote);
        return this.processDataApi(
                salesDataApi.postUpdateQuote(user.toString(), id, QuoteData.fromJSON(quote.getJSONObject("quoteData")), quote.getJSONArray("deletedContractNotes"), quote.getJSONArray("deletedOperationsNotes"))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> delete(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @PathVariable int id) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        return this.processDataApi(
                salesDataApi.deleteQuote(id)
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }

    @PostMapping(value = "/upload/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> upload(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @PathVariable int id,
            @RequestParam("upload") MultipartFile file) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        String pathName = uploadPath + "\\" + user.getDataset();
        String hashFileName = UploadService.randomHash(file.getOriginalFilename());
        String fileName = file.getOriginalFilename();

        return this.processDataApi(
                Mono.just(!file.isEmpty())
                        .flatMap(ok -> {
                            boolean status = false;
                            JSONObject res = new JSONObject();
                            if (ok) {
                                status = uploadService.uploadFile(file, pathName, hashFileName);
                                res.put("webResponse", salesDataApi.uploadSignedQuote(user.toString(), hashFileName, fileName, id));
                            } else {
                                String error = "There was an error uploading a signed quote";
                                return Mono.error(new Error(error));
                            }

                            return Mono.just(res.put("uploaded", status));
                        })
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }

    @DeleteMapping(value = "/upload/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> deleteUpload(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @PathVariable int id) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        return this.processDataApi(
                Mono.just(new JSONObject())
                        .map(json -> json.put("res", salesDataApi.deleteUploadSignedQuote(id, user.toString())))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }

    @GetMapping(value = "/download/signed/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public Mono<FileSystemResource> downloadSignedQuote(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @PathVariable int id) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        String pathName = uploadPath + "\\" + user.getDataset();

        return Mono.just(salesDataApi.getRawQuoteData(id))
                .flatMap(quote -> Mono.just(new FileSystemResource(pathName + "\\" + quote.getPath())))
                .doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }

    @GetMapping(value = "/sign-request", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> generateSignRequest(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @RequestParam(value = "command") String command,
            @RequestParam(value = "quoteId") String _quoteId,
            @RequestParam(value = "company") String company,
            @RequestParam(value = "account") String account,
            @RequestParam(value = "fromEmail") String fromEmail,
            @RequestParam(value = "redirectUrl") String redirectUrl,
            @RequestParam(value = "firstName") String firstName,
            @RequestParam(value = "lastName") String lastName,
            @RequestParam(value = "signerId") String signerId,
            @RequestParam(value = "email") String email) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);
        AccessDataManager.AccessDataApi accessDataApi = accessDataManager.connect(user);

        final String quoteId = String.format("%011d", Integer.parseInt(_quoteId));
        String events_callback_url = this.appPath + "/webhook/sign-request/" + user.getDataset() + "/" + _quoteId;
        String docUrl = accessDataApi.quotePDF(command.toUpperCase(), company.toUpperCase(), user.getDataset(), quoteId, email);

        try {
            docUrl += "&dsaccount="+ URLEncoder.encode(account, "UTF-8")+"&dsname="+URLEncoder.encode(firstName + " " + lastName, "UTF-8")+"&dstitle=";
        } catch (UnsupportedEncodingException e) {
            // todo bugsnag
        }

        JSONObject signRequestObj = new JSONObject()
                .put("file_from_url", docUrl)
                .put("events_callback_url", events_callback_url)
                .put("from_email", fromEmail)
                .put("from_email_name", user.getUsername())
                .put("redirect_url", redirectUrl)
                .put("message", "Please sign this document.")
                .put("signers", new JSONArray().put(new JSONObject()
                        .put("email", email)
                        .put("first_name", firstName)
                        .put("last_name", lastName)
                        .put("order", 1)
                        .put("embed_url_user_id", signerId)
                ));

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization",signRequestToken);

        RestCall restCall = new RestCall(signRequestUrl, headers);
        restCall.setReqBody(signRequestObj);
        restRequestService.post(restCall);

        JSONObject resObj = new JSONObject(restCall.getResBody());
        JSONArray signers =  resObj.getJSONArray("signers");

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .map(json -> {
                            String signUrl = null;
                            for (int x = 0; x < signers.length(); x++) {
                                if (signers.getJSONObject(x).getString("email").equals(email)) {
                                    signUrl = signers.getJSONObject(x).getString("embed_url");
                                }
                            }

                            return json.put("redirect", signUrl);
                        })
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi, accessDataApi));
    }
}
