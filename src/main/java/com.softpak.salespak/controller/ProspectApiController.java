package com.softpak.salespak.controller;

import com.softpak.access.data.ProspectData;
import com.softpak.salespak.data.*;
import com.softpak.salespak.security.SoftPakDataPrinciple;
import com.softpak.salespak.service.DataManagerService;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@RestController
@RequestMapping(path = "/prospect")
public class ProspectApiController extends AbstractController {

    @Autowired
    private SalesDataManager salesDataManager;

    @Autowired
    private UtilDataManager utilDataManager;

    @Autowired
    private SecurityDataManager securityDataManager;

    @Autowired
    private CompanyDataManager companyDataManager;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Resource(name = "${config.dataManager}")
    DataManagerService dataManagerService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getProspectData(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @PathVariable(value = "id") int id) {

        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .flatMap(json -> salesDataApi.prospectData(user.getUsername(), id, new JSONObject())
                                .map(obj -> json.put("prospect", obj)))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> updateProspect(@RequestBody String _prospect, @AuthenticationPrincipal SoftPakDataPrinciple user) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        JSONObject prospect = new JSONObject(_prospect);

        return this.processDataApi(
                salesDataApi.postUpdateProspect(user.toString(), ProspectData.fromJSON(prospect.getJSONObject("prospectData")), prospect.getJSONArray("deletedAddressList"), prospect.getJSONArray("deletedContactList"), prospect.getJSONArray("deletedPhoneList"), prospect.getJSONArray("deletedNotesList"))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> createProspect(@AuthenticationPrincipal SoftPakDataPrinciple user, @RequestBody String prospect) {

        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        return this.processDataApi(
                salesDataApi.postCreateProspect(user.toString(), ProspectData.fromJSON(prospect))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }

    @GetMapping(value = "/events/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getEventListProspect(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @PathVariable(value = "id") Integer id,
            @RequestParam(value = "status", defaultValue = "OPEN") String status,
            @RequestParam(value = "toYear") Integer toYear,
            @RequestParam(value = "toMonth") Integer toMonth,
            @RequestParam(value = "fromYear") Integer fromYear,
            @RequestParam(value = "fromMonth") Integer fromMonth
    ) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        DateTime fromDate = new DateTime(fromYear, fromMonth,1,0, 0);
        DateTime toDate = new DateTime(toYear,toMonth, 1,0, 0);

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .flatMap(json -> salesDataApi.getProspectEvents(user.toString(), id, status, fromDate, toDate.plusMonths(1).minusDays(1))
                                .map(arr -> json.put("events", arr)))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }

    @GetMapping(value = "/options", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getProspectOptions(@AuthenticationPrincipal SoftPakDataPrinciple user) {

        UtilDataManager.UtilDataApi utilDataApi = utilDataManager.connect(user);
        SecurityDataManager.SecurityDataApi securityDataApi = securityDataManager.connect(user);
        CompanyDataManager.CompanyDataApi companyDataApi = companyDataManager.connect(user);

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .flatMap(json -> this.prospectOptions(json, utilDataApi, securityDataApi, companyDataApi, user.toString()))
        ).doOnTerminate(dataManagerService.disconnect(utilDataApi, securityDataApi, companyDataApi));
    }

    private Mono<JSONObject> prospectOptions(JSONObject json, UtilDataManager.UtilDataApi utilDataApi, SecurityDataManager.SecurityDataApi securityDataApi, CompanyDataManager.CompanyDataApi companyDataApi, String user) {
        return Mono.just(new JSONObject())
                .flatMap(opt -> utilDataApi.listCodeTypes(CodeType.CODE_BUSINESS_TYPE, utilDataApi.jsonObjectMapper())
                        .map(list -> opt.put("businessType", list)))
                .flatMap(opt -> utilDataApi.listCodeTypes(CodeType.CODE_SERVICE_TYPE, utilDataApi.jsonObjectMapper())
                        .map(list -> opt.put("serviceType", list)))
                .flatMap(opt -> utilDataApi.listCodeTypes(CodeType.CODE_CURRENT_PROVIDER, utilDataApi.jsonObjectMapper())
                        .map(list -> opt.put("provider", list)))
                .flatMap(opt -> utilDataApi.listCodeTypes(CodeType.CODE_MANAGEMENT_COMPANY, utilDataApi.jsonObjectMapper())
                        .map(list -> opt.put("managementCompany", list)))
                .flatMap(opt -> utilDataApi.listCodeTypes(CodeType.CODE_PROSPECT_LEVEL, utilDataApi.jsonObjectMapper())
                        .map(list -> opt.put("prospectLevel", list)))
                .flatMap(opt -> utilDataApi.listCodeTypes(CodeType.CODE_SOURCE, utilDataApi.jsonObjectMapper())
                        .map(list -> opt.put("source", list)))
                .flatMap(opt -> utilDataApi.listCodeTypes(CodeType.CODE_TERRITORY_ID, utilDataApi.jsonObjectMapper())
                        .map(list -> opt.put("territoryId", list)))
                .flatMap(opt -> utilDataApi.listCodeTypes(CodeType.PHONE, utilDataApi.jsonObjectMapper())
                        .map(list -> opt.put("phoneType", list)))
//                .flatMap(opt -> utilDataApi.listCodeTypes(CodeType.ADDRESS_PROSPECT, utilDataApi.jsonObjectMapper())
//                        .map(list -> opt.put("addressType", list)))
                .flatMap(opt -> securityDataApi.listUsers()
                        .map(list -> opt.put("users", list)))
                .flatMap(opt -> securityDataApi.listSalesId()
                        .map(list -> opt.put("sales", list)))
                .flatMap(opt -> companyDataApi.getCompanyList()
                        .flatMap(companyList -> securityDataApi.listCompanies(user, companyList))
                        .map(list -> opt.put("companies", list)))
                .map(opt -> json.put("options", opt));
    }

}
