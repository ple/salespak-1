package com.softpak.salespak.controller;

import com.softpak.salespak.data.ResponseStatusKey;
import com.softpak.salespak.data.ResponseStatusValue;
import com.softpak.salespak.data.SalesDataManager;
import com.softpak.salespak.security.SoftPakDataPrinciple;
import com.softpak.salespak.service.DataManagerService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@RestController
@RequestMapping(path = "/search")
public class SearchApiController extends AbstractController {

    @Autowired
    SalesDataManager salesDataManager;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Resource(name = "${config.dataManager}")
    DataManagerService dataManagerService;

    @GetMapping(value = "/client", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getClientSearch(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @RequestParam(value = "searchType", defaultValue = "") String searchType,
            @RequestParam(value = "firstName", defaultValue = "") String firstName,
            @RequestParam(value = "lastName", defaultValue = "") String lastName,
            @RequestParam(value = "businessName", defaultValue = "") String businessName,
            @RequestParam(value = "email", defaultValue = "") String email,
            @RequestParam(value = "phoneType", defaultValue = "") String phoneType,
            @RequestParam(value = "phoneNumber", defaultValue = "") String phoneNumber,
            @RequestParam(value = "searchBy", defaultValue = "") String searchBy,
            @RequestParam(value = "number", defaultValue = "") String number,
            @RequestParam(value = "direction", defaultValue = "") String direction,
            @RequestParam(value = "street", defaultValue = "") String street,
            @RequestParam(value = "suffix", defaultValue = "") String suffix,
            @RequestParam(value = "city", defaultValue = "") String city,
            @RequestParam(value = "state", defaultValue = "") String state,
            @RequestParam(value = "zip", defaultValue = "") String zip,
            @RequestParam(value = "companyNumber", defaultValue = "") String companyNumber,
            @RequestParam(value = "customerNumber", defaultValue = "") String customerNumber
    ) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        return this.processDataApi(Mono.just(new JSONObject()
                .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                .flatMap(json -> salesDataApi.getClientSearch(user.toString(),
                        searchType,
                        firstName.replace("*", "%"),
                        lastName.replace("*", "%"),
                        businessName.replace("*", "%"),
                        email.replace("*", "%"),
                        phoneType,
                        phoneNumber,
                        searchBy,
                        number.replace("*", "%"),
                        direction,
                        street.replace("*", "%"),
                        suffix,
                        city.replace("*", "%"),
                        state,
                        zip.replace("*", "%"),
                        companyNumber.replace("*", "%"),
                        customerNumber.replace("*", "%"))
                        .map(obj -> json.put("search", obj))))
                .doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }

    @GetMapping(value = "/quote/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> quoteSearch(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @PathVariable(value = "id") Integer id
    ) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        return this.processDataApi(Mono.just(new JSONObject()
                .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                .flatMap(json -> salesDataApi.getQuoteData(id)
                        .map(obj -> json.put("quote", obj)))
                .flatMap(json -> {
                            int prospectId = json.getJSONObject("quote").getInt("prospectId");
                            int customer = json.getJSONObject("quote").getInt("customer");
                            String company = json.getJSONObject("quote").getString("company");

                            if (prospectId > 0) {
                                return salesDataApi.prospectData(user.getUsername(), prospectId, new JSONObject())
                                        .map(obj -> json.put("prospect", obj));
                            } else if (customer > 0) {
                                return salesDataApi.customerData(user.toString(), company, customer, new JSONObject())
                                        .map(obj -> json.put("customer", obj));
                            }

                            return Mono.just(json);
                        }
                )
        )
                .doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }

    @PostMapping(value = "/notes/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> notesList(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @RequestBody String _noteType
    ) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);
        JSONObject noteType = new JSONObject(_noteType);


        return this.processDataApi(Mono.just(new JSONObject()
                .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                .flatMap(json -> salesDataApi.listNotes(user.toString(), noteType.getString("noteType"), noteType.getInt("id"))
                        .map(obj -> json.put("response", obj)))
        )
                .doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }
}
