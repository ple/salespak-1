package com.softpak.salespak.controller;

import com.softpak.access.data.ContactData;
import com.softpak.salespak.security.SoftPakDataPrinciple;
import com.softpak.salespak.service.DataManagerService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import com.softpak.salespak.data.*;

import javax.annotation.Resource;

@RestController
@RequestMapping(path = "/contact")
public class ContactApiController extends AbstractController {

    @Autowired
    SalesDataManager salesDataManager;

    @Autowired
    private UtilDataManager utilDataManager;

    @Autowired
    SecurityDataManager securityDataManager;

    @Autowired
    CompanyDataManager companyDataManager;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Resource(name = "${config.dataManager}")
    DataManagerService dataManagerService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getContactData(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @PathVariable(value = "id") int id) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);
        UtilDataManager.UtilDataApi utilDataApi = utilDataManager.connect(user);
        SecurityDataManager.SecurityDataApi securityDataApi = securityDataManager.connect(user);
        CompanyDataManager.CompanyDataApi companyDataApi = companyDataManager.connect(user);

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .flatMap(json -> salesDataApi.contactData(user.toString(), id, new JSONObject())
                                .map(obj -> json.put("contact", obj)))
                        .flatMap(json -> salesDataApi.listContactOwners(user.toString(), id)
                                .map(arr -> json.put("references", arr)))
                        .flatMap(json -> this.contactOptions(json, utilDataApi, securityDataApi, companyDataApi, user.toString()))
                        .flatMap(json -> utilDataApi.listMiscellaneousVariables(user.toString(), "", 0, id, "CONTACT")
                                .map(arr -> {
                                    json.getJSONObject("options").put("miscVariables", arr);
                                    return json;
                                }))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi, utilDataApi, securityDataApi, companyDataApi));
    }

    @GetMapping(value = "/recent/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getRecentContactData(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @PathVariable(value = "id") int id) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .flatMap(json -> salesDataApi.contactData(user.toString(), id, new JSONObject())
                                .map(obj -> json.put("contact", obj)))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }

    @GetMapping(value = "/options", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getContactOptions(@AuthenticationPrincipal SoftPakDataPrinciple user) {
        UtilDataManager.UtilDataApi utilDataApi = utilDataManager.connect(user);
        SecurityDataManager.SecurityDataApi securityDataApi = securityDataManager.connect(user);
        CompanyDataManager.CompanyDataApi companyDataApi = companyDataManager.connect(user);
        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .flatMap(json -> this.contactOptions(json, utilDataApi, securityDataApi, companyDataApi, user.toString()))
                        .flatMap(json -> utilDataApi.listMiscellaneousVariables(user.toString(), "", 0, 0, "CONTACT")
                                .map(arr -> {
                                    json.getJSONObject("options").put("miscVariables", arr);
                                    return json;
                                }))
        ).doOnTerminate(dataManagerService.disconnect(utilDataApi, securityDataApi, companyDataApi));
    }

    private Mono<JSONObject> contactOptions(JSONObject json, UtilDataManager.UtilDataApi utilDataApi, SecurityDataManager.SecurityDataApi securityDataApi, CompanyDataManager.CompanyDataApi companyDataApi, String user) {
        return Mono.just(new JSONObject())
                .flatMap(opt -> utilDataApi.listCodeTypes(CodeType.PHONE, utilDataApi.jsonObjectMapper())
                        .map(list -> opt.put("phoneType", list)))
                .flatMap(opt -> utilDataApi.listCodeTypes(CodeType.ADDRESS_CONTACT, utilDataApi.jsonObjectMapper())
                        .map(list -> opt.put("addressType", list)))
                .flatMap(opt -> securityDataApi.listUsers()
                        .map(list -> opt.put("users", list)))
                .flatMap(opt -> companyDataApi.getCompanyList()
                        .flatMap(companyList -> securityDataApi.listCompanies(user, companyList))
                        .map(list -> opt.put("companies", list)))
                .map(opt -> json.put("options", opt));
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> updateContact(@AuthenticationPrincipal SoftPakDataPrinciple user, @RequestBody String _update) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        JSONObject update = new JSONObject(_update);

        return this.processDataApi(
                salesDataApi.postUpdateContact(user.toString(), ContactData.fromJSON(update.getJSONObject("contactData")), update.getJSONArray("deletedAddressList"), update.getJSONArray("deletedPhoneList"), update.getJSONArray("deletedNotesList"))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> createContact(@AuthenticationPrincipal SoftPakDataPrinciple user, @RequestBody String create) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        return this.processDataApi(
                salesDataApi.postCreateContact(user.toString(), ContactData.fromJSON(create))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }
}
