package com.softpak.salespak.controller;

import com.softpak.salespak.data.ResponseStatusKey;
import com.softpak.salespak.data.ResponseStatusValue;
import com.softpak.salespak.logging.AbstractErrorLogging;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

public abstract class AbstractController {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Resource(name = "${config.errorLogger}")
    protected AbstractErrorLogging errorLogging;

    protected Mono<ResponseEntity<String>> processDataApi(Mono<JSONObject> dataApiCall) {
        return dataApiCall
                .map(data -> new ResponseEntity<>(data.toString(), HttpStatus.OK))
                .onErrorResume(err -> {
                            errorLogging.handleLogging(err);
                            return Mono.just(
                                    new ResponseEntity<>(
                                            new JSONObject()
                                                    .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_NOK)
                                                    .put(ResponseStatusKey.MESSAGE, err.getMessage())
                                                    .toString(),
                                            HttpStatus.INTERNAL_SERVER_ERROR)
                            );
                        }
                );
    }
}
