package com.softpak.salespak.controller;

import com.softpak.access.data.QuoteData;
import com.softpak.access.data.SalesPakSettings;
import com.softpak.salespak.data.ResponseStatusKey;
import com.softpak.salespak.data.ResponseStatusValue;
import com.softpak.salespak.data.SalesDataManager;
import com.softpak.salespak.data.SecurityDataManager;
import com.softpak.salespak.security.SoftPakDataPrinciple;
import com.softpak.salespak.service.DataManagerService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@Controller
public class LoginController extends AbstractController {

    @Autowired
    SalesDataManager salesDataManager;

    @Autowired
    SecurityDataManager securityDataManager;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Resource(name = "${config.dataManager}")
    DataManagerService dataManagerService;

    @GetMapping(value = "/login")
    public String getLogin() {
        return "login";
    }

    @GetMapping(value = "/user/info", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getUserInfo(
            @AuthenticationPrincipal SoftPakDataPrinciple user) {
        return this.processDataApi(Mono.just(new JSONObject()
                .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK)
                .put("username", user.getUsername())
                .put("dataset", user.getDataset())));
    }

    @GetMapping(value = "/user/settings", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getUserSettings(
            @AuthenticationPrincipal SoftPakDataPrinciple user) {

        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);
        SecurityDataManager.SecurityDataApi securityDataApi = securityDataManager.connect(user);

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .flatMap(json -> salesDataApi.getUserSettings(user.toString())
                                .map(obj -> json.put("settings", obj)))
                        .flatMap(json -> securityDataApi.getUserEmail(user.toString())
                                .map(str -> json.put("email", str)))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi, securityDataApi));
    }

    @PostMapping(value = "/user/settings", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> saveUserSettings(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @RequestBody String settings) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        return this.processDataApi(
                salesDataApi.updateSettings(user.toString(), SalesPakSettings.fromJSON(settings))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }
}
