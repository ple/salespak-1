package com.softpak.salespak.controller;

import com.softpak.access.data.Note;
import com.softpak.access.data.NoteAlternate;
import com.softpak.salespak.data.*;
import com.softpak.salespak.security.SoftPakDataPrinciple;
import com.softpak.salespak.service.DataManagerService;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@RestController
@RequestMapping(path = "/customer")
public class CustomerApiController extends AbstractController {

    @Autowired
    SalesDataManager salesDataManager;

    @Autowired
    private UtilDataManager utilDataManager;

    @Autowired
    SecurityDataManager securityDataManager;

    @Autowired
    CompanyDataManager companyDataManager;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Resource(name = "${config.dataManager}")
    DataManagerService dataManagerService;

    @Autowired
    AccessDataManager accessDataManager;

    @GetMapping(value = "/{company}/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getCustomerData(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @PathVariable(value = "id") int id,
            @PathVariable(value = "company") String company) {

        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);
        UtilDataManager.UtilDataApi utilDataApi = utilDataManager.connect(user);
        SecurityDataManager.SecurityDataApi securityDataApi = securityDataManager.connect(user);
        CompanyDataManager.CompanyDataApi companyDataApi = companyDataManager.connect(user);

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .flatMap(json -> salesDataApi.customerData(user.toString(), company, id, new JSONObject())
                                .map(obj -> json.put("customer", obj)))
                        .flatMap(json -> salesDataApi.listContactOwners(user.toString(), id)
                                .map(arr -> json.put("references", arr)))
                        .flatMap(json -> salesDataApi.listPricing(user.toString(), company, id)
                                .map(obj -> json.put("pricing", obj)))
                        .flatMap(json -> salesDataApi.listMonthlyCharges(user.toString(), company, id)
                                .map(obj -> json.put("monthlyCharges", obj)))
                        .flatMap(json -> salesDataApi.listActivity(user.toString(), company, id)
                                .map(arr -> json.put("activity", arr)))
                        .flatMap(json -> salesDataApi.customerReport(user.toString(), company, id)
                                .map(arr -> json.put("customerReport", arr)))
                        .flatMap(json -> salesDataApi.customerNotes(user.toString(), company, id)
                                .map(obj -> json.put("notesList", obj)))

        ).doOnTerminate(dataManagerService.disconnect(salesDataApi, utilDataApi, securityDataApi, companyDataApi));
    }

    @GetMapping(value = "/recent/{company}/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getRecentCustomerData(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @PathVariable(value = "id") int id,
            @PathVariable(value = "company") String company) {

        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .flatMap(json -> salesDataApi.customerData(user.toString(), company, id, new JSONObject())
                                .map(obj -> json.put("customer", obj)))

        ).doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }


    @GetMapping(value = "/events/{companyId}/{customerId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> getEventListProspect(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @PathVariable(value = "customerId") Integer customerId,
            @PathVariable(value = "companyId") String companyId,
            @RequestParam(value = "status", defaultValue = "OPEN") String status,
            @RequestParam(value = "type", defaultValue = "") String type,
            @RequestParam(value = "toYear") Integer toYear,
            @RequestParam(value = "toMonth") Integer toMonth,
            @RequestParam(value = "fromYear") Integer fromYear,
            @RequestParam(value = "fromMonth") Integer fromMonth
    ) {
        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        DateTime fromDate = new DateTime(fromYear, fromMonth, 1, 0, 0);
        DateTime toDate = new DateTime(toYear, toMonth, 1, 0, 0);

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .flatMap(json -> salesDataApi.getCustomerEvents(
                                user.toString(),
                                companyId,
                                customerId,
                                status,
                                type,
                                "",
                                user.toString(),
                                fromDate,
                                toDate
                        )
                                .map(arr -> json.put("events", arr)))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }

    @DeleteMapping(value = "/event/delete/{eventId}/{companyId}/{customerId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> deleteEvent(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @PathVariable Integer eventId,
            @PathVariable String companyId,
            @PathVariable Integer customerId,
            @RequestParam(value = "status", defaultValue = "OPEN") String status,
            @RequestParam(value = "type", defaultValue = "") String type,
            @RequestParam(value = "toYear") Integer toYear,
            @RequestParam(value = "toMonth") Integer toMonth,
            @RequestParam(value = "fromYear") Integer fromYear,
            @RequestParam(value = "fromMonth") Integer fromMonth) {

        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);
        DateTime fromDate = new DateTime(fromYear, fromMonth, 1, 0, 0);
        DateTime toDate = new DateTime(toYear, toMonth, 1, 0, 0);

        return this.processDataApi(
                Mono.just(new JSONObject()
                        .put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .flatMap(json -> salesDataApi.deleteEvent(user.toString(), eventId)
                                .map(obj -> json.put("response", obj)))
                        .flatMap(json -> salesDataApi.getCustomerEvents(
                                user.toString(),
                                companyId,
                                customerId,
                                status,
                                type,
                                "",
                                user.toString(),
                                fromDate,
                                toDate
                        )
                                .map(arr -> json.put("events", arr)))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }

    @PostMapping(value = "/note/{company}/{customerId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> addCustomerNote(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @PathVariable(value = "customerId") int customerId,
            @PathVariable(value = "company") String company,
            @RequestBody String note) {

        SalesDataManager.SalesDataApi salesDataApi = salesDataManager.connect(user);

        return this.processDataApi(
                Mono.just(new JSONObject().put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                        .flatMap(json -> salesDataApi.addCustomerNote(user.toString(), company, customerId, NoteAlternate.fromJSON(new JSONObject(note)))
                                .map(obj -> json.put("addCustomerNoteResponse", obj)))
                        .flatMap(json -> salesDataApi.customerNotes(user.toString(), company, customerId)
                                .map(obj -> json.put("notesList", obj)))
        ).doOnTerminate(dataManagerService.disconnect(salesDataApi));
    }

    @GetMapping(value = "/services/list/{company}/{account}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<String>> listServices(
            @AuthenticationPrincipal SoftPakDataPrinciple user,
            @PathVariable(value = "company") String company,
            @PathVariable(value = "account") int account
    ) {
        AccessDataManager.AccessDataApi accessDataApi = accessDataManager.connect(user);

        return this.processDataApi(
                Mono.just(new JSONObject().put(ResponseStatusKey.STATUS, ResponseStatusValue.STATUS_OK))
                .flatMap(json -> accessDataApi.listServices(company, account)
                        .map(obj -> json.put("results", obj)))
        ).doOnTerminate(dataManagerService.disconnect(accessDataApi));
    }
}
