package com.softpak.salespak.model.signrequest;

public class SignRequestEvent {
    private String event_type;
    private Document document;

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }
}
