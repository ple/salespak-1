package com.softpak.salespak.model;

import com.softpak.access.data.ProspectData;
import org.json.JSONArray;

import java.util.ArrayList;

public class UpdateProspect {
    private ProspectData prospectData;
    private ArrayList<Integer> deletedAddressList;
    private ArrayList<Integer> deletedContactList;
    private ArrayList<Integer> deletedPhoneList;
    private ArrayList<Integer> deletedNotesList;

    public ProspectData getProspectData() {
        return prospectData;
    }

    public void setProspectData(ProspectData prospectData) {
        this.prospectData = prospectData;
    }

    public JSONArray getDeletedAddressList() {
        return  new JSONArray(deletedAddressList);
    }

    public void setDeletedAddressList(ArrayList<Integer> deletedAddressList) {
        this.deletedAddressList = deletedAddressList;
    }

    public JSONArray getDeletedContactList() {
        return  new JSONArray(deletedContactList);
    }

    public void setDeletedContactList(ArrayList<Integer> deletedContactList) {
        this.deletedContactList = deletedContactList;
    }

    public JSONArray getDeletedPhoneList() {
        return  new JSONArray(deletedPhoneList);
    }

    public void setDeletedPhoneList(ArrayList<Integer> deletedPhoneList) {
        this.deletedPhoneList = deletedPhoneList;
    }

    public JSONArray getDeletedNotesList() {
        return new JSONArray(deletedNotesList);
    }

    public void setDeletedNotesList(ArrayList<Integer> deletedNotesList) {
        this.deletedNotesList = deletedNotesList;
    }
}
