package com.softpak.salespak.model;

import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


public class RestCall {

    private String url;
    private RestTemplate rest;
    private HttpHeaders headers;
    private HttpStatus status;
    private JSONObject reqBody;
    private ResponseEntity<String> responseEntity;

    public RestCall(String url) {
        this(url, new HttpHeaders());
    }

    public RestCall(String url, HttpHeaders headers) {
        this.url = url;
        this.rest = new RestTemplate();
        this.headers = headers;
    }

    public String getUrl() {
        return url;
    }

    public RestTemplate getRest() {
        return rest;
    }

    public HttpHeaders getHeaders() {
        return headers;
    }

    public void setHeaders(HttpHeaders headers) {
        this.headers = headers;
    }

    public HttpStatus getStatus() {
        return this.responseEntity != null ? this.responseEntity.getStatusCode() : null;
    }

    public String getResBody() {
        return this.responseEntity != null ? this.responseEntity.getBody() : null;
    }

    public JSONObject getReqBody() {
        return reqBody;
    }

    public void setReqBody(JSONObject reqBody) {
        this.reqBody = reqBody;
    }

    public ResponseEntity<String> getResponseEntity() {
        return responseEntity;
    }

    public void setResponseEntity(ResponseEntity<String> responseEntity) {
        this.responseEntity = responseEntity;
    }
}
