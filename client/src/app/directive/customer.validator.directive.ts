import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, ValidationErrors, FormGroup } from '@angular/forms';

import { ValidateInput } from '../component/partial/search/form/customer.validator';

@Directive({
  selector: '[validateInput]',
  providers: [{provide: NG_VALIDATORS, useExisting: CustomerValidatorDirective, multi: true}]
})

export class CustomerValidatorDirective implements Validator{
  @Input('validateInput') validateInput: string[] = [];

  validate(formGroup: FormGroup): ValidationErrors {
    return ValidateInput(this.validateInput[0], this.validateInput[1], this.validateInput[2], this.validateInput[3],
      this.validateInput[4], this.validateInput[5], this.validateInput[6], this.validateInput[7], this.validateInput[8])(formGroup);
  }
}
