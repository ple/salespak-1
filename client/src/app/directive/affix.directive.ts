import {Directive, ElementRef, Inject, Input, Renderer2} from "@angular/core";
import {DOCUMENT} from "@angular/common";

@Directive({
  selector: '[affix]'
})

export class AffixDirective {
  @Input('affix') affix: boolean;

  constructor(@Inject(DOCUMENT) private document: Document, private renderer: Renderer2, private el: ElementRef) { }

  ngOnInit() {
    window.addEventListener('scroll', this.scroll, true);
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }

  scroll = (): void => {
    //compare element Y-Axis position to viewport bottom coordinate
    if (this.affix && this.el.nativeElement.getBoundingClientRect().y > this.document.body.getClientRects()[0].bottom) {
      //add class to target element
      this.renderer.addClass(this.el.nativeElement, 'sticky');
      this.renderer.addClass(this.el.nativeElement, 'bg-light');

      if (this.document.body.getClientRects()[0].width <= 414) {
        this.renderer.addClass(this.el.nativeElement, 'col-sm-12');
        this.renderer.addClass(this.el.nativeElement, 'text-center');
        this.renderer.removeClass(this.el.nativeElement, 'modal-footer');
        this.renderer.removeClass(this.el.nativeElement.children[0].children[0], 'mx-1');
        this.renderer.removeClass(this.el.nativeElement.children[0].children[0], 'pull-right');
      }
    }
  };
}
