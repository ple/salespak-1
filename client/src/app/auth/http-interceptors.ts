import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpResponse, HttpErrorResponse
} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment, auth, api} from '../../environments/environment';
import {tap} from "rxjs/operators";

@Injectable()
export class HttpInterceptors implements HttpInterceptor {


  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (!environment.production)
      request = request.clone({
        setHeaders: {
          Authorization: auth.basic
        }
      });

    return next.handle(request).pipe(
      tap((event: HttpEvent<any>) => {},
        (err: any) => {
          if (err instanceof HttpErrorResponse && err.url && err.url.includes('login'))
              window.location.href = `${err.url}`;
        })
    );
  }
}
