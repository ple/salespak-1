import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {UIRouterModule} from "@uirouter/angular";
import {STATES} from "./router/routes.router"
import {AppComponent} from './component/module/app/app.component';
import {FooterComponent} from './component/footer/footer.component';
import {SideNavComponent} from './component/partial/nav/side-nav/side-nav.component';
import {TopNavComponent} from './component/partial/nav/top-nav/top-nav.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {BsDatepickerModule, TimepickerModule, ModalModule, TabsModule, TooltipModule} from 'ngx-bootstrap';
import {HttpClientModule, HTTP_INTERCEPTORS} from "@angular/common/http";
import {ProspectViewComponent} from './component/module/prospect-view/prospect-view.component';
import {FormsModule} from "@angular/forms";
import {PhoneFieldsComponent} from './component/partial/contact/phone-fields/phone-fields.component';
import {AddressFieldComponent} from './component/partial/contact/address-field/address-field.component';
import {NoteComponent} from './component/partial/note/note/note.component';
import {RadioComponent} from './component/partial/form/radio/radio.component';
import { NoteFieldComponent } from './component/modal/note-field/note-field.component';
import {TextMaskModule} from "angular2-text-mask";
import { MiscVarFieldComponent } from './component/partial/prospect/misc-var-field/misc-var-field.component';
import { ContactsFieldComponent } from './component/partial/contact/contacts-field/contacts-field.component';
import { CustomerViewComponent } from './component/module/customer-view/customer-view.component';
import {ToastrModule} from "ngx-toastr";
import {QuoteViewComponent} from "./component/module/quote-view/quote-view.component";
import { QuoteServComponent } from './component/modal/quote-service/quote-serv.component';
import { CheckboxComponent } from './component/partial/form/checkbox/checkbox.component';
import {MatTableModule, MatPaginatorModule} from '@angular/material';
import { EventListComponent } from './component/partial/event/event-list/event-list.component';
import { EventEditComponent } from './component/modal/event-edit/event-edit.component';
import {HttpInterceptors} from "./auth/http-interceptors";
import {FullCalendarModule} from "ng-fullcalendar";
import { ForwardEventComponent } from './component/modal/forward-event/forward-event.component';
import { EmailPromptComponent } from './component/modal/email-prompt/email-prompt.component';
import { ContactListComponent } from './component/partial/contact/contact-list/contact-list.component';
import { AddContactComponent } from './component/modal/add-contact/add-contact.component';
import { AddContactResultsComponent } from './component/partial/contact/add-contact-results/add-contact-results.component';
import { CalendarFilterComponent } from './component/modal/calendar-filter/calendar-filter.component';
import { PhoneListComponent } from './component/partial/contact/phone-list/phone-list.component';
import { DeleteConfirmationComponent } from './component/partial/form/delete-confirmation/delete-confirmation.component';
import { AddressListComponent } from './component/partial/contact/address-list/address-list.component';
import { UploadComponent } from './component/modal/upload/upload.component';
import { SearchViewComponent } from './component/module/search-view/search-view.component';
import { ResultsComponent } from './component/partial/search/results/results.component';
import { FormComponent } from './component/partial/search/form/form.component';
import {SearchResultsContactsComponent} from "./component/partial/search/search-results-contacts/search-results-contacts.component";
import {SearchResultsCustomersComponent} from "./component/partial/search/search-results-customers/search-results-customers.component";
import {SearchResultsQuoteComponent} from "./component/partial/search/search-results-quote/search-results-quote.component";
import { NoteListComponent } from './component/partial/note/note-list/note-list.component';
import { QuoteListComponent } from './component/partial/quote/quote-list/quote-list.component';
import { ContactViewComponent } from './component/module/contact-view/contact-view.component';
import { EventViewComponent } from './component/module/event-view/event-view.component';
import { SearchResultsProspectsComponent } from './component/partial/search/search-results-prospects/search-results-prospects.component';
import { SettingsViewComponent } from './component/module/settings-view/settings-view.component';
import { ConfirmationPromptComponent } from './component/modal/confirmation-prompt/confirmation-prompt.component';
import { SignInPersonComponent } from './component/modal/sign-in-person/sign-in-person.component';
import { CustomerValidatorDirective } from './directive/customer.validator.directive';
import { TextBoxComponent } from './component/modal/text-box/text-box.component';
import { AffixDirective } from './directive/affix.directive';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    SideNavComponent,
    TopNavComponent,
    ProspectViewComponent,
    PhoneFieldsComponent,
    AddressFieldComponent,
    NoteComponent,
    RadioComponent,
    NoteFieldComponent,
    MiscVarFieldComponent,
    ContactsFieldComponent,
    CustomerViewComponent,
    SearchResultsContactsComponent,
    SearchResultsCustomersComponent,
    QuoteViewComponent,
    QuoteServComponent,
    CheckboxComponent,
    EventListComponent,
    EventEditComponent,
    ForwardEventComponent,
    SearchResultsQuoteComponent,
    EmailPromptComponent,
    ContactListComponent,
    AddContactComponent,
    AddContactResultsComponent,
    CalendarFilterComponent,
    PhoneListComponent,
    DeleteConfirmationComponent,
    AddressListComponent,
    UploadComponent,
    SearchViewComponent,
    ResultsComponent,
    FormComponent,
    NoteListComponent,
    QuoteListComponent,
    ContactViewComponent,
    EventViewComponent,
    SearchResultsProspectsComponent,
    SettingsViewComponent,
    ConfirmationPromptComponent,
    SignInPersonComponent,
    CustomerValidatorDirective,
    TextBoxComponent,
    AffixDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    FullCalendarModule,
    TextMaskModule,
    UIRouterModule.forRoot({
      states: STATES,
      useHash: true,
      otherwise: {state: 'events'}
    }),
    MatTableModule,
    MatPaginatorModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    ToastrModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
  ],
  entryComponents: [
    NoteFieldComponent,
    QuoteServComponent,
    EventEditComponent,
    ForwardEventComponent,
    EmailPromptComponent,
    AddContactComponent,
    AddContactResultsComponent,
    CalendarFilterComponent,
    UploadComponent,
    ConfirmationPromptComponent,
    SignInPersonComponent,
    TextBoxComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptors,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
