import { Injectable } from '@angular/core';
import {ApiService} from "./api.service";
import {ModuleViewService} from "./module-view.service";

@Injectable({
  providedIn: 'root'
})
export class SettingsViewService {

  constructor(private viewService: ModuleViewService, private api: ApiService) {}

  getUserSettings(next: Function, error: Function) {
    this.viewService.process(
      this.api.getUserSettings(),
      next,
      error
    );
  }

  save(settings, next: Function, error: Function) {
    this.viewService.process(
      this.api.saveUserSettings(settings),
      next,
      error
    );
  }

}
