import { Injectable } from '@angular/core';
import {ModuleViewService} from "./module-view.service";
import {EventApiService} from "./api/event-api.service";
import {forkJoin} from "rxjs";
import {SoftpakEvent} from "../model/softpak-event";

@Injectable({
  providedIn: 'root'
})
export class EventViewService {

  constructor(public viewService: ModuleViewService, private eventApi: EventApiService) {}

  getEventsView(next: Function, errHandle: Function) {
    this.viewService.process(
      forkJoin(
        this.eventApi.getEventList(),
        this.eventApi.eventOptions()
      ),
      next,
      errHandle
    )
  }
  getFilteredEventsView(filter, next: Function, errHandle: Function) {
    this.viewService.process(
      forkJoin(
        this.eventApi.getFilterEventList(filter),
        this.eventApi.eventOptions()
      ),
      next,
      errHandle
    )
  }


  save(event: SoftpakEvent, next: Function, errHandle: Function) {
    this.viewService.process(
      this.eventApi.save(event),
      next,
      errHandle
    )
  }

  delete(event: SoftpakEvent, next: Function, errHandle: Function) {
    this.viewService.process(
      this.eventApi.deleteEvent(event.eventId),
      next,
      errHandle
    )
  }

}
