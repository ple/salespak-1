import { Injectable } from '@angular/core';
import {ContactApiService} from "./api/contact-api.service";
import {forkJoin} from "rxjs";
import {ModuleViewService} from "./module-view.service";
import {ContactView} from "../model/contact-view";

@Injectable({
  providedIn: 'root'
})
export class ContactViewService {

  constructor(public viewService: ModuleViewService, private contactApi: ContactApiService) { }

  getContactView(id: number, next: Function, errHandle: Function) {
    this.viewService.process(
      forkJoin(
        this.contactApi.getContact(id)),
      next,
      errHandle
    )
  }

  getContactOptions(next: Function, errHandle: Function) {
    this.viewService.process(
      this.contactApi.getOptions(),
      next,
      errHandle
    )
  }

  save(viewObj: ContactView, next: Function, errHandle: Function) {
    this.viewService.process(
      this.contactApi.updateContact(viewObj),
      next,
      errHandle
    )
  }
}
