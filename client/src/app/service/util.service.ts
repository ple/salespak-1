import {Injectable} from '@angular/core';
import {StateService} from '@uirouter/core';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor(private stateService: StateService) {}

  daysOfTheWeek() {
    return [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday'
    ]
  }

  timeOfDay24() {
    return [
      'Midnight',
      '1:00 AM',
      '2:00 AM',
      '3:00 AM',
      '4:00 AM',
      '5:00 AM',
      '6:00 AM',
      '7:00 AM',
      '8:00 AM',
      '9:00 AM',
      '10:00 AM',
      '11:00 AM',
      '12:00 PM',
      '1:00 PM',
      '2:00 PM',
      '3:00 PM',
      '4:00 PM',
      '5:00 PM',
      '6:00 PM',
      '7:00 PM',
      '8:00 PM',
      '9:00 PM',
      '10:00 PM',
      '11:00 PM'
    ]
  }

  states() {
    return {
      "AL": "Alabama",
      "AK": "Alaska",
      "AZ": "Arizona",
      "AR": "Arkansas",
      "CA": "California",
      "CO": "Colorado",
      "CT": "Connecticut",
      "DE": "Delaware",
      "DC": "District Of Columbia",
      "FL": "Florida",
      "GA": "Georgia",
      "HI": "Hawaii",
      "ID": "Idaho",
      "IL": "Illinois",
      "IN": "Indiana",
      "IA": "Iowa",
      "KS": "Kansas",
      "KY": "Kentucky",
      "LA": "Louisiana",
      "ME": "Maine",
      "MD": "Maryland",
      "MA": "Massachusetts",
      "MI": "Michigan",
      "MN": "Minnesota",
      "MS": "Mississippi",
      "MO": "Missouri",
      "MT": "Montana",
      "NE": "Nebraska",
      "NV": "Nevada",
      "NH": "New Hampshire",
      "NJ": "New Jersey",
      "NM": "New Mexico",
      "NY": "New York",
      "NC": "North Carolina",
      "ND": "North Dakota",
      "OH": "Ohio",
      "OK": "Oklahoma",
      "OR": "Oregon",
      "PA": "Pennsylvania",
      "PR": "Puerto Rico",
      "RI": "Rhode Island",
      "SC": "South Carolina",
      "SD": "South Dakota",
      "TN": "Tennessee",
      "TX": "Texas",
      "UT": "Utah",
      "VT": "Vermont",
      "VI": "Virgin Islands",
      "VA": "Virginia",
      "WA": "Washington",
      "WV": "West Virginia",
      "WI": "Wisconsin",
      "WY": "Wyoming"
    }
  }

  provCAN() {
    return {
      "AB": "Alberta",
      "BC": "British Columbia",
      "MB": "Manitoba",
      "NB": "New Brunswick",
      "NL": "Newfoundland and Labrador",
      "NS": "Nova Scotia",
      "NT": "Northwest Territories",
      "NU": "Nunavut",
      "ON": "Ontario",
      "PE": "Prince Edward Island",
      "QC": "Quebec",
      "SK": "Saskatchewan",
      "YT": "Yukon"
    }
  }

  unique(value, index, self) {
    return self.indexOf(value) === index;
  }

  navigateTo(to, id, _params = {}) {
    let params = {
      id,
      mode: 'edit',
      company: ''
    };

    this.stateService.go(to, Object.assign(params, _params))
  }

  mapObject(obj, params = {}) {
    for (let key in obj) {
      if (obj[key] != null) {
        params[key] = obj[key]
      }
    }
    return params;
  };

  serialize(obj: Object): Object {
    let param = {};
    for (let key in obj) {
      if (obj[key] == null) {
        continue;
      } else if (Array.isArray(obj[key])) {
        param[key] = Array.from(obj[key])
      } else if (typeof obj[key] == 'object') {
        param[key] = this.serialize(obj[key])
      } else {
        param[key] = obj[key]
      }
    }

    return param
  }

  queryString(options: Object): string {
    let params = new URLSearchParams();
    for(let key in options){
      params.set(key, options[key])
    }

    return params.toString();
  }

  isFunction(fn) {
    return typeof fn == 'function'
  }

  isNull(val) {
    return val === null
  }

  formatNumberLength(number: number, length: number = 11) {
    if (number.toString().length > length) {
      new Error(`Number cannot be greater than length. number: ${number}, length: ${length}`)
      return;
    }

    let r = "" + number;

    while (r.length < length) {
      r = "0" + r;
    }

    return r;
  }

  blobToFile = (theBlob: Blob, fileName:string): File => {
    let b: any = theBlob;
    b.lastModifiedDate = new Date();
    b.name = fileName;
    return <File>theBlob;
  }
}
