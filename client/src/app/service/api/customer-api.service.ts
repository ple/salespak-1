import { Injectable } from '@angular/core';
import {api} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {UtilService} from "../util.service";
import * as moment from "moment";
import {StatusKey} from "../../model/softpak-event";
import {Note} from "../../model/note";
import dateFormat from "../../model/date-format";

@Injectable({
  providedIn: 'root'
})
export class CustomerApiService {

  private readonly HOST: string = `${api.host}/customer`;

  private eventParams(toDate = '3000-12-31', fromDate = '1900-01-01', status: StatusKey = 'OPEN') {
    let tdate = moment(toDate, dateFormat.simpleFormat),
      fdate = moment(fromDate, dateFormat.simpleFormat);

    if (!tdate.isValid())
      tdate = moment().add(4, 'months');

    if (!fdate.isValid())
      fdate = moment().subtract(4, 'months');

    return this.util.queryString({
      toMonth: tdate.format('MM'),
      toYear: tdate.format('YYYY'),
      fromMonth: fdate.format('MM'),
      fromYear: fdate.format('YYYY'),
      status
    });
  }

  constructor(private http: HttpClient, private util: UtilService) {}

  getCustomerData(company, id) {
    return this.http.get(`${this.HOST}/${company}/${id}`);
  }

  getOldServices(company, id) {
    return this.http.get(`${this.HOST}/services/list/${company}/${id}`);
  }

  getRecentCustomerData(company, id) {
    return this.http.get(`${this.HOST}/recent/${company}/${id}`);
  }

  getEvents(company: string, id: number, toDate = '3000-12-31', fromDate = '1900-01-01', status: StatusKey = 'OPEN') {
    let params = this.eventParams(toDate, fromDate, status);
    return this.http.get(`${this.HOST}/events/${company}/${id}?${params}`);
  }

  deleteEvent(eventId: number, company: string, customerId: number, toDate = '3000-12-31', fromDate = '1900-01-01', status: StatusKey = 'OPEN') {
    let params = this.eventParams(toDate, fromDate, status);
    return this.http.delete(`${this.HOST}/event/delete/${eventId}/${company}/${customerId}?${params}`)
  }

  addCustomerNote(company: string, id: number, note: Note) {
    return this.http.post(`${this.HOST}/note/${company}/${id}`, note);
  }
}
