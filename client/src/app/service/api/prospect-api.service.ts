import { Injectable } from '@angular/core';
import {api} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {UtilService} from "../util.service";
import * as moment from "moment";
import {EventService} from "../event.service";
import {EventServiceModel} from "../../model/event-service-model";
import {SoftpakEvent} from "../../model/softpak-event";
import {ProspectView} from "../../model/prospect-view";
import {ToastrService} from "ngx-toastr";

@Injectable({
  providedIn: 'root'
})
export class ProspectApiService {

  private readonly HOST: string = `${api.host}/prospect`;

  constructor(private http: HttpClient, private util: UtilService, private eventService: EventService, private toast: ToastrService) {}

  getProspect(id: number) {
    return this.http.get(`${this.HOST}/${id}`);
  }

  getOptions() {
    return this.http.get(`${this.HOST}/options`);
  }

  getEvents(id: number, toDate?) {
    let tdate = moment(toDate, 'MM/YYYY'),
      fdate = moment(toDate, 'MM/YYYY');

    if (!tdate.isValid())
      tdate = moment().add(4, 'months');

    if (!fdate.isValid())
      fdate = moment().subtract(4, 'months');

    let params = this.util.queryString({
      toMonth: tdate.format('MM'),
      toYear: tdate.format('YYYY'),
      fromMonth: fdate.format('MM'),
      fromYear: fdate.format('YYYY')
    });

    return this.http.get(`${this.HOST}/events/${id}?${params}`);
  }

  // actionNext: Function = next => data => {
  //   data['events'] = data['events'].filter(e => e.prospectId == this.id);
  //   next(data)
  // };
  //
  // errorHandler: Function = (toastMsg, errHandle) => err => {
  //   this.toast.error(toastMsg);
  //   errHandle(err)
  // };
  //
  // deleteEvent(event: SoftpakEvent, next: Function, errHandle: Function = () => {}) {
  //   this.eventService.deleteEvent(event, this.actionNext(next), this.errorHandler("There was a problem deleting the event", errHandle))
  // }
  //
  // persistEvent(event: SoftpakEvent, next: Function, errHandle: Function = () => {}) {
  //   this.eventService.persistEvent(event, this.actionNext(next), this.errorHandler("There was a problem creating the event", errHandle))
  // }

  updateProspect(viewObj: ProspectView) {
    if (viewObj.id === 0)
      return this.http.post(`${this.HOST}`, viewObj.getSaveObj());

    return this.http.put(`${this.HOST}`, viewObj.getSaveObj());
  }
}
