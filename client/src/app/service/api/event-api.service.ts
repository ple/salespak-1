import { Injectable } from '@angular/core';
import {api} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {UtilService} from "../util.service";
import * as moment from 'moment';
import {SoftpakEvent} from "../../model/softpak-event";

@Injectable({
  providedIn: 'root'
})
export class EventApiService {

  private readonly HOST: string = `${api.host}/event`;

  constructor(private http: HttpClient, private util: UtilService) {}

  /**
   *
   * @param toDate string format MM/YYYY
   */
  getEventList(toDate?) {
    let date = moment(toDate, 'MM/YYYY');

    if (!date.isValid())
      date = moment().add(4, 'months');

    let params = this.util.queryString({toMonth: date.format('MM'), toYear: date.format('YYYY')});

    return this.http.get(`${this.HOST}/list?${params}`);
  }
  getFilterEventList(filter) {
    let date = moment(filter.startDate);
    let endDate =  moment(filter.endDate);
    let params = this.util.queryString({ userId: filter.userId, status: filter.status, type: filter.type,
      toMonth: endDate.format('MM'), toDay: endDate.format('DD'), toYear: endDate.format('YYYY'),
      fromMonth: date.format('MM'), fromDay: date.format('DD'), fromYear: date.format('YYYY'),
      description: filter.description});

    return this.http.get(`${this.HOST}/list?${params}`);
  }

  save(event: SoftpakEvent) {
    if (event.eventId)
      return this.updateEvent(this.util.mapObject(event));
     else
      return this.addEvent(this.util.mapObject(event))
  }

  eventOptions() {
    return this.http.get(`${this.HOST}/options`);
  }

  addEvent(event) {
    return this.http.post(`${this.HOST}/add`, event);
  }

  updateEvent(event) {
    return this.http.post(`${this.HOST}/update`, event);
  }

  deleteEvent(id) {
    return this.http.delete(`${this.HOST}/delete/${id}`)
  }
}
