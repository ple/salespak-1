import { Injectable } from '@angular/core';
import {api} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {UtilService} from "../util.service";
import {ContactView} from "../../model/contact-view";

@Injectable({
  providedIn: 'root'
})
export class ContactApiService {

  private readonly HOST: string = `${api.host}/contact`;

  constructor(private http: HttpClient, private util: UtilService) {}

  getContact(id) {
    return this.http.get(`${this.HOST}/${id}`);
  }

  getRecentContact(id) {
    return this.http.get(`${this.HOST}/recent/${id}`);
  }

  getOptions() {
    return this.http.get(`${this.HOST}/options`);
  }

  updateContact(view: ContactView) {
    if (view.id === 0){
      return this.http.post(`${this.HOST}`, view.getSaveObj());
    }

    return this.http.put(`${this.HOST}`, view.getSaveObj())
  }
}
