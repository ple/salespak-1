import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {UtilService} from "../util.service";
import {api} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class SearchApiService {

  private readonly HOST: string = `${api.host}/search`;

  constructor(private http: HttpClient, private util: UtilService) {}

  client(searchObject) {
    let params = this.util.queryString(searchObject);
    return this.http.get(`${this.HOST}/client?${params}`)
  }

  quote(id: number) {
    return this.http.get(`${this.HOST}/quote/${id}`)
  }

  noteList(noteType: 'EVENT'|'PROSPECT'|'CUSTOMER'|'QUOTE', id: number) {
    return this.http.post(`${this.HOST}/notes/list`, {
      noteType,
      id
    })
  }
}
