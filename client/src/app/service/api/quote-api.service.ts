import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {UtilService} from "../util.service";
import {api} from "../../../environments/environment";
import {Subject} from "rxjs";
import {switchMap} from "rxjs/operators";
import {QuoteView} from "../../model/quote-view";
import {PageService} from "../page.service";
import {Quote} from "../../model/quote";

@Injectable({
  providedIn: 'root'
})
export class QuoteApiService {

  private readonly HOST: string = `${api.host}/quote`;

  constructor(private http: HttpClient, private util: UtilService, private pageService: PageService) {
  }

  private httpReq(_swtMap) {
    const reqObs = new Subject(),
      observable = reqObs
        .pipe(switchMap(_swtMap));

    return {
      next: reqObj => reqObs.next(reqObj),
      subscribe: (next, err?, complete?) => observable.subscribe(next, err, complete)
    };
  }

  getQuoteData(id) {
    return this.http.get(`${this.HOST}/${id}`);
  }

  getList(id: number, company: string = "", customer: number = 0) {
    let params = this.util.queryString({id, company, customer});
    return this.http.get(`${this.HOST}/list?${params}`);
  }

  getOptions(company: string) {
    return this.http.get(`${this.HOST}/fields?company=${company}`)
  }

  deleteQuote(id: number) {
    return this.http.delete(`${this.HOST}/${id}`);
  }

  deleteSignedQuote(id: number) {
    return this.http.delete(`${this.HOST}/upload/${id}`);
  }

  quotePdf(command, company, quoteId, email = '') {
    let params = this.util.queryString({
      command,
      company,
      quoteId,
      email
    });

    return this.http.get(`${this.HOST}/pdf/view?${params}`);
  }

  generateSignRequest(quoteId, fromEmail, redirectUrl, firstName, lastName, signerId, email, company, account) {
    let params = this.util.queryString({
      command: 'VIEW',
      quoteId,
      fromEmail,
      redirectUrl,
      firstName,
      lastName,
      signerId,
      email,
      company,
      account
    });

    return this.http.get(`${this.HOST}/sign-request?${params}`);
  }

  emailPdf(company, quoteId, email) {
    let params = this.util.queryString({
      company,
      quoteId,
      email
    });

    return this.http.get(`${this.HOST}/pdf/email?${params}`);
  }

  downloadSignedQuote(id: number): string {
    return `${this.HOST}/download/signed/${id}`
  }

  uploadQuote(id: number, file: File) {
    let formData = new FormData();
    formData.append('upload', file);

    return this.http.post(`${this.HOST}/upload/${id}`, formData);
  }

  updateStatus(quote) {
    return this.http.put(`${this.HOST}/${quote.quoteId}`, this.util.serialize({
      quoteData: quote,
      deletedOperationsNotes: [],
      deletedContractNotes: []
    }))
  }

  updateQuote(viewObj: QuoteView) {
    if (viewObj.id === 0)
      return this.http.post(`${this.HOST}`, this.util.serialize(viewObj.getSaveObj()));

    return this.http.put(`${this.HOST}/${viewObj.quote.quoteId}`, this.util.serialize(viewObj.getSaveObj()));
  }

  //
  // createQuote() {
  //   return this.httpReq(createObj => {
  //     return this.http.post(`${this.HOST}/quote`, this.util.mapObject(createObj))
  //   })
  // }
  //

  //

  //
  // deleteSignedQuote(id: number) {
  //   return this.http.delete(`${this.HOST}/upload/quote/${id}`);
  // }
  //

  //
  // quoteSearch() {
  //   return this.httpReq(id => {
  //     return this.http.get(`${this.HOST}/search/quote/${id}`)
  //   })
  // }

}
