import {Injectable} from '@angular/core';
import {ApiService} from "./api.service";
import {forkJoin} from "rxjs";
import {UserSettings} from "../model/user-settings";

@Injectable({
  providedIn: 'root'
})
export class PageService {

  loading: boolean = true;
  dataSet = '';
  userName = '';
  settings = new UserSettings();
  email = '';
  resolved = false;

  constructor(private api: ApiService) {

    forkJoin(
      this.api.getUserInfo(),
      this.api.getUserSettings()
    ).subscribe(
      ([userInfo, userSettings]) => {
        this.dataSet = userInfo['dataset'];
        this.userName = userInfo['username'];
        this.settings = Object.assign(this.settings, userSettings['settings']);
        this.email = userSettings['email'];
        this.resolved = true;
      }, error => {

      }
    );
  }

  toggleLoading() {
    this.loading = !this.loading;
  }
}
