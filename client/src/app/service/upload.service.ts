import { Injectable } from '@angular/core';
import {ErrorLoggingService} from "./error-logging.service";

@Injectable({
  providedIn: 'root'
})
export class UploadService {


  constructor(private errorLogging: ErrorLoggingService) {}

  fileChange(event, next: Function = () => {}): File {
    let files = event.target.files;

    if (files.length == 0) {
      this.errorLogging.notify(new Error("UploadService.fileChange: No file selected"));
      console.log("UploadService.fileChange: No file selected");
      return
    }

    next(files[0]);

    return files[0];
  }
}
