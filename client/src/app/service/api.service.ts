import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {api} from '../../environments/environment';
import {Subject} from "rxjs/internal/Subject";
import {switchMap} from "rxjs/operators";
import {UtilService} from "./util.service";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private host: string = api.host;

  constructor(private http: HttpClient, private util: UtilService) {}

  private httpReq(_swtMap) {
    const reqObs = new Subject(),
      observable = reqObs
        .pipe(switchMap(_swtMap));

    return {
      next: reqObj => reqObs.next(reqObj),
      subscribe: (next, err?, complete?) => observable.subscribe(next, err, complete)
    };
  }

  getUserInfo() {
    return this.http.get(`${this.host}/user/info`)
  }

  getUserSettings() {
    return this.http.get(`${this.host}/user/settings`)
  }

  saveUserSettings(settings) {
    return this.http.post(`${this.host}/user/settings`, settings)
  }
}

