import { Injectable } from '@angular/core';
import {ModuleViewService} from "./module-view.service";
import {CustomerApiService} from "./api/customer-api.service";
import {forkJoin} from "rxjs";
import {EventApiService} from "./api/event-api.service";
import {SoftpakEvent} from "../model/softpak-event";
import {Note} from "../model/note";
import {CustomerView} from "../model/customer-view";
import {QuoteApiService} from "./api/quote-api.service";

@Injectable({
  providedIn: 'root'
})
export class CustomerViewService {

  constructor(public viewService: ModuleViewService, public customerApi: CustomerApiService, private eventApi: EventApiService, private quoteApi: QuoteApiService) {}

  getCustomerView(id: number, company: string, next: Function, errHandle: Function) {
    this.viewService.process(
      forkJoin(
        this.customerApi.getCustomerData(company, id),
        this.customerApi.getEvents(company, id),
        this.eventApi.eventOptions(),
        this.quoteApi.getList(0, company, id)
      ),
      next,
      errHandle
    )
  }

  deleteEvent(view: CustomerView, event: SoftpakEvent, next: Function, errHandle: Function = () => {}) {
    this.viewService.process(
      this.customerApi.deleteEvent(event.eventId, view.company, view.id),
      data => {
        data['events'] = data['events'].filter(e => e.ownerType == 'CUSTOMER');
        next(data)
      },
      errHandle
    )
  }

  saveEvent(customerId: number, event: SoftpakEvent, next: Function, errHandle: Function = () => {}) {
    this.viewService.process(
      this.eventApi.save(event),
      data => {
        data['events'] = data['events'].filter(e => e.ownerType == 'CUSTOMER');
        next(data)
      },
      errHandle
    )
  }

  addCustomerNote(company: string, id: number, note: Note, next: Function, errHandle: Function) {
    this.viewService.process(
      this.customerApi.addCustomerNote(company, id, note),
      next,
      errHandle
    )
  }

  deleteQuotes(deletedQuoteList: Array<number>, next: Function, errHandle: Function = () => {}) {
    this.viewService.process(
      forkJoin(deletedQuoteList.map(quoteId => this.quoteApi.deleteQuote(quoteId))),
      next,
      errHandle
    )
  }
}
