import {Injectable} from '@angular/core';
import {ModuleViewService} from "./module-view.service";
import {ProspectApiService} from "./api/prospect-api.service";
import {forkJoin} from "rxjs";
import {QuoteApiService} from "./api/quote-api.service";
import {ProspectView} from "../model/prospect-view";
import {EventApiService} from "./api/event-api.service";
import {SoftpakEvent} from "../model/softpak-event";

@Injectable({
  providedIn: 'root'
})
export class ProspectViewService {

  constructor(public viewService: ModuleViewService, public prospectApi: ProspectApiService, private eventApi: EventApiService, private quoteApi: QuoteApiService) {}

  getProspectView(id: number, next: Function, errHandle: Function) {
    this.viewService.process(
      forkJoin(
        this.prospectApi.getProspect(id),
        this.prospectApi.getOptions(),
        this.prospectApi.getEvents(id),
        this.eventApi.eventOptions(),
        this.quoteApi.getList(id)),
      next,
      errHandle
    )
  }

  getProspect(id: number, next: Function, errHandle: Function) {
    this.viewService.process(
      this.prospectApi.getProspect(id),
      next,
      errHandle
    )
  }

  newProspect(next: Function, errHandle: Function) {
    this.viewService.process(
      forkJoin(
        this.prospectApi.getOptions()),
      next,
      errHandle
    )
  }

  save(viewObj: ProspectView, next: Function, errHandle: Function) {
    let jobs: Array<any> = this.deleteQuotes(viewObj.deletedQuoteList);
    jobs.push(this.prospectApi.updateProspect(viewObj));

    this.viewService.process(
      forkJoin(jobs),
      next,
      errHandle
    )
  }

  deleteEvent(prospectId: number, event: SoftpakEvent, next: Function, errHandle: Function = () => {}) {
    this.viewService.process(
      this.eventApi.deleteEvent(event.eventId),
      data => {
        data['events'] = data['events'].filter(e => e.ownerType == 'PROSPECT');
        next(data)
      },
      errHandle
    )
  }


  deleteQuotes(deletedQuoteList: Array<number>): Array<any> {
    return deletedQuoteList.map(quoteId => this.quoteApi.deleteQuote(quoteId))
  }

  saveEvent(prospectId: number, event: SoftpakEvent, next: Function, errHandle: Function = () => {}) {
    this.viewService.process(
      this.eventApi.save(event),
      data => {
        data['events'] = data['events'].filter(e => e.ownerType == 'PROSPECT');
        next(data)
      },
      errHandle
    )
  }
}
