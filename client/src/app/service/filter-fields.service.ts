import { Injectable } from '@angular/core';
import {SoftpakEvent} from "../model/softpak-event";
import * as moment from "moment";
import {PageService} from "./page.service";
const newDateFormat = 'MM/DD/YYYY hh:mm:ss a'
const timeStampFormat = 'YYYY-MM-DD-HH.mm.ss.SSSSSS'

@Injectable({
  providedIn: 'root'
})

export class FilterFieldsService {
  startDate: string = moment().subtract(1, 'month').minute(0).second(0).format(newDateFormat);
  endDate: string = moment().add(4, 'month').minute(0).second(0).format(newDateFormat);
  status: string = '';
  type: string = '';
  description: string = '';
  userId: string;

  constructor(public pageService: PageService){
    this.userId = this.pageService.userName;
  }

  test(event: SoftpakEvent): boolean {
    if (!(this.startTimestamp <= event.startTimestamp)) {
      return false;
    }

    if (!(this.endTimestamp >= event.endTimestamp)) {
      return false;
    }

    if (this.status != '' && !(this.status == event.status.toUpperCase()) ) {
      return false;
    }

    if (this.type != '' && !(this.type == event.type)) {
      return false
    }

    if (this.description != '' && !(event.description.includes(this.description))) {
      return false
    }

    if (this.userId != '' && !(this.userId == event.eventUser)) {
      return false
    }
    return true;
  }

  get startTimestamp() {
    let time: moment.Moment = moment(this.startDate);

    if (!time.isValid())
      time = moment();

    return time.format(timeStampFormat);
  }


  get endTimestamp() {
    let time: moment.Moment = moment(this.endDate);

    if (!time.isValid())
      time = moment();

    return time.format(timeStampFormat);
  }
}
