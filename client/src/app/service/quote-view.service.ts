import {Injectable} from '@angular/core';
import {ModuleViewService} from "./module-view.service";
import {QuoteApiService} from "./api/quote-api.service";
import {concat, forkJoin} from "rxjs";
import {QuoteView} from "../model/quote-view";
import {ProspectApiService} from "./api/prospect-api.service";
import {CustomerApiService} from "./api/customer-api.service";

@Injectable({
  providedIn: 'root'
})
export class QuoteViewService {

  constructor(public viewService: ModuleViewService, public quoteApi: QuoteApiService, public prospectApi: ProspectApiService, public customerApi: CustomerApiService) {}

  getQuoteView(quoteId: number, prospectId: number, next: Function, errHandle: Function) {
    this.viewService.process(
      forkJoin(
        this.quoteApi.getQuoteData(quoteId),
        this.prospectApi.getProspect(prospectId)
      ),
      next,
      errHandle
    )
  }

  getCustomerQuoteView(quoteId: number, accountId: number, company: string, next: Function, errHandle: Function) {
    this.viewService.process(
      forkJoin(
        this.quoteApi.getQuoteData(quoteId),
        this.customerApi.getCustomerData(company, accountId)
      ),
      next,
      errHandle
    )
  }

  newQuote(company: string, prospectId: number, next: Function, errHandle: Function) {
    this.viewService.process(
      forkJoin(
        this.quoteApi.getOptions(company),
        this.prospectApi.getProspect(prospectId)
      ),
      next,
      errHandle
    )
  }

  newQuoteCustomer(company: string, customerId: number, next: Function, errHandle: Function) {
    this.viewService.process(
      forkJoin(
        this.quoteApi.getOptions(company),
        this.customerApi.getCustomerData(company, customerId),
        this.customerApi.getOldServices(company, customerId)
      ),
      next,
      errHandle
    )
  }

  emailPdf(command, company, email, quote, next: Function, errHandle: Function) {
    this.viewService.process(
      concat(
        this.quoteApi.emailPdf(company, quote.quoteId, email),
        this.quoteApi.updateStatus(quote))
        .pipe(),
      next,
      errHandle
    )
  }

  getQuote(id: number, next: Function, errHandle: Function) {
    this.viewService.process(
      this.quoteApi.getQuoteData(id),
      next,
      errHandle
    )
  }

  save(viewObj: QuoteView, next: Function, errHandle: Function) {
    this.viewService.process(
      this.quoteApi.updateQuote(viewObj),
      next,
      errHandle
    )
  }

}
