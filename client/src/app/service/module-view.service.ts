import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {UtilService} from "./util.service";
import {ToastrService} from "ngx-toastr";
import {ErrorLoggingService} from "./error-logging.service";
import {PageService} from "./page.service";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ModuleViewService {

  constructor(public http: HttpClient,
              public util: UtilService,
              public errorLogging: ErrorLoggingService,
              public pageService: PageService,
              public toast: ToastrService) {
  }

  process<T>(obj: Observable<T>, next: Function, errHandle: Function) {
    this.pageService.loading = true;
    obj.subscribe(
      data => {
        this.pageService.loading = false;
        next(data)
      }, err => {
        this.pageService.loading = false;
        this.errorLogging.notify(err);
        errHandle(err)
      }
    )
  }

}
