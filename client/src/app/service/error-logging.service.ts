import { Injectable } from '@angular/core';
import bugsnag from "bugsnag-js";
import {bugsnag as bugsnagConfig, environment} from "../../environments/environment";
import {PageService} from "./page.service";

@Injectable({
  providedIn: 'root'
})
export class ErrorLoggingService {

  constructor(private pageService: PageService) {}

  private bugsnagClient = bugsnag({
    apiKey: bugsnagConfig.token,
    appVersion: environment.appVersion,
    autoCaptureSessions: true,
    releaseStage: bugsnagConfig.releaseStage,
    notifyReleaseStages: ['stage', 'production'],
    maxEvents: 50,
    logger: null,
    beforeSend: (report) => {
      if (report.errorClass === 'Error' && report.severity === 'info') {
        report.errorClass = report.errorMessage
      }
    }
  });

  notify(err) {
    this.bugsnagClient.user = {
      name: this.pageService.userName,
      email: this.pageService.email,
      dataset: this.pageService.dataSet
    };

    this.bugsnagClient.notify(err);
  }
}
