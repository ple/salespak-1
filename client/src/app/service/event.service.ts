import {Injectable} from '@angular/core';
import {UtilService} from "./util.service";
import {SoftpakEvent} from "../model/softpak-event";
import {EventApiService} from "./api/event-api.service";
import {CalendarFilter} from "../model/calendar-filter";

@Injectable({
  providedIn: 'root'
})
export class EventService {

  filter;

  constructor(private eventApi: EventApiService, protected util: UtilService) {}

  getFilteredEvents(allEvents: Array<SoftpakEvent>, filter: CalendarFilter) {
    this.eventApi.getFilterEventList(filter);
  }
  // getFilteredEvents(allEvents: Array<SoftpakEvent>, filter: CalendarFilter) {
  //   this.eventApi.getFilterEventList(filter);
  // }

  forwardEvent(event) {
    this.eventApi.save(event).subscribe(() => {}, () => {})
  }

  getEventById(allEvents: Array<SoftpakEvent>, id) {
    let index = allEvents.findIndex(e => e.eventId == id)
    return allEvents[index];
  }
}
