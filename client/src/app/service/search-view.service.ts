import {Injectable} from '@angular/core';
import {ModuleViewService} from "./module-view.service";
import {SearchView} from "../model/search-view";
import {SearchApiService} from "./api/search-api.service";
import {forkJoin} from "rxjs";
import {ProspectApiService} from "./api/prospect-api.service";

@Injectable({
  providedIn: 'root'
})
export class SearchViewService {

  searchStatus: string;

  constructor(public viewService: ModuleViewService, private searchApi: SearchApiService, private prospectApi: ProspectApiService) {
    this.noResultStatus();
  }

  searchClient(searchView: SearchView, next: Function, errHandler: Function) {
    this.viewService.process(
      this.searchApi.client(searchView.sf),
      next,
      errHandler
    )
  }

  searchQuote(searchView: SearchView, next: Function, errHandler: Function) {
    this.viewService.process(
      this.searchApi.quote(searchView.sf.quoteId),
      next,
      errHandler
    )
  }

  getSearchStatus() {
    return this.searchStatus;
  }
  noResultStatus() {
    this.searchStatus = 'There are no results to display';
  }

  getCompanies(next: Function, errHandle: Function) {
    this.viewService.process(
      forkJoin(
        this.prospectApi.getOptions()),
      next,
      errHandle
    )
  }
}
