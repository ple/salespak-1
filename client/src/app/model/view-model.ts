export abstract class ViewModel {

  setOptions(_options, optionsObj) {
    Object.keys(_options).forEach(obj => {
      switch(obj) {
        case 'sales' : optionsObj[obj] = _options[obj].map(code => ({value: code.userid, display: code.name})); break;
        case 'users' : optionsObj[obj] = _options[obj].map(code => ({value: code.userid, display: code.name})); break;
        case 'companies' : optionsObj[obj] = _options[obj].map(code => ({value: code.company, display: `${code.company} ${code.companyName}`})); break;
        case 'miscVariables' : optionsObj[obj] = _options[obj]; break;
        default : optionsObj[obj] = _options[obj].map(code => ({value: code.type, display: code.description}));
      }
    })
  }

  unique(value, index, self) {
    return self.indexOf(value) === index;
  }
}
