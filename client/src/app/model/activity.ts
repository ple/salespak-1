export class Activity {
  arrive: string;
  bin: string;
  charge: string;
  date: string;
  day: string;
  servDelayDescription: string;
  depart: string;
  driver: string;
  load: string;
  quantity: string;
  remark: string;
  route: string;
  size: string;
  source: string;
  vehicle: string;
  weight: string;
  workOrder: string;
}
