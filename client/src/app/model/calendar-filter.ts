import * as moment from 'moment';
import {SoftpakEvent} from "./softpak-event";
import {PageService} from "../service/page.service";
import dateFormat from './date-format';

const {newDateFormat, timeStampFormat} = dateFormat;

export class CalendarFilter {
  startDate: string = moment().subtract(1, 'month').minute(0).second(0).format(newDateFormat);
  endDate: string = moment().add(1, 'month').minute(0).second(0).format(newDateFormat);
  status: string = '';
  type: string = '';
  description: string = '';
  userId: string;

  constructor(public pageService: PageService){
    this.userId = this.pageService.userName;
  }

  test(event: SoftpakEvent): boolean {
    if (!(this.startTimestamp <= event.startTimestamp)) {
      return false;
    }

    if (!(this.endTimestamp >= event.endTimestamp)) {
      return false;
    }

    if (this.status != '' && !(this.status == event.status.toUpperCase()) ) {
      return false;
    }

    if (this.type != '' && !(this.type == event.type)) {
      return false
    }

    if (this.description != '' && !(event.description.includes(this.description))) {
      return false
    }

    if (this.userId != '' && !(this.userId == event.eventUser)) {
      return false
    }
    return true;
  }

  get startTimestamp() {
    let time: moment.Moment = moment(this.startDate);

    if (!time.isValid())
      time = moment();

    return time.format(timeStampFormat);
  }


  get endTimestamp() {
    let time: moment.Moment = moment(this.endDate);

    if (!time.isValid())
      time = moment();

    return time.format(timeStampFormat);
  }


}
