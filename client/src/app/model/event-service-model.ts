import {SoftpakEvent} from "./softpak-event";

export interface EventServiceModel {
  persistEvent(event: SoftpakEvent, next: Function, errHandle: Function);
  deleteEvent(event: SoftpakEvent, next: Function, errHandle: Function);
  id: number;
}
