import {Contact} from "./contact";
import {ViewModel} from "./view-model";

export class ContactView extends ViewModel {
  id: number = 0;
  contact: Contact = new Contact();
  options = {};
  references: Array<any> = [];

  deletedPhoneList: Array<number> = [];
  deletedNoteList: Array<number> = [];
  deletedAddressList: Array<number> = [];

  getOptions(key) {
    return this.options.hasOwnProperty(key) ? this.options[key] : []
  }

  getSaveObj() {
    this.contact.companyName = this.contact.businessName;
    this.contact.miscVariables = this.getOptions('miscVariables');

    if (this.id === 0)
      return this.contact;

    return {
      contactData: this.contact,
      deletedAddressList: this.deletedAddressList.filter(this.unique),
      deletedPhoneList: this.deletedPhoneList.filter(this.unique),
      deletedNotesList: this.deletedNoteList.filter(this.unique)
    };
  }
}
