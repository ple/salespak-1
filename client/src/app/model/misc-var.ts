export class MiscVar {
  varValueId: number = 0;
  listOfValues: Array<Value> = [];
  value: string = '';
  required: boolean = false;
  varDescription: string = '';
  validate: boolean = false;
}

class Value {
  code: string = '';
  description: string = '';
  type: string = '';
}
