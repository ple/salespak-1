import {Note} from "./note";
import {Phone} from "./phone";
import {Address} from "./address";

export abstract class User {
  public phoneList: Array<Phone> = [];
  public notesList: Array<Note> = [];
  public addressList: Array<Address> = [];
  public businessName: string = '';
  public companyName: string = '';
  public type: string = '';
}
