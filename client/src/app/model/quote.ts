import {User} from "./user";
import {Note} from "./note";
import {QuoteService} from "./quote-service";
import * as moment from 'moment';
import dateFormat from "./date-format";

const simpleDateFormat = dateFormat.simpleFormat,
  newDateFormat = dateFormat.newDateFormat;

export class Quote extends User {
  prospectId: number = 0;
  fileName: string = "";
  company: string = '';
  customer: number = 0;
  quoteId: number = 0;
  status: string = '0';
  quoteType: string = "";
  quoteName: string = "";
  priceIncreaseMonth: number = 0;
  priceIncreaseYear: number = 0;
  _serviceStartDate: string = moment().format(newDateFormat);
  _billingStartDate: string = moment().format(newDateFormat);
  _effectiveDate: string = moment().format(newDateFormat);
  _expirationDate: string = moment().format(newDateFormat);

  _priceIncreaseMax: number = 0;
  priceIncreaseType: string = "$";
  newServices: Array<QuoteService> = [];
  oldServices: Array<QuoteService> = [];
  deletedServices = Array<any>();

  readOnly: boolean = false;

  set priceIncreaseMax(priceIncreaseMax: string) {
    this._priceIncreaseMax = parseFloat(priceIncreaseMax);
  }

  get priceIncreaseMax() {
    return `${this._priceIncreaseMax}`
  }

  set serviceStartDate(serviceStartDate) {
    this._serviceStartDate = serviceStartDate;
  }

  get serviceStartDate() {
    return moment(this._serviceStartDate).format(simpleDateFormat);
  }

  set billingStartDate(billingStartDate) {
    this._billingStartDate = billingStartDate;
  }

  get billingStartDate() {
    return moment(this._billingStartDate).format(simpleDateFormat);
  }

  set effectiveDate(effectiveDate) {
    this._effectiveDate = effectiveDate;
  }

  get effectiveDate() {
    return moment(this._effectiveDate).format(simpleDateFormat);
  }

  set expirationDate(expirationDate) {
    this._expirationDate = expirationDate;
  }

  get expirationDate() {
    return moment(this._expirationDate).format(simpleDateFormat);
  }

  set quoteDate(q) {
  }

  get quoteDate() {
    return null;
  }

  operationsNotes: Array<Note> = [];
  contractNotes: Array<Note> = [];

}
