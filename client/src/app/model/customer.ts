import {User} from "./user";

export class Customer extends User {
  billingName: string = '';
  serviceName: string = '';
  account: string = '';
  age01: number = 0;
  age30: number = 0;
  age60: number = 0;
  age90: number = 0;
  unapplied: number = 0;
  balanceDue: number = 0;
  cancelled: boolean = false;
  lockedDelinquent: boolean = false;

  get serviceNameDisplay() {
    return this.serviceName;
  }
}
