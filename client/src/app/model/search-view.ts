import {SearchField} from "./search-field";
import {SearchResultFields} from "./search-result-fields";
import {BsModalRef} from "ngx-bootstrap";
import {Optional} from "@angular/core";
import {LinkedAccount} from "./linked-account";
import {ViewModel} from "./view-model";

export class SearchView extends ViewModel {
  sf: SearchField;
  sr: SearchResultFields;
  results: Array<any> = [];
  searchTypeDisplay: boolean = true;
  display: string = 'contacts';
  actionTemplate: string = '';
  customCb: Function;
  linkedAccount: LinkedAccount;
  canEditCompany: boolean = true;
  options = {};

  constructor(sf: SearchField = new SearchField(), sr: SearchResultFields = new SearchResultFields()) {
    super();
    this.sf = sf;
    this.sr = sr;
  }

  clearResults() {
    this.results = [];
  }

  clearForm(type) {
    this.sf = new SearchField(type);
  }

  static searchType(): Object {
    return {
      'CONTACTS': 'Contact',
      'PROSPECTS': 'Prospect',
      'CUSTOMERS': 'Customer',
      'QUOTES': 'Quote'
    }
  }

  typeMap(type) {
    switch (type) {
      case 'PROSPECTS' : return 'prospect-edit';
      case 'CONTACTS' : return 'contact-edit';
      case 'CUSTOMERS' : return 'customer';
      case 'QUOTE' : return 'quote';
      default : new Error('SearchView: Type unknown');
    }
  }

  getOptions(key) {
    return this.options.hasOwnProperty(key) ? this.options[key] : []
  }
}
