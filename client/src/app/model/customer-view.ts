import {Customer} from "./customer";
import {Pricing} from "./pricing";
import {Activity} from "./activity";
import {SoftpakEvent} from "./softpak-event";

export class CustomerView {

  id: number;
  company: string;
  events: Array<any> = [];
  eventOptions = {
    eventType: [],
    source: {},
    user: {},
    users: []
  };
  quotes: Array<any> = [];
  quoteOptions = {};
  customer: Customer = new Customer();
  contacts: Array<any> = [];
  customerReport: Array<string> = [];
  monthlyCharges: Array<any> = [];
  pricing: Pricing = new Pricing();
  activity: Array<Activity> = [];
  references: Array<any> = [];
  hasAddNoteAccess: boolean = true;
  deletedPhoneList: Array<number> = [];
  deletedNoteList: Array<number> = [];
  deletedQuoteList: Array<number> = [];

  setEvents(events: Array<any>, eventOptions, userid) {
    this.events = events
      .filter(e => e.userid == userid)
      .map(e => Object.assign(new SoftpakEvent(), e));
    this.eventOptions = {
      eventType: eventOptions['event'],
      source: eventOptions['srcevent'],
      user: eventOptions['user'],
      users: eventOptions['users']
    };
  }

  setQuoteOptions(fields) {
    this.quoteOptions = {
      descriptionList: fields['descriptionList'].map(obj => ({
        value: obj.value,
        display: obj.label,
        capacity: obj.capacity,
        size: obj.size,
        charge: obj.charge
      })),
      frequencyList: fields['frequencyList'].map(obj => ({value: obj.value, display: obj.label, numberOfPickups: obj.numberOfPickups})),
      unitList: fields['unitList'].map(obj => ({value: obj.value, display: obj.label})),
      quoteStatuses: fields['quoteStatuses'].map(obj => ({value: obj.value, display: obj.label})),
    }
  }
}
