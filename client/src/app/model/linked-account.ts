export class LinkedAccount {
  selectAccount: Function;
  searchTypeOpt =  {
    'PROSPECTS': 'Prospect',
    'CUSTOMERS': 'Customer',
  };

  constructor(selectAccount = account => {
    console.log('LinkedAccount:selectAccount:noop')
  }) {
    this.selectAccount = selectAccount;
  }
}
