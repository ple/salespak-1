export class QuoteService {
  quantity: number = 0;
  description: string = "";
  size: string = "";
  capacity: string = "";
  charge: string = '';
  lineNumber: number = 0;
  material: string = "";
  frequency: string = "";
  monday: boolean = false;
  tuesday: boolean = false;
  wednesday: boolean = false;
  thursday: boolean = false;
  friday: boolean = false;
  saturday: boolean = false;
  sunday: boolean = false;
  rate: string = "";
  unit: string = "MONTHLY";
  total: string = "";
  included: number = 0;
  memo: string = "";
  serviceType = 'NEW';

  get dateCount(): number {
    let count:number = 0;
    if(this.monday) count++;
    if(this.tuesday) count++;
    if(this.wednesday) count++;
    if(this.thursday) count++;
    if(this.friday) count++;
    if(this.saturday) count++;
    if(this.sunday) count++;
    return count;
  }
}
