export class Phone {
  public primaryPhone: boolean = false;
  public phoneType: string = 'HOME';
  public extension: string = '';
  public phoneNumber: string = '';
  public _phoneNumber: string = '';
  public phoneId: number = 0;

  static readonly PHONEMASK = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
}
