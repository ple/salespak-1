import {Prospect} from "./prospect";
import {SoftpakEvent} from "./softpak-event";
import {ViewModel} from "./view-model";

export class ProspectView extends ViewModel {
  id: number = 0;
  prospect: Prospect = new Prospect();
  oriProspectHash;
  contacts: Array<any> = [];
  options = {};
  events: Array<SoftpakEvent> = [];
  eventOptions = {
    eventType: [],
    source: {},
    user: {},
    users: []
  };
  quotes: Array<any> = [];
  quoteOptions = {};

  canEditCompany: boolean = true;
  canAddQuotes: boolean = false;

  deletedPhoneList: Array<number> = [];
  deletedNoteList: Array<number> = [];
  deletedQuoteList: Array<number> = [];
  deletedAddressList: Array<number> = [];
  deletedContactList: Array<number> = [];

  setEvents(events: Array<any>, eventOptions) {
    this.events = events.map(e => Object.assign(new SoftpakEvent(), e));
    this.eventOptions = {
      eventType: eventOptions['event'],
      source: eventOptions['srcevent'],
      user: eventOptions['user'],
      users: eventOptions['users']
    };
  }

  getOptions(key) {
    return this.options.hasOwnProperty(key) ? this.options[key] : []
  }

  setQuoteOptions(fields) {
    this.quoteOptions = {
      descriptionList: fields['descriptionList'].map(obj => ({
        value: obj.value,
        display: obj.label,
        capacity: obj.capacity,
        size: obj.size,
        charge: obj.charge
      })),
      frequencyList: fields['frequencyList'].map(obj => ({value: obj.value, display: obj.label, numberOfPickups: obj.numberOfPickups})),
      unitList: fields['unitList'].map(obj => ({value: obj.value, display: obj.label})),
      quoteStatuses: fields['quoteStatuses'].map(obj => ({value: obj.value, display: obj.label})),
    }
  }

  getSaveObj() {
    this.prospect.companyName = this.prospect.businessName;

    if (this.id === 0)
      return this.prospect;

    return {
      prospectData: this.prospect,
      deletedAddressList: this.deletedAddressList.filter(this.unique),
      deletedContactList: this.deletedContactList.filter(this.unique),
      deletedPhoneList: this.deletedPhoneList.filter(this.unique),
      deletedNotesList: this.deletedNoteList.filter(this.unique)
    };
  }
}
