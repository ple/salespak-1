import {ViewModel} from "./view-model";
import {Quote} from "./quote";
import {Prospect} from "./prospect";
import {Date} from "./date";
import * as moment from "moment";
import {Customer} from "./customer";

export class QuoteView extends ViewModel {
  id: number = 0;
  quote: Quote = new Quote();
  prospect: Prospect = new Prospect();
  customer: Customer = new Customer();
  module: 'prospect'|'customer';
  accountId: number;
  options = {
    descriptionList: [],
    frequencyList: [],
    unitList: [],
    quoteStatuses: [],
  };

  deletedOperationsNotes = [];
  deletedContractNotes = [];

  priceIncreaseYearOptions: Array<Date> = [];

  constructor() {
    super();

    this.setPriceIncreaseYearOptions();
  }

  setPriceIncreaseYearOptions() {
    this.priceIncreaseYearOptions = [];
    let now = new Date(),
      start = Math.min(this.quote.priceIncreaseYear, now.getYearInt() - this.quote.priceIncreaseYear, 0);

    for (let x = start; x < 15 + start; x++) {
      let tmpDate = new Date();

      if (this.quote.priceIncreaseYear) {
        tmpDate = new Date(moment(this.quote.priceIncreaseYear, 'YY'))
      }

      tmpDate.addYears(x);
      this.priceIncreaseYearOptions.push(tmpDate);
    }
  }

  setOptions(fields) {
    this.options = {
      descriptionList: fields['descriptionList'].map(obj => ({
        value: obj.value,
        display: obj.label,
        capacity: obj.capacity,
        size: obj.size,
        charge: obj.charge
      })),
      frequencyList: fields['frequencyList'].map(obj => ({value: obj.value, display: obj.label, numberOfPickups: obj.numberOfPickups})),
      unitList: fields['unitList'].map(obj => ({value: obj.value, display: obj.label})),
      quoteStatuses: fields['quoteStatuses'].map(obj => ({value: obj.value, display: obj.label})),
    }
  }

  getSaveObj() {
    if (this.id === 0)
      return this.quote;

    return {
      quoteData: this.quote,
      deletedOperationsNotes: this.deletedOperationsNotes.filter(this.unique),
      deletedContractNotes: this.deletedContractNotes.filter(this.unique)
    };
  }
}
