import * as moment from 'moment';
import dateFormat from "./date-format";

const newDateFormat = dateFormat.newDateFormat;

export class Note {
  public note: string = '';
  // public updatedBy: string = '';
  public createdBy: string = '';
  public createdTimestamp: string = moment().format(newDateFormat);
  public noteCode: string = '';
  public noteId: number = 0;
  public lineNumber: number = 0;
  public noteTimestamp: string = moment().format(newDateFormat);
  // public updatedTimestamp: string = ''
}
