import * as moment from 'moment';
import dateFormat from './date-format';

type YearKey = 'YYYY' | 'YY';
type MonthKey = 'M' | 'MM' | 'MMM' | 'MMMM';
type DayKey = 'D' | 'DD' | 'Do';


export class Date {
  date: moment.Moment;

  constructor(date: moment.Moment = moment()) {
    this.date = date;
  }

  getYear(format: YearKey = 'YY'): string {
    return this.date.format(format)
  }

  getYearInt(format: YearKey = 'YY'): number {
    return parseInt(this.date.format(format))
  }

  getMonth(format: MonthKey = 'MM'): string {
    return this.date.format(format)
  }

  getDay(format: DayKey = 'DD'): string {
    return this.date.format(format)
  }

  addYears(num: number) {
    this.date = this.date.add(num, "years")
  }

  setYear(year: number) {
    this.date.year(year);
  }

  clone(): Date {
    return new Date(this.date)
  }

}
