import {ViewModel} from "./view-model";
import {SoftpakEvent} from "./softpak-event";

export class EventView extends ViewModel {
  events: Array<SoftpakEvent> = [];
  allEvents: Array<SoftpakEvent> = [];
  calEvents = [];
  options = {
    eventType: [],
    source: {},
    user: {},
    users: []
  };

  setEvents(events: Array<any>) {
    this.allEvents = events.map(e => {
      let event = Object.assign(new SoftpakEvent(), e)
      event.eventUser = e.userid;
      return event;
    });
    this.calEvents = this.allEvents.map(event => SoftpakEvent.toCalEvent(event));
    this.events = this.allEvents.filter(e => e.status == '');
  }

  setOptions(options) {
    this.options = {
      eventType: options['event'],
      source: options['srcevent'],
      user: options['user'],
      users: options['users']
    };
  }
}
