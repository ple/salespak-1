export class UserSettings {
  calendarFirstDay: number = 0;
  calendarFirstHour: number = 0;
  defaultSearchType: string = 'CONTACTS';
  sortNotesDescending: boolean = false;
  userid: string = '';
  showWeekends: boolean = false;
  //isUS: boolean = true;
}
