import * as moment from 'moment';

export class EventStream {
  constructor(
    public name: string,
    public event: string,
    private _icon: string,
    public timeStamp: string,
  ) {
  }

  get icon() {
    switch (this._icon) {
      case 'PHONE' :
        return 'fa fa-phone';
      case 'MEETING' :
        return 'fa fa-comments';
      case 'PERSONAL' :
        return 'fa fa-user-circle';
      case 'TASK' :
        return 'fa fa-list';
      case 'OTHER' :
        return 'fa fa-paper-plane';
      default :
        return 'fa fa-circle'
    }
  }

  get timeStampDisplay() {
    let ts = moment(this.timeStamp);

    if (ts.isValid())
      return moment(this.timeStamp).format('MM/DD/YY')

    return 'Unknown Date'
  }
}
