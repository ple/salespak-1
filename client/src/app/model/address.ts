type AddressTypeKey = 'SERVICE' | 'BILLING' | 'MAILING';

export class Address {
  public city: string = '';
  public addressType: string;
  public postalCode: string = '';
  public location: Geolocation;
  public primaryAddress: boolean = false;
  public state: string = '';
  public line2: string = '';
  public line1: string = '';
  public addressId: number = 0;

  constructor(addressType: AddressTypeKey = 'SERVICE') {
    this.addressType = addressType;
  }
}
