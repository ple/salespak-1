export class ContactReference {
  company: string;
  customer: number;
  name: string;
  ownerType: string;
  contactId: number;
  firstName: string;
  lastName: string;
  email: string;
  title: string;
}
