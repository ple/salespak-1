import {Address} from "./address";
import {User} from "./user";

export class Prospect extends User {
  public prospectId: number = 0;

  public source: string = '';
  public status: string = '';
  public contactList: Array<number> = [];
  public estimatedMonthlyRevenue: number = 0;
  public prospectLevel: string = '';
  public salesId: string = '';
  public provider: string = '';
  public territoryId: string = '';
  public company: string = '';
  public managementCompany: string = '';
  public businessType: string = "";
  public serviceType: string = "";
  public contractLength: number = 0;
  public attention: string = '';
  public billingName: string = '';
  public email: string = '';
  public billType: string = 'P';
  public serviceContact: number = 0;
  public serviceName: string = '';
  public customer: number = 0;
  public serviceAddress: Address = new Address();
  public billingAddress: Address = new Address('BILLING');

  get readOnly(): boolean {
    return this.status == '9';
  }

  get serviceNameDisplay() {
    return this.serviceName;
  }
}
