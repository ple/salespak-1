const dateFormat = {
  timeStampFormat: 'YYYY-MM-DD-HH.mm.ss.SSSSSS',
  newDateFormat: 'MM/DD/YYYY hh:mm:ss a',
  simpleFormat: 'YYYY-MM-DD'
};

export default dateFormat
