export class SearchField {
  _businessName: string = '';
  _firstName: string = '';
  _lastName: string = '';
  searchType: string = '';
  _phoneNumber: string = '';
  _number: string = '';
  _street: string = '';
  _city: string = '';
  state: string = '';
  _zip: string = '';
  searchBy: string = 'S';
  quoteId: number;
  _customerNumber: string = '';
  _company: string = '';
  _omit: string = '';

  constructor(searchType: string = 'CONTACTS') {
    this.searchType = searchType;
  }

  get businessName(): string {
    return this._businessName == '' ? '' : `${this._businessName}*`;
  }

  get firstName(): string {
    return this._firstName == '' ? '' : `${this._firstName}*`;
  }

  get lastName(): string {
    return this._lastName == '' ? '' : `${this._lastName}*`;
  }

  get number(): string {
    return this._number == '' ? '' : `${this._number}*`;
  }

  get street(): string {
    return this._street == '' ? '' : `${this._street}*`;
  }

  get city(): string {
    return this._city == '' ? '' : `${this._city}*`;
  }

  get zip(): string {
    return this._zip == '' ? '' : `${this._zip}*`;
  }

  get phoneNumber(): string {
    return this._phoneNumber == '' ? '' : `${this._phoneNumber}`;
  }

  get customerNumber(): string {
    return this._customerNumber == '' ? '' : `${this._customerNumber}`;
  }

  get companyNumber(): string {
    return this._company == '' ? '' : `${this._company}`;
  }

  get omit(): string {
    return this._omit == '' ? '' : `${this._omit}`;
  }
}
