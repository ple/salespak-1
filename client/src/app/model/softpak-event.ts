import * as moment from 'moment';
import dateFormat from './date-format';
import {Note} from "./note";

const timeStampFormat = dateFormat.timeStampFormat,
  newDateFormat = dateFormat.newDateFormat

export type StatusKey = 'OPEN' | 'CLOSED' | 'ALL';

export class SoftpakEvent {
  closed;
  color = 'RED';
  type = 'PHONE';
  description = '';
  eventId = 0;
  eventUser;
  userid;
  isAllDay: boolean = true;
  notificationMinutes = 0;
  ownerType = "USER";
  source: string = '';
  _startTimestamp = moment().minute(0).second(0).format(newDateFormat);
  _endTimestamp = moment().minute(0).second(0).format(newDateFormat);
  status = '';
  save = false;
  delete = false;
  forward = false;
  prospectId: number;
  customer: number;
  company: string;
  linkedDisplay: string = '';
  linkedContact = {
    name: "",
    phone: ""
  };
  notesList: Array<Note> = [];
  deletedNotesList = [];




  get endTimestamp() {
    let time: moment.Moment = moment(this._endTimestamp);

    if (!time.isValid())
      time = moment();

    return this.isAllDay ? time.hour(8).format(timeStampFormat) : time.format(timeStampFormat);
  }

  set endTimestamp(endTimestamp) {
    this._endTimestamp = moment(endTimestamp, timeStampFormat).format(newDateFormat)
  }

  get startTimestamp() {
    let time: moment.Moment = moment(this._startTimestamp);

    if (!time.isValid())
      time = moment();

    return this.isAllDay ? time.hour(8).format(timeStampFormat) : time.format(timeStampFormat);
  }

  set startTimestamp(startTimestamp) {
    this._startTimestamp = moment(startTimestamp, timeStampFormat).format(newDateFormat)
  }

  get dateDisplay() {
    let date = moment(this._startTimestamp);
    return date.format('MMM DD YYYY')
  }

  get timeDisplay() {
    let date = moment(this._startTimestamp);
    return this.isAllDay ? 'All Day' : date.format('hh:mm a')
  }

  get calStart() {
    return this._startTimestamp;
  }

  get calEnd() {
    return this._endTimestamp;
  }

  constructor(event?) {
    if (event) {
      this.eventId = event.eventId;
      this.description = event.description;
      this.type = event.type;
      this.isAllDay = event.isAllDay;
      this._startTimestamp = event._startTimestamp ? event._startTimestamp : moment(event.start).format(newDateFormat);
      this._endTimestamp = event._endTimestamp ? event._endTimestamp : event.end ? moment(event.end).format(newDateFormat) : moment(event.start).format(newDateFormat);
      this.color = event.backgroundColor;
      this.eventUser = event.eventUser;
      this.source = event.source;
      this.status = (event.status || '').toUpperCase();
	    this.notesList = event.notesList;
      this.deletedNotesList = event.deletedNotesList;
      this.prospectId =  event.prospectId;
      this.customer = event.customer;
      this.company = event.company;
      this.userid =  event.userid;
      this.ownerType = SoftpakEvent.getOwnerType(event)
    }
  }

  static getOwnerType(event: SoftpakEvent) {
    if (event.customer)
      return 'CUSTOMER';

    if (event.prospectId)
      return 'PROSPECT';

    return 'USER'
  }

  static toCalEvent(event: SoftpakEvent) {
    return {
      id: event.eventId,
      title: event.description,
      allDay: event.isAllDay,
      start: event.calStart,
      end: event.calEnd,
      backgroundColor: event.color,
      borderColor: event.color,
      prospectId: event.prospectId,
      customer: event.customer,
      company: event.company,
      ownerType: SoftpakEvent.getOwnerType(event)
    }
  }
}
