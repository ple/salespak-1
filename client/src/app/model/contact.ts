import {Address} from "./address";
import {User} from "./user";

export class Contact extends User {
  public contactId: number = 0;
  public userid: string = '';
  public firstName: string = '';
  public lastName: string = '';
  public title: string = '';
  public email: string = '';
  public id: number = 0;
  miscVariables;

  get primaryPhoneNumber() {
    if (this.phoneList.length == 0)
      return '';

    let phone = this.phoneList
      .filter(p => p.primaryPhone);

    if (phone.length > 0)
      return `${phone[0].phoneType} | ${phone[0].phoneNumber} ext ${phone[0].extension}`;

    return ''
  }

  constructor() {
    super();

    this.addressList = [
      new Address('MAILING')
    ]
  }
}
