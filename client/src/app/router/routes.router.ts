import {CustomerViewComponent} from "../component/module/customer-view/customer-view.component";
import {QuoteViewComponent} from "../component/module/quote-view/quote-view.component";
import {ProspectViewComponent} from "../component/module/prospect-view/prospect-view.component";
import {SearchViewComponent} from "../component/module/search-view/search-view.component";
import {ContactViewComponent} from "../component/module/contact-view/contact-view.component";
import {EventViewComponent} from "../component/module/event-view/event-view.component";
import {SettingsViewComponent} from "../component/module/settings-view/settings-view.component";
import {ApiService} from "../service/api.service";
import {PageService} from "../service/page.service";

const eventState = {
  name: 'events',
  url: '/',
  component: EventViewComponent,
  resolve: [
    {
      token: 'settings',
      deps: [ApiService, PageService],
      resolveFn: (api: ApiService, pageService: PageService) =>
        new Promise(yay => {
          setInterval(() => {
            if (pageService.resolved) {
              yay()
            }
          }, 500)
        })
    }
  ]
};

const searchState = {
  name: 'search',
  url: '/search',
  component: SearchViewComponent,
  resolveFn: (api: ApiService, pageService: PageService) =>
    new Promise(yay => {
      setInterval(() => {
        if (pageService.resolved) {
          yay()
        }
      }, 500)
    })
};


const settingsState = {
  name: 'settings',
  url: '/settings',
  component: SettingsViewComponent,
  resolveFn: (api: ApiService, pageService: PageService) =>
    new Promise(yay => {
      setInterval(() => {
        if (pageService.resolved) {
          yay()
        }
      }, 500)
    })
};

const prospectEdit = {
  name: 'prospect-edit',
  url: '/prospect/edit/:id',
  component: ProspectViewComponent,
  params: {
    mode: 'edit'
  }
};

const prospectCreate = {
  name: 'prospect',
  url: '/prospect/create',
  component: ProspectViewComponent
};

const contactEdit = {
  name: 'contact-edit',
  url: '/contact/edit/:id',
  component: ContactViewComponent,
  params: {
    mode: 'edit'
  }
};

const contactCreate = {
  name: 'contact',
  url: '/contact/create',
  component: ContactViewComponent
};

const customer = {
  name: 'customer',
  url: '/customer/edit/:company/:id',
  component: CustomerViewComponent,
  params: {
    fromQuote: false
  }
};

const quote = {
  name: 'quote',
  url: '/quote/{module: prospect|customer}/{mode: edit|create}/:accountId/:company/:id',
  component: QuoteViewComponent
};

export const STATES = [
  eventState,
  searchState,
  prospectEdit,
  contactEdit,
  contactCreate,
  prospectCreate,
  customer,
  quote,
  settingsState
];

export const NAV = [
  {
    icon: 'fa fa-calendar',
    routes: [
      eventState
    ]
  }, {
    icon: 'fa fa-search',
    routes: [
      searchState
    ]
  }, {
    display: 'Add',
    icon: 'fa fa-plus-square',
    routes: [
      prospectCreate,
      contactCreate
    ]
  }
];
