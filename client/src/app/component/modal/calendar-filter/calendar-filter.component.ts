import { Component } from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";
import {EventService} from "../../../service/event.service";
import {SoftpakEvent} from "../../../model/softpak-event";
import {PageService} from "../../../service/page.service";
import {EventViewService} from "../../../service/event-view.service";
import * as moment from "moment";
import {FilterFieldsService} from "../../../service/filter-fields.service";

const newDateFormat = 'MM/DD/YYYY hh:mm:ss a'
const timeStampFormat = 'YYYY-MM-DD-HH.mm.ss.SSSSSS'

@Component({
  templateUrl: './calendar-filter.component.html',
  styleUrls: ['./calendar-filter.component.scss']
})
export class CalendarFilterComponent {

  events: Array<SoftpakEvent>;
  options;
  evs: EventViewService;
  view;
  filter;
  ucCalendar;
  filteredEvents = [];
  filterFn: Function = () => {};


  constructor(public bsModalRef: BsModalRef, public eventService: EventService, public page: PageService, public filterService: FilterFieldsService) {
    this.filter = filterService;
  }

  clearFilter() {
    this.filter = new FilterFieldsService(this.page);
    this.eventService.filter = this.filter;
  }

  filterEvents() {
    this.bsModalRef.hide();
    this.evs.getFilteredEventsView( this.filter ,data => {
      let [list, options] = data;
      this.view.setEvents(list['events']);
      this.view.setOptions(options);

      this.ucCalendar.renderEvents(this.view.calEvents);
    }, err => {
    });
    // this.filteredEvents = this.eventService.getFilteredEvents(this.events, this.filter);
    // this.filterFn(this.filteredEvents);
  }
  validateFromDates(event) {
    if (event !== null) {
      let time: string = moment(event).format(timeStampFormat);
      if (time > this.filter.endTimestamp) {
        this.filter.endDate = event;
      }
    }
  }
  validateEndDates(event) {
    if (event !== null) {
      let time: string = moment(event).format(timeStampFormat);
      if (time < this.filter.startTimestamp) {
        this.filter.startDate = event;
      }
    }
  }
}
