import { Component } from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";
import {UploadService} from "../../../service/upload.service";
import {ToastrService} from "ngx-toastr";
import {PageService} from "../../../service/page.service";
import {QuoteApiService} from "../../../service/api/quote-api.service";

@Component({
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent {

  title: string = 'Upload Document';
  label: string = 'Upload';
  readonly maxSize: number = 2000000; //in bytes
  signedQuoteFile: File;
  id: number;
  nextFn: Function;

  constructor(public bsModalRef: BsModalRef, public uploadService: UploadService, public quoteApi: QuoteApiService, private toast: ToastrService, public pageService: PageService) { }

  updateFile() {
    return (file: File) => {
      this.signedQuoteFile = file;
    }
  }

  upload() {
    if (this.signedQuoteFile.size > this.maxSize) {
      this.toast.warning("File Size too big")
      return;
    }

    if (this.signedQuoteFile != null) {
      this.pageService.loading = true;
      this.quoteApi.uploadQuote(this.id, this.signedQuoteFile)
        .subscribe(res => {
          if (typeof this.nextFn == 'function') {
            this.bsModalRef.hide();
            this.toast.success("Great Success");
            this.pageService.loading = false;
            this.nextFn(res)
          }
        }, err => {
          this.pageService.loading = false;
          this.toast.warning("There was an error uploading the document")
        })
    } else {
      this.toast.info("No file to upload")
    }
  }
}
