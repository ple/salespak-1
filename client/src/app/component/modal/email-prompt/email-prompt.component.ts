import { Component } from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";
import {QuoteApiService} from "../../../service/api/quote-api.service";
import {Quote} from "../../../model/quote";
import {QuoteViewService} from "../../../service/quote-view.service";
import {ToastrService} from "ngx-toastr";

@Component({
  templateUrl: './email-prompt.component.html',
  styleUrls: ['./email-prompt.component.scss']
})
export class EmailPromptComponent {

  email = '';
  quoteOptions = {};
  quote: Quote;
  command;
  company;
  // emailMask = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  constructor(public bsModalRef: BsModalRef, private quoteApi: QuoteApiService, private quoteViewService: QuoteViewService, private toast: ToastrService) { }

  emailPdf() {
    if (this.email.length > 0) {

      this.quoteViewService.emailPdf(
        this.command,
        this.company,
        this.email,
        this.quote,
        () => {
          this.bsModalRef.hide();
          this.toast.success("Email sent");
        },
        err => {
          this.toast.error("There was a problem with your request. Please try again later.");
        });
    }
  }

  canUpdateStatus() {
    return this.quote.status != "9";
  }

  get quoteStatuses() {
    return this.quoteOptions['quoteStatuses'] || [];
  }
}
