import {Component} from '@angular/core';
import {SearchView} from "../../../model/search-view";

@Component({
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.scss']
})
export class AddContactComponent {

  sv: SearchView;

}
