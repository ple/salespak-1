import { Component } from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";
import {UtilService} from "../../../service/util.service";

@Component({
  selector: 'app-text-box',
  templateUrl: './text-box.component.html',
  styleUrls: ['./text-box.component.scss']
})
export class TextBoxComponent {

  title: string = 'Edit Text';
  text: string = '';
  saveFn: (text: string) => void;
  limit: number = 999;

  constructor(public bsModalRef: BsModalRef, private util: UtilService) {}

  save() {
    this.util.isFunction(this.saveFn) && this.saveFn(this.text);
    this.bsModalRef.hide();
  }
}
