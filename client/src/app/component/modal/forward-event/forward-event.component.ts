import { Component } from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";
import {EventService} from "../../../service/event.service";

@Component({
  templateUrl: './forward-event.component.html',
  styleUrls: ['./forward-event.component.scss']
})
export class ForwardEventComponent {

  event;
  options;

  constructor(public bsModalRef: BsModalRef, public eventService: EventService) { }

  forward() {
    this.event.foward = true;
    this.event.eventId = 0;
    this.eventService.forwardEvent(this.event);
    this.bsModalRef.hide();
  }
}
