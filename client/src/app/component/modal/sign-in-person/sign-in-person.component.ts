import {Component} from '@angular/core';
import {Quote} from "../../../model/quote";
import {QuoteApiService} from "../../../service/api/quote-api.service";
import {BsModalRef} from "ngx-bootstrap";
import {PageService} from "../../../service/page.service";
import {ToastrService} from "ngx-toastr";
import {ErrorLoggingService} from "../../../service/error-logging.service";

@Component({
  templateUrl: './sign-in-person.component.html',
  styleUrls: ['./sign-in-person.component.scss']
})
export class SignInPersonComponent {

  quote: Quote;
  docUrl: string;
  error: string;

  firstName: string;
  lastName: string;
  email: string;
  account: string;

  constructor(public bsModalRef: BsModalRef, private quoteApi: QuoteApiService, private pageService: PageService, private toast: ToastrService, private errorLogging: ErrorLoggingService) {
  }

  generate() {
    // if (this.docUrl) {
    this.pageService.loading = true;
    this.quoteApi.generateSignRequest(
      this.quote.quoteId,
      this.pageService.email,
      window.location.href,
      this.firstName,
      this.lastName,
      `${this.pageService.dataSet}-quote-${this.quote.quoteId}`,
      this.email,
      this.quote.company,
      this.account
    ).subscribe(
      res => {
        window.location.href = res['redirect']
      }, err => {
        this.pageService.loading = false;
        this.errorLogging.notify(err);
        this.toast.error("Unable to generate Sign Request");
      }
    );
    // }
  }

}
