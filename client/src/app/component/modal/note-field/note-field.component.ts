import {Component, OnInit} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";
import {Note} from "../../../model/note";
import {UtilService} from "../../../service/util.service";

@Component({
  templateUrl: './note-field.component.html',
  styleUrls: ['./note-field.component.scss']
})
export class NoteFieldComponent implements OnInit {

  note: Note = new Note();
  saveFn: Function;
  limit: number = 999;
  counterNumber: number;

  constructor(public bsModalRef: BsModalRef, private util: UtilService) {
  }

  ngOnInit() {
    this.counterNumber = this.limit - this.note.note.length;
  }

  save() {
    this.util.isFunction(this.saveFn) && this.saveFn(this.note);
    this.bsModalRef.hide();
  }

  onKey(event: any) {
    this.counterNumber = this.limit - this.note.note.length;
  }
}
