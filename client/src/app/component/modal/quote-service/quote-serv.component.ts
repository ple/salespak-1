import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";
import {QuoteService} from "../../../model/quote-service";
import {ToastrService} from "ngx-toastr";

@Component({
  templateUrl: './quote-serv.component.html',
  styleUrls: ['./quote-serv.component.scss']
})
export class QuoteServComponent implements OnInit{

  saveFn: Function;
  options;
  qs: QuoteService = new QuoteService();
  disableRadio: boolean = true;
  numberOfPickups: number;

  constructor(public bsModalRef: BsModalRef, public toast: ToastrService) { }

  checkFreq(){
    if(this.qs.frequency != '') {
      let freq = this.options.frequencyList.filter(freq => freq.value == this.qs.frequency);
      this.numberOfPickups = freq[0].numberOfPickups;
      if (!isNaN(this.frequencyNumber()) && this.qs.frequency != "") {
        if (+this.frequencyNumber() < this.qs.dateCount) {
          this.qs.monday = false;
          this.qs.tuesday = false;
          this.qs.wednesday = false;
          this.qs.thursday = false;
          this.qs.friday = false;
          this.qs.saturday = false;
          this.qs.sunday = false;
        }
        this.disableRadio = this.frequencyNumber() <= this.weekCount() ? true : false;
        return;
      }
    }else{
        this.disableRadio = false;
    }
  }

  weekCount(){
    return this.qs.dateCount;
  }

  frequencyNumber(){
    return this.numberOfPickups;
  }

  save() {
    if (typeof this.saveFn == 'function') {
      if(this.qs.rate === "" || this.qs.rate === null){
        this.qs.rate = "0";
      }
      if(this.qs.total === "" || this.qs.total === null){
        this.qs.total = "0";
      }
      this.saveFn(this.qs)
      this.bsModalRef.hide()
    }
  }

  setCapacity() {
    let desc = this.options.descriptionList.filter(desc => desc.value == this.qs.description);
    this.qs.capacity = desc[0].capacity;
    this.qs.size = desc[0].size;
    this.qs.charge = desc[0].charge;
  }
  ngOnInit(): void {
    this.checkFreq();
  }
}
