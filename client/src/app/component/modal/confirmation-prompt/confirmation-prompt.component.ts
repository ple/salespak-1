import {Component, Input, OnInit} from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";
import {UtilService} from "../../../service/util.service";

@Component({
  templateUrl: './confirmation-prompt.component.html',
  styleUrls: ['./confirmation-prompt.component.scss']
})
export class ConfirmationPromptComponent implements OnInit {

  @Input()
  confirmationPrompt: string;
  @Input()
  confirmLabel: string;
  @Input()
  cancelLabel: string;

  @Input()
  confirmAction: Function;

  @Input()
  cancelAction: Function = () => {
    this.bsModalRef.hide();
  };

  constructor(public bsModalRef: BsModalRef, private util: UtilService) { }

  confirmActionClick() {
    this.util.isFunction(this.confirmAction) && this.confirmAction()
  }

  cancelActionClick() {
    this.util.isFunction(this.cancelAction) && this.cancelAction()
  }

  ngOnInit() {
  }

}
