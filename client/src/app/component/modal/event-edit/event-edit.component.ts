import {Component, OnInit} from '@angular/core';
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {ForwardEventComponent} from "../forward-event/forward-event.component";
import {SoftpakEvent} from "../../../model/softpak-event";
import {Note} from "../../../model/note";
import {UtilService} from "../../../service/util.service";
import {SearchViewComponent} from "../../module/search-view/search-view.component";
import {ProspectApiService} from "../../../service/api/prospect-api.service";
import {CustomerApiService} from "../../../service/api/customer-api.service";
import {PageService} from "../../../service/page.service";
import {SearchView} from "../../../model/search-view";
import {LinkedAccount} from "../../../model/linked-account";
import {NoteFieldComponent} from "../note-field/note-field.component";
import ics from "../../../../assets/js/ics.min";
import {SearchApiService} from "../../../service/api/search-api.service";

@Component({
  templateUrl: './event-edit.component.html',
  styleUrls: ['./event-edit.component.scss']
})
export class EventEditComponent implements OnInit {

  note: Note;
  event: SoftpakEvent;
  options;
  saveFn: Function;
  deleteFn: Function;

  forwardModalRef: BsModalRef;
  cusProspModalRef: BsModalRef;
  noteModalRef: BsModalRef;

  formValidFlag = false;

  eventStatus = '';
  fDateOpen = false;
  dDateOpen = false;
  noteListValidationFlag = false;

  constructor(public bsModalRef: BsModalRef, protected modalService: BsModalService, private util: UtilService,
              public prospectApi: ProspectApiService, public customerApi: CustomerApiService, public pageService: PageService, private searchApi: SearchApiService) {
  }

  ngOnInit() {
    this.eventStatus = this.event.status;
    this.event.linkedDisplay = 'Loading...';
    if (this.event.prospectId > 0) {
      this.prospectApi.getProspect(this.event.prospectId).subscribe(res => {
        this.event.linkedDisplay = res['prospect']['client'].businessName;
        let serviceContactId = res['prospect']['client'].serviceContact;
        res['prospect']['contactData']
          .filter(c => serviceContactId == c.contactId)
          .forEach(c => {
            this.event.linkedContact.name = `${c.firstName} ${c.lastName}`;
            c.phoneList.forEach(p => {
              this.event.linkedContact.phone += `${p.phoneNumber} - ${p.phoneType}\n`
            })
          });
      });
    } else if (this.event.customer > 0) {
      this.customerApi.getCustomerData(this.event.company, this.event.customer).subscribe(res => {
        this.event.linkedDisplay = res['customer']['client'].serviceName;
        this.event.linkedContact.name = res['customer']['client'].contactName;
        this.event.linkedContact.phone = res['customer']['client'].contactPhone1;
      });
    } else {
      this.event.linkedDisplay = '';
    }

    this.searchApi.noteList('EVENT', this.event.eventId).subscribe(data => {
      this.event.notesList = data['response'].noteList;
    })
  }

  save() {
    if (this.util.isFunction(this.saveFn) && this.formValidFlag) {
      this.saveFn(this.event);
      this.bsModalRef.hide();
    }
  }

  search() {
    let sv = new SearchView();
      sv.linkedAccount = new LinkedAccount(account => {
        if (account['type'] === 'CUSTOMER') {
          this.event.prospectId = 0;
          this.event.customer = account.obj.id;
          this.event.ownerType = 'CUSTOMER';
          this.event.company = account.obj.company;
          this.event.linkedDisplay = account.obj.serviceName;
        } else {
          this.event.customer = 0;
          this.event.prospectId = account.obj.id;
          this.event.ownerType = 'PROSPECT';
          this.event.linkedDisplay = account.obj.businessName;
        }

        this.event.linkedContact.name = account.obj.contactName;
        this.event.linkedContact.phone = account.obj.contactPhone;
        this.cusProspModalRef.hide();
      });

    this.cusProspModalRef = this.modalService.show(SearchViewComponent, {
      initialState: {
        sv
      }
    });
  }

  unLinkAccount() {
    this.event.prospectId = 0;
    this.event.customer = 0;
    this.event.company = '';
    this.event.linkedDisplay = '';
  }

  deleteEvent: Function = () => {
    if (this.util.isFunction(this.deleteFn)) {
      this.deleteFn(this.event)
      if (this.bsModalRef) {
        this.bsModalRef.hide()
      }
    }
  };

  validateFromDates(event) {
    if (event !== null) {
      if (event > this.event._endTimestamp) {
        this.formValidFlag = false;
      } else {
        this.formValidFlag = true;
      }
    }
  }

  validateToDates(event) {
    if (event !== null) {
      if (event < this.event._startTimestamp) {
        this.formValidFlag = false;
      } else {
        this.formValidFlag = true;
      }
    }
  }

  addNote() {
    this.noteModalRef = this.modalService.show(NoteFieldComponent, {
      initialState: {
        saveFn: note => {
          this.event.notesList.push(note);
          this.event.notesList = this.event.notesList.slice();
        }
      },
    });
  }

  forward() {
    this.forwardModalRef = this.modalService.show(ForwardEventComponent, {
      initialState: {
        event: this.event,
        options: this.options
      }
    });
  }

  colour(type) {
    if (type === '') {
      return false;
    }
    let colour = this.options.eventType.filter(et => et.type == type);
    this.event.color = colour[0].color;
  }

  exportCalendar() {
    let cal = ics(),
      calDesc = `${this.event.linkedDisplay}. `;

    calDesc += this.event.linkedContact.name == '' || this.event.linkedContact.name == undefined ? '' : `Contact name: ${this.event.linkedContact.name}. `;
    calDesc += this.event.linkedContact.phone == '' || this.event.linkedContact.phone == undefined ? '' : `Phone: ${this.event.linkedContact.phone}`;

    cal.addEvent(this.event.description, calDesc, '', this.event._startTimestamp, this.event._endTimestamp);
    //window.open( "data:text/calendar;charset=utf8," + escape(cal));
    cal.download(`event_${this.event.type}_${this.event.eventId}`);
  }
}
