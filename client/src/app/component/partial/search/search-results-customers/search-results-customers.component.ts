import {Component, Input, ChangeDetectorRef} from '@angular/core';
import {SearchView} from "../../../../model/search-view";
import {UtilService} from "../../../../service/util.service";
import {SearchViewService} from "../../../../service/search-view.service";
import {forEach} from "@angular/router/src/utils/collection";

@Component({
  selector: 'app-search-results-customers',
  templateUrl: './search-results-customers.component.html',
  styleUrls: ['./search-results-customers.component.scss']
})
export class SearchResultsCustomersComponent {

  @Input()
  sv: SearchView;

  constructor(private util: UtilService, public searchViewService: SearchViewService) {}

  get results(): Array<any> {
    return this.sv.results;
  }

  redirect(id, type, company) {
    const idSet = new Set(JSON.parse(localStorage.getItem(type)));
    type == "CUSTOMERS" ? idSet.add(id+":"+company) : idSet.add(id);
    const idArray = Array.from(idSet.values()).reverse();
    localStorage.setItem( type, JSON.stringify(idArray.slice(0,5)));
    this.util.navigateTo(this.sv.typeMap(type), id, {mode: 'edit', company})
  }

  actionView(): string {
    if (this.sv.linkedAccount) {
      return 'linkedAccount'
    }
    return 'default'
  }

  selectAccount(obj) {
    this.sv.linkedAccount.selectAccount({obj: obj, type: 'CUSTOMER'});
  }
}
