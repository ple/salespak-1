import {Component, EventEmitter, Input, OnChanges, OnInit, Optional, Output, SimpleChanges} from '@angular/core';
import {UtilService} from "../../../../service/util.service";
import {SearchView} from "../../../../model/search-view";
import {Quote} from "../../../../model/quote";
import {BsModalRef} from "ngx-bootstrap";
import {SearchViewService} from "../../../../service/search-view.service";
import {Phone} from "../../../../model/phone";
import {ProspectApiService} from "../../../../service/api/prospect-api.service";
import {Observable, Subject, forkJoin} from "rxjs";
import {ContactApiService} from "../../../../service/api/contact-api.service";
import {CustomerApiService} from "../../../../service/api/customer-api.service";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  @Input()
  sv: SearchView;

  @Output()
  svChange = new EventEmitter<SearchView>();

  phoneMask = Phone.PHONEMASK;

  searchTypeOpt = SearchView.searchType();

  thisRequest;

  constructor(public searchViewService: SearchViewService, private util: UtilService, @Optional() public bsModalRef: BsModalRef, public prospectApi: ProspectApiService, public contactApi: ContactApiService, public customerApi: CustomerApiService) {
  }

  ngOnInit() {
    if (!this.sv.actionTemplate) this.getPrevious();
    if (this.sv.linkedAccount) {
      this.sv.clearForm('CUSTOMERS');
      this.searchTypeOpt = this.sv.linkedAccount.searchTypeOpt;
    }

    this.companies();
  }

  get states() {
    return Object.keys(this.util.states());
  }

  get provCAN() {
    return Object.keys(this.util.provCAN());
  }

  companies() {
    this.searchViewService.getCompanies(
      ([options]) => {
        this.sv.setOptions(options.options, this.sv.options)
      },
      () => {
        console.log('error: getCompanies')
      }
    )
  }

  getPrevious() {
    let e = this.sv.sf.searchType;
    if (!localStorage.getItem(e))
      return;
    this.searchViewService.searchStatus = "Loading ...";
    if (this.thisRequest)
      this.thisRequest.unsubscribe();
    let idArray: string[] = JSON.parse(localStorage.getItem(e));
    let observableList = [];
    switch (e) {
      case "PROSPECTS" :
        for (let id of idArray) {
          observableList.push(this.prospectApi.getProspect(+id))
        }
        this.processRequest(observableList, 'prospect');
        return;
      case "CONTACTS" :
        for (let id of idArray) {
          observableList.push(this.contactApi.getRecentContact(+id))
        }
        this.processRequest(observableList, 'contact');
        return;
      case "CUSTOMERS" :
        for (let id of idArray) {
          let array = id.toString().split(':'), cusId = array[0], company = array[1];
          observableList.push(this.customerApi.getRecentCustomerData(company, cusId))
        }
        this.processRequest(observableList, 'customer');
        return;
      default:
        return;
    }
  }

  processRequest(observableList, key) {
    this.sv.results = [];
    this.thisRequest = forkJoin(observableList).subscribe(data => {
      this.sv.results = data
        .map(d1 => {

          let d = d1[key]['client'];
          d.type = this.sv.sf.searchType;
          d.id = d.prospectId || d.contactId || d.customer;
          d.company = d.company || "";
          d.contactPhone = '';

          if (key == 'prospect') {
            let serviceContactId = d.serviceContact;
            d1[key]['contactData']
              .filter(c => serviceContactId == c.contactId)
              .forEach(c => {
                d.contactName = `${c.firstName} ${c.lastName}`;
                c.phoneList.forEach(p => {
                  d.contactPhone += `${p.phoneNumber} - ${p.phoneType}\n`
                })
              });
          } else if (key == 'customer') {
            d.contactPhone = d.contactPhone1;
          }

          return d;
        });

      this.sv.sr.resultType = this.sv.sf.searchType;
    });
  }

  search() {
    if (this.sv.sf.searchType == 'QUOTES') {
      this.searchViewService.searchQuote(
        this.sv,
        this.quoteSuccess,
        () => {

        }
      )
    } else {
      this.searchViewService.searchClient(
        this.sv,
        this.clientSuccess,
        () => {
        }
      )
    }
  }

  clientSuccess = data => {
    this.sv.results = data['search']['results']
      .filter(data => !this.sv.sf.omit || !data.cancelled)
      .map(d => {
      d.type = this.sv.sf.searchType;
      d.id = d.prospectId || d.contactId || d.customer;
      d.company = d.company || "";
      d.contactPhone = '';

      if (d.serviceContact != undefined) {
        d.contactName = '';

        let c = d.contactList.filter(cl => cl.contactId === d.serviceContact);

        if (c.length > 0) {
          d.contactName = `${c[0].firstName} ${c[0].lastName}`;
        }
      } else if (d.contactName != undefined) {
        d.contactPhone = d.contactPhone1;
      }

      return d;
    });
    if (this.sv.results.length == 0) {
      this.searchViewService.noResultStatus();
    }
    this.sv.sr.resultType = this.sv.sf.searchType;
    this.svChange.emit(this.sv);
  };

  quoteSuccess = res => {
    let {quote, prospect, customer} = res;

    this.sv.results = [];
    this.sv.sr.resultType = this.sv.sf.searchType;
    if (quote.quoteId > 0) {
      let q = Object.assign(new Quote(), quote);
      q.parentName = 'Unknown Prospect';

      if (prospect != null && prospect.hasOwnProperty("client")) {
        q.parentName = prospect.client.businessName;
      } else if (customer != null && customer.hasOwnProperty("client")) {
        q.parentName = prospect.client.serviceName;
      }

      this.sv.results.push(q);
      this.svChange.emit(this.sv);
    }
  }
}
