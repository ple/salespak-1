import { FormGroup } from '@angular/forms';

export function ValidateInput(co: string, cust: string, last: string, first: string, num: string,
                              str: string, ci: string, sta: string, zip: string) {
  return (formGroup: FormGroup) => {
    const company = formGroup.controls[co];
    const customer = formGroup.controls[cust];
    const lastName = formGroup.controls[last];
    const firstName = formGroup.controls[first];
    const number = formGroup.controls[num];
    const street = formGroup.controls[str];
    const city = formGroup.controls[ci];
    const state = formGroup.controls[sta];
    const zipCode = formGroup.controls[zip];

    if (!company || !customer || !lastName || !firstName
        || !number || !street || !city || !state || !zipCode)
      return null;

    try {
      if (company.value == "" && customer.value.length === 0 && lastName.value.length === 0 && firstName.value.length === 0
        && number.value.length === 0 && street.value.length === 0 && city.value.length === 0 && state.value == ""
        && zipCode.value.length === 0) {
        company.setErrors({searchCriteriaRequired: true});
      } else if (company.value != "" && (customer.value.length === 0 && lastName.value.length === 0 && firstName.value.length === 0
        && number.value.length === 0 && street.value.length === 0 && city.value.length === 0 && state.value == ""
        && zipCode.value.length === 0)) {
        company.setErrors({additionalCriteriaRequired: true});
      } else if (customer.value.length > 0 && company.value == "") {
        company.setErrors({companyRequired: true});
      } else {
        company.setErrors(null);
      }
    } catch (e) {
      console.log("Validator controls not yet initialised");
    }
  }

}
