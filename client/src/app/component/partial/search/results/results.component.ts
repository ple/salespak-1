import {Component, Input} from '@angular/core';
import {SearchView} from "../../../../model/search-view";

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent {

  @Input()
  sv: SearchView;

  constructor(){}
}
