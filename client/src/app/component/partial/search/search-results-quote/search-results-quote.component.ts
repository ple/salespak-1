import {Component, Input} from '@angular/core';
import {SearchView} from "../../../../model/search-view";
import {UtilService} from "../../../../service/util.service";
import {Quote} from "../../../../model/quote";

@Component({
  selector: 'app-search-results-quote',
  templateUrl: './search-results-quote.component.html',
  styleUrls: ['./search-results-quote.component.scss']
})
export class SearchResultsQuoteComponent {

  @Input()
  sv: SearchView;

  constructor(private util: UtilService) { }

  get results(): Array<any> {
    return this.sv.results.filter(r => !r.id)
  }

  redirect(q: Quote, module: 'prospect'|'customer') {
    this.util.navigateTo('quote', q.quoteId, {
      accountId: q.prospectId,
      company: q.company,
      module
    });
  }

}
