import {Component, Input} from '@angular/core';
import {SearchView} from "../../../../model/search-view";
import {UtilService} from "../../../../service/util.service";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {SearchViewService} from "../../../../service/search-view.service";

@Component({
  selector: 'app-search-results-prospects',
  templateUrl: './search-results-prospects.component.html',
  styleUrls: ['./search-results-prospects.component.scss']
})
export class SearchResultsProspectsComponent {

  @Input()
  sv: SearchView;
  bsModalRef: BsModalRef;

  constructor(private util: UtilService, private modalService: BsModalService, public searchViewService: SearchViewService) { }

  get results(): Array<any> {
    return this.sv.results
  }

  redirect(id, type, company) {
    const idSet = new Set(JSON.parse(localStorage.getItem(type)));
    type == "CUSTOMERS" ? idSet.add(id+":"+company) : idSet.add(id);
    const idArray = Array.from(idSet.values()).reverse();
    localStorage.setItem( type , JSON.stringify(idArray.slice(0,5)));
    this.util.navigateTo(this.sv.typeMap(type), id, {mode: 'edit', company})
  }

  actionView(): string {
    if (this.sv.linkedAccount) {
      return 'linkedAccount'
    }
    return 'default'
  }

  selectAccount(obj) {
    this.sv.linkedAccount.selectAccount({obj: obj, type: 'PROSPECT'});
  }
}
