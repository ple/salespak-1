import {Component, Input, OnInit, Optional} from '@angular/core';
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {ToastrService} from "ngx-toastr";
import {SearchView} from "../../../../model/search-view";
import {UtilService} from "../../../../service/util.service";
import {Contact} from "../../../../model/contact";
import {SearchViewService} from "../../../../service/search-view.service";

@Component({
  selector: 'app-search-results-contacts',
  templateUrl: './search-results-contacts.component.html',
  styleUrls: ['./search-results-contacts.component.scss']
})
export class SearchResultsContactsComponent{

  @Input()

  sv: SearchView;
  bsModalRef: BsModalRef;
  contactResult;

  constructor(private toast: ToastrService, private modalService: BsModalService, private util: UtilService, public searchViewService: SearchViewService) { }

  add(contact: Contact) {
    this.util.isFunction(this.sv.customCb) && this.sv.customCb(contact);
  }

  get results(): Array<any> {
    return this.sv.results;
  }

  actionTemplate(): string {
    return this.sv.actionTemplate;
  }

  redirect = (id, type, company = '') => {
    const idSet = new Set(JSON.parse(localStorage.getItem(type)));
    idSet.add(id);
    const idArray = Array.from(idSet.values()).reverse();
    localStorage.setItem( type , JSON.stringify(idArray.slice(0,5)));
    this.util.navigateTo(this.sv.typeMap(type), id, {mode: 'edit', company});
  };
  // redirect(id, index) {
  //   let c: Array<Contact> = this.results;
  //   this.bsModalRef = this.modalService.show(ContactEditComponent, { class: 'modal-lg',initialState:{
  //       id,
  //       index,
  //       c
  //   }});
  // };
}
