import {Component, Input, SimpleChanges, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatPaginator, MatTableDataSource} from "@angular/material";
import {PageService} from "../../../../service/page.service";
import {SoftpakEvent} from "../../../../model/softpak-event";
import {EventEditComponent} from "../../../modal/event-edit/event-edit.component";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss']
})
export class EventListComponent {

  @Input()
  events: Array<any>;

  @Input()
  options;

  @Input()
  saveFn: Function;

  @Input()
  deleteFn: Function;

  @ViewChild(MatSort) sort: MatSort;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  dataSource: MatTableDataSource<Array<any>>;

  displayedColumns: Array<string> = ['date', 'time', 'desc', 'action'];

  bsModalRef: BsModalRef;

  constructor(private pageService: PageService, private modalService: BsModalService, private toast: ToastrService) {}

  editEventModal(event: SoftpakEvent = new SoftpakEvent()) {
    if (!event.eventUser) event.eventUser = this.options['user'];

    this.bsModalRef = this.modalService.show(EventEditComponent, {
      class: "modal-lg",
      initialState: {
        event: Object.create(event),
        options: this.options,
        saveFn: (event: SoftpakEvent) => {
          this.saveEvent(event)();
        },
        deleteFn: (event: SoftpakEvent) => {
          this.deleteEvent(event)()
        }
      }
    });
  }

  deleteEvent(event) {
    return () => {
      if (typeof this.deleteFn == 'function') {
        this.deleteFn(event);
        if (this.bsModalRef) {
          this.bsModalRef.hide()
        }
      } else {
        new Error("deleteFn is not defined")
      }
    };
  }

  saveEvent(event) {
    return () => {
      if (typeof this.saveFn == 'function') {
        this.saveFn(event);
        this.bsModalRef.hide()
      } else {
        new Error("saveFn is not defined")
      }
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    this.events = changes.events.currentValue;
    this.updateDataSource()
  }

  updateDataSource() {
    this.dataSource = new MatTableDataSource(this.events);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string = '') {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
