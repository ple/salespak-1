import {Component, EventEmitter, Input, Output, SimpleChanges, ViewChild} from '@angular/core';
import {UtilService} from "../../../../service/util.service";
import {Note} from "../../../../model/note";
import {PageService} from "../../../../service/page.service";
import * as moment from 'moment';
import dateFormat from "../../../../model/date-format";
import {MatPaginator, MatSort, MatTableDataSource} from "@angular/material";

const {newDateFormat} = dateFormat;

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.scss']
})
export class NoteListComponent {

  @Input()
  noteList: Array<any>;

  @Input()
  deletedNoteList: Array<number>;

  @Input()
  limit: number = 999;

  @Input()
  displayAction: boolean = true;

  @Output()
  noteListChange: EventEmitter<Array<Note>> = new EventEmitter<Array<Note>>();

  @Output()
  deletedNoteListChange:EventEmitter<Array<number>> = new EventEmitter<Array<number>>();

  @ViewChild(MatSort) sort: MatSort;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  dataSource: MatTableDataSource<Array<any>>;

  displayedColumns: Array<string> = ['date', 'time', 'code', 'note', 'added', 'action'];

  constructor(private util: UtilService, private pageService: PageService) {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.sortNotes();
    this.updateDataSource()
  }

  sortNotes() {
    if (this.noteList != null) {
      this.noteList.sort((a, b) => this.pageService.settings.sortNotesDescending ? b.noteId - a.noteId : a.noteId - b.noteId)
    }
  }

  updateDataSource() {
    this.dataSource = new MatTableDataSource(this.noteList);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  deleteNote(id, index): Function {
    return () => {
      id != 0 && this.deletedNoteList.push(id);
      this.noteList.splice(index, 1);
      this.change()
    }
  }

  change = () => {
    this.deletedNoteListChange.emit(this.deletedNoteList);
    this.noteListChange.emit(this.noteList);
    this.updateDataSource()
  };

  getDate(timeStamp: string): string {
    return moment(timeStamp, newDateFormat).format('ddd MM/DD/YY')
  }

  getTime(timeStamp: string): string {
    return moment(timeStamp, newDateFormat).format('HH:mm')
  }

}

