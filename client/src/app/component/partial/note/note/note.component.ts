import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Note} from "../../../../model/note";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {NoteFieldComponent} from "../../../modal/note-field/note-field.component";
import {UtilService} from "../../../../service/util.service";

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent {

  @Input()
  note: Note;

  @Input()
  deleteFn: Function;

  @Input()
  limit: number = 999;

  @Input()
  displayAction: boolean = true;

  @Output()
  noteChange = new EventEmitter<Note>();

  bsModalRef: BsModalRef;

  constructor(private util: UtilService, public modalService: BsModalService) {}

  change() {
    this.noteChange.emit(this.note);
  }

  canDelete(): boolean {
    return this.util.isFunction(this.deleteFn);
  }

  delete: Function = () => {
    this.canDelete() && this.deleteFn();
  };

  edit() {
    this.bsModalRef = this.modalService.show(NoteFieldComponent, {
      initialState: {
        note: Object.create(this.note),
        limit: this.limit,
        saveFn: (note: Note) => {
          this.note.note = note.note;
        }
      }
    });
  }
}
