import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss']
})
export class RadioComponent {

  @Input() label: string;
  @Input() model: string;
  @Input() value: any;
  @Input() name: string;
  @Input() readOnly: boolean = false;
  @Output() modelChange = new EventEmitter<string>();

  change() {
    this.modelChange.emit(this.model);
  }

}
