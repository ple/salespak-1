import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent {

  @Input() label: string;
  @Input() model: string;
  @Input() isActive: boolean;
  @Output() modelChange = new EventEmitter<string>();

  change() {
    this.modelChange.emit(this.model);
  }

}
