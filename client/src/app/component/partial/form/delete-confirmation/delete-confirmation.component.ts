import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-delete-confirmation',
  templateUrl: './delete-confirmation.component.html',
  styleUrls: ['./delete-confirmation.component.scss']
})
export class DeleteConfirmationComponent {

  @Input()
  confMessage: string = 'You Sure?';

  @Input()
  deleteFn: Function;

  @Input()
  cancelFn: Function;

  @Input()
  confirmLabel: string = 'Yes';

  @Input()
  rejectLabel: string = 'No';

  @Input()
  buttonLabel: string = 'Delete';

  @Input()
  icon: string = '';

  delete() {
    if (typeof this.deleteFn == 'function')
      this.deleteFn()
  }
}
