import { Component, OnInit } from '@angular/core';
import { STATES } from '../../../../router/routes.router';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {

  STATES: Array<any>;

  ngOnInit() {
    this.STATES = STATES
  }

}
