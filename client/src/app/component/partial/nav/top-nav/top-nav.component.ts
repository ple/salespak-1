import {Component} from '@angular/core';
import {ApiService} from "../../../../service/api.service";
import {NAV} from "../../../../router/routes.router";
import {UtilService} from "../../../../service/util.service";
import {PageService} from "../../../../service/page.service";

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.scss']
})
export class TopNavComponent {

  NAV: Array<any> = NAV;

  constructor(private api: ApiService, private util: UtilService, public page: PageService){}
}
