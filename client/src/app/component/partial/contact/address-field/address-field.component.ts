import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Address} from "../../../../model/address";
import {UtilService} from "../../../../service/util.service";
import {PageService} from "../../../../service/page.service";

@Component({
  selector: 'app-address-field',
  templateUrl: './address-field.component.html',
  styleUrls: ['./address-field.component.scss']
})
export class AddressFieldComponent {

  @Input()
  address: Address;

  @Input()
  addressType: Array<any>;

  @Input()
  deleteFn: Function;

  @Input()
  canDeleteFn: Function;

  @Input()
  fixedAddressType: boolean = true;

  @Input()
  updatePrimaryAddressFn: Function;

  @Input()
  readOnly : boolean = false;

  @Input()
  label: boolean = true;

  @Output()
  addressChange = new EventEmitter<Address>();

  states: Array<string>;
  provCAN: Array<string>;

  constructor(private util: UtilService, public pageService: PageService) {
    this.states = Object.keys(util.states());
    this.provCAN = Object.keys(util.provCAN());
  }

  change() {
    this.addressChange.emit(this.address);
  }

  canDelete(): boolean {
    return this.util.isFunction(this.canDeleteFn) && this.canDeleteFn() && !this.address.primaryAddress;
  }

  delete = () => this.util.isFunction(this.deleteFn) && this.deleteFn();

  canUpdatePrimaryAddress(): boolean {
    return this.util.isFunction(this.updatePrimaryAddressFn)
  }

  updatePrimaryAddress() {
    this.util.isFunction(this.updatePrimaryAddressFn) && this.updatePrimaryAddressFn(this.address)
  }

  isUSZip(zip){
    return /^\d{5}(-\d{4})?$/.test(zip);
  }

  get addressOneLabel() {
    return 'Address'
  }

  get addressTwoLabel() {
    return 'Address 2'
  }

  get cityLabel() {
    return 'City'
  }

  get stateLabel() {
    return 'State/Province'
  }

  get zipLabel() {
    return 'Zip/Postal Code'
  }
}
