import {Component, Input} from '@angular/core';
import {Contact} from "../../../../model/contact";
import {UtilService} from "../../../../service/util.service";
import {ToastrService} from "ngx-toastr";
import {ProspectView} from "../../../../model/prospect-view";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {ContactViewComponent} from "../../../module/contact-view/contact-view.component";

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent {

  @Input()
  contacts: Array<Contact>;

  @Input()
  readOnly: boolean = false;

  @Input()
  prospectView: ProspectView;

  bsModalRef: BsModalRef;

  constructor(private util: UtilService, private toast: ToastrService, private modalService: BsModalService) {}

  serviceContactTooltip(id: number): string {
    return this.prospectView.prospect.serviceContact == id ? "Current Service Contact" : "Make Service Contact"
  }

  serviceContactClass(id: number): string {
    return this.prospectView.prospect.serviceContact == id ? "btn-success" : "btn-default"
  }

  selectServiceContact(id: number) {
    if (!this.readOnly)
      this.prospectView.prospect.serviceContact = id;
  }

  editContact(contact) {
    this.bsModalRef = this.modalService.show(ContactViewComponent, {
      class: 'modal-lg',
      initialState: {
        contact
      }
    });
  }

  deleteContact(id) {
    if (this.readOnly)
      return () => {};

    return () => {
      if (this.prospectView.deletedContactList.length > 1 && this.prospectView.prospect.serviceContact == id)
        return this.toast.warning("Please select new service contact before removing");

      this.prospectView.deletedContactList.push(id);
      this.prospectView.contacts = this.prospectView.contacts
        .filter(c => c.contactId != id);

      if (this.prospectView.deletedContactList.length == 0)
        this.prospectView.prospect.serviceContact = 0
    }
  }
}
