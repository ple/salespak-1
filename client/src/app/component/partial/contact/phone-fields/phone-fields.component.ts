import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Phone} from "../../../../model/phone";
import {UtilService} from "../../../../service/util.service";

@Component({
  selector: 'app-phone-fields',
  templateUrl: './phone-fields.component.html',
  styleUrls: ['./phone-fields.component.scss']
})
export class PhoneFieldsComponent implements OnInit {

  @Input()
  phone: Phone;

  @Input()
  phoneTypeList;

  @Input()
  deleteFn: Function;

  @Input()
  canDeleteFn: Function;

  @Input()
  primaryPhoneFn: Function;

  @Input()
  readOnly: boolean = false;

  @Output()
  phoneChange = new EventEmitter<Phone>();

  phoneMask = Phone.PHONEMASK;

  constructor(private util: UtilService) {}

  change() {
    this.phone.phoneNumber = this.phone._phoneNumber.replace(/\D+/g, '');
    this.phoneChange.emit(this.phone);
  }

  delete = () => {
    if (this.readOnly)
      return;

    this.util.isFunction(this.deleteFn) && this.deleteFn();
  };

  canDelete(): boolean {
    return this.util.isFunction(this.canDeleteFn) && this.canDeleteFn() && !this.phone.primaryPhone;
  }

  updatePrimaryPhone() {
    if (!this.readOnly)
      this.util.isFunction(this.primaryPhoneFn) && this.primaryPhoneFn(this.phone)
  }

  ngOnInit(): void {
    this.phone._phoneNumber = this.phone.phoneNumber;
    this.phone.phoneNumber = this.phone._phoneNumber.replace(/\D+/g, '');
  }
}
