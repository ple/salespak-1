import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Phone} from "../../../../model/phone";
import {UtilService} from "../../../../service/util.service";

@Component({
  selector: 'app-phone-list',
  templateUrl: './phone-list.component.html',
  styleUrls: ['./phone-list.component.scss']
})
export class PhoneListComponent {

  @Input()
  phoneList: Array<Phone>;

  @Input()
  deletedPhoneList: Array<number> = [];

  @Input()
  phoneTypeList: Array<any>;

  @Input()
  canDeletePhoneFn: Function;

  @Input()
  readOnly: boolean = false;

  @Output()
  deletedPhoneListChange:EventEmitter<Array<number>> = new EventEmitter<Array<number>>();

  @Output()
  phoneListChange: EventEmitter<Array<Phone>> = new EventEmitter<Array<Phone>>();

  constructor(private util: UtilService) {}

  deletePhone(id, index): Function {
    return () => {
      id != 0 && this.deletedPhoneList.push(id);
      this.phoneList.splice(index, 1);
      this.change()
    }
  };

  change() {
    this.deletedPhoneListChange.emit(this.deletedPhoneList);
    this.phoneListChange.emit(this.phoneList)
  }

  canDeletePhone: Function = () => {
    if (this.util.isFunction(this.canDeletePhoneFn))
      return this.canDeletePhoneFn();

    return this.phoneList.length > 1

  };

  updatePrimaryPhone: Function = phone => {
    this.phoneList.forEach(phone => phone.primaryPhone = false);
    phone.primaryPhone = true;
    this.phoneListChange.emit(this.phoneList)
  }

}
