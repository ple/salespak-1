import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Address} from "../../../../model/address";

@Component({
  selector: 'app-address-list',
  templateUrl: './address-list.component.html',
  styleUrls: ['./address-list.component.scss']
})
export class AddressListComponent {

  @Input()
  addressList: Array<Address>;

  @Input()
  deleteAddressList: Array<number>;

  @Input()
  addressTypeList: Array<any>;

  @Input()
  fixedAddressType: boolean = true;

  @Output()
  addressListChange = new EventEmitter<Array<Address>>();

  @Output()
  deleteAddressListChange = new EventEmitter<Array<number>>();

  deleteAddress(id, index): Function {
    return () => {
      id != 0 && this.deleteAddressList.push(id);
      this.addressList.splice(index, 1);
      this.change()
    }
  };

  canDeleteAddress: Function = () => {
    return this.addressList.length > 1;
  };

  change() {
    this.deleteAddressListChange.emit(this.deleteAddressList);
    this.addressListChange.emit(this.addressList)
  }

  updatePrimaryAddress: Function = address => {
    this.addressList.forEach(a => a.primaryAddress = false);
    address.primaryAddress = true;
    this.change()
  }
}
