import {Component, Input} from '@angular/core';
import {StateService} from '@uirouter/core';
import {UtilService} from "../../../../service/util.service";
import {ContactReference} from "../../../../model/contact-reference";

@Component({
  selector: 'app-contacts-field',
  templateUrl: './contacts-field.component.html',
  styleUrls: ['./contacts-field.component.scss']
})
export class ContactsFieldComponent {

  @Input()
  contacts: Array<ContactReference>;

  constructor(private util: UtilService) {}

  redirect(cr: ContactReference) {
    this.util.navigateTo(cr.ownerType.toLowerCase(), cr.customer, {mode: 'edit', company: cr.company})
  }
  contactRedirect(id: number) {
    this.util.navigateTo("contact-edit", id, {mode: 'edit', company: ''})
  }
}

