import {Component, EventEmitter, Input, Output} from '@angular/core';
import {MiscVar} from "../../../../model/misc-var";

@Component({
  selector: 'app-misc-var-field',
  templateUrl: './misc-var-field.component.html',
  styleUrls: ['./misc-var-field.component.scss']
})
export class MiscVarFieldComponent {

  @Input()
  miscVar: MiscVar;
  @Output()
  miscVarChange = new EventEmitter<MiscVar>();

  change() {
    this.miscVarChange.emit(this.miscVar);
  }
}
