import {Component, EventEmitter, Input, Output} from '@angular/core';
import {QuoteApiService} from "../../../../service/api/quote-api.service";
import {EmailPromptComponent} from "../../../modal/email-prompt/email-prompt.component";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {PageService} from "../../../../service/page.service";
import {UploadComponent} from "../../../modal/upload/upload.component";
import {UtilService} from "../../../../service/util.service";
import {Prospect} from "../../../../model/prospect";
import {Quote} from "../../../../model/quote";
import {SignInPersonComponent} from "../../../modal/sign-in-person/sign-in-person.component";
import {Customer} from "../../../../model/customer";

@Component({
  selector: 'app-quote-list',
  templateUrl: './quote-list.component.html',
  styleUrls: ['./quote-list.component.scss']
})
export class QuoteListComponent {

  @Input()
  quoteList: Array<any>;

  @Input()
  account: Prospect | Customer;

  @Input()
  deletedQuoteList: Array<number>;

  @Input()
  company: string;

  @Input()
  readOnly: boolean = false;

  @Input()
  quoteOptions = [];

  @Input()
  module: 'prospect'|'customer';

  @Output()
  quoteListChange: EventEmitter<Array<any>> = new EventEmitter<Array<any>>();

  @Output()
  deletedQuoteListChange: EventEmitter<Array<number>> = new EventEmitter<Array<number>>();

  bsModalRef: BsModalRef;

  constructor(private modalService: BsModalService, private quoteApi: QuoteApiService, private pageService: PageService, private util: UtilService) {
  }

  change() {
    this.quoteListChange.emit(this.quoteList);
    this.deletedQuoteListChange.emit(this.deletedQuoteList);
  }

  deleteQuote(id, index) {
    return () => {
      id != 0 && this.deletedQuoteList.push(id);
      this.quoteList.splice(index, 1);
      this.change()
    }
  }

  emailPdf(quote: Quote) {
    this.bsModalRef = this.modalService.show(EmailPromptComponent, {
      initialState: {
        command: 'email',
        company: this.company,
        quoteOptions: this.quoteOptions,
        quote
      }
    });
  }

  deleteSignedQuote(quote: Quote) {
    this.pageService.loading = true;
    this.quoteApi.deleteSignedQuote(quote.quoteId)
      .subscribe(res => {
        quote.fileName = ''
        this.pageService.loading = false;
      }, err => {
        console.log('err', err);
        this.pageService.loading = false;
      })
  }

  uploadQuote(quote: Quote) {
    this.bsModalRef = this.modalService.show(UploadComponent, {
      initialState: {
        id: quote.quoteId,
        nextFn: res => {
          quote.fileName = 'asdf'
        }
      }
    });
  }

  quotePdf(quote: Quote) {
    if (quote.fileName) {
      this.downloadSignedQuote(quote.quoteId)
    } else {
      this.quoteApi.quotePdf('view', this.company, quote.quoteId, null)
        .subscribe(data => {
          window.open(data['url'], "_blank");
        }, err => {
          console.log('error', err);
        })
    }
  }

  downloadSignedQuote(id: number) {
    window.open(this.quoteApi.downloadSignedQuote(id))
  }

  editQuote(quote: Quote) {
    let accountId = this.module === 'prospect' ? quote.prospectId : quote.customer;

    this.util.navigateTo('quote', quote.quoteId, {
      accountId,
      company: quote.company,
      module: this.module
    })
  }

  signInPerson(quote: Quote) {
    this.bsModalRef = this.modalService.show(SignInPersonComponent, {
      initialState: {
        quote,
        account: this.account.serviceNameDisplay
      }
    });
  }

  isReadOnly(quote: Quote): boolean {
    return quote.status == '9' || this.readOnly
  }

  hasSignedQuote(quote: Quote): boolean {
    return quote.fileName.length > 0;
  }
}
