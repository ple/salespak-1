import {Component} from '@angular/core';
import {PageService} from "../../../service/page.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  constructor(public pageService: PageService) {}
}
