import { Component, OnInit } from '@angular/core';
import {SearchView} from "../../../model/search-view";
import {SearchViewService} from "../../../service/search-view.service";
import {PageService} from "../../../service/page.service";

@Component({
  templateUrl: './search-view.component.html',
  styleUrls: ['./search-view.component.scss']
})
export class SearchViewComponent {

  sv: SearchView = new SearchView();

  constructor(private searchView: SearchViewService, private pageService: PageService) {
    if (SearchView.searchType().hasOwnProperty(pageService.settings.defaultSearchType)) {
      this.sv.sf.searchType = pageService.settings.defaultSearchType;
    }

    setTimeout(() => {
      this.searchView.viewService.pageService.loading = false;
    });
  }
}
