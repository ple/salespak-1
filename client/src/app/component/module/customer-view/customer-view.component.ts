import {Component, SimpleChanges, ViewChild} from '@angular/core';
import {Transition} from "@uirouter/core";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {PageService} from "../../../service/page.service";
import {CustomerViewService} from "../../../service/customer-view.service";
import {CustomerView} from "../../../model/customer-view";
import {NoteFieldComponent} from "../../modal/note-field/note-field.component";
import {MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {SoftpakEvent} from "../../../model/softpak-event";
import {EventEditComponent} from "../../modal/event-edit/event-edit.component";
import {Note} from "../../../model/note";
import {Quote} from "../../../model/quote";

@Component({
  templateUrl: './customer-view.component.html',
  styleUrls: ['./customer-view.component.scss']
})
export class CustomerViewComponent {

  view: CustomerView;
  bsModalRef: BsModalRef;
  fromQuote: boolean = false;
  @ViewChild(MatSort) sort: MatSort;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  dataSource: MatTableDataSource<Array<any>>;

  displayedColumns: Array<string> = ['date', 'day', 'arrive', 'depart', 'servDelayDescription', 'weight', 'bin', 'size',
    'load', 'route', 'vehicle', 'driver', 'workOrder', 'remark'];

  constructor(private trans: Transition, public pageService: PageService, private customerViewService: CustomerViewService, private modalService: BsModalService) {
    let {company, id, fromQuote} = this.trans.params();

    this.fromQuote = fromQuote;
    this.view = new CustomerView();
    this.view.id = parseInt(id);
    this.view.company = decodeURIComponent(company);

    this.customerViewService.getCustomerView(
      id,
      this.view.company,
      ([customer, events, eventOptions, quoteList]) => {
        this.view.customer = Object.assign(this.view.customer, customer['customer']['client']);
        this.view.contacts = customer['customer']['contactData'];
        this.view.customerReport = customer['customerReport'];
        this.view.monthlyCharges = customer['monthlyCharges']['chargeData'];
        this.view.pricing = customer['pricing'];
        this.view.activity = customer['activity'];
        this.view.customer.notesList = customer['notesList']['noteList'];
        this.view.references = customer['references'];
        this.view.customer.notesList = customer['notesList']['notesList'];
        this.view.setEvents(events.events, eventOptions, pageService.userName);
        this.view.hasAddNoteAccess = customer['notesList']['hasAddNoteAccess'];
        this.updateDataSource(this.view.activity);
        this.view.quotes = quoteList.quote.map(quote => Object.assign(new Quote(), quote));
        this.view.setQuoteOptions(quoteList.fields);
      },
      err => {
        console.log('error: getCustomerView')
      }
    );
  }

  deleteQuotes() {
    this.customerViewService.deleteQuotes(this.view.deletedQuoteList,
      () => {
        this.view.deletedQuoteList = [];
        this.customerViewService.viewService.toast.success("Success - Quotes deleted");
      },
      err => {
        this.customerViewService.viewService.toast.error("There was an error deleting quotes.");
      })
  }

  addQuote() {
    this.customerViewService.viewService.util.navigateTo(
      'quote',
      null,
      {
        mode: 'create',
        company: this.view.company,
        accountId: this.view.id,
        module: 'customer'
      }
    )
  }

  addNote() {
    this.bsModalRef = this.modalService.show(NoteFieldComponent, {
      initialState: {
        saveFn: (note: Note) => {
          note.noteCode = 'SLSPK ';
          this.customerViewService.addCustomerNote(this.view.company, this.view.id, note,
            res => {
              this.view.customer.notesList = res.notesList.notesList
            },
            err => {}
          )
        }
      }
    });
  }

  addEvent() {
    let event = new SoftpakEvent(),
      {id, company} = this.trans.params();
    event.customer = parseInt(id);
    event.eventUser = this.view.eventOptions['user'];
    event.ownerType = 'CUSTOMER';
    event.company = decodeURIComponent(company);

    this.bsModalRef = this.modalService.show(EventEditComponent, {
      class: "modal-lg",
      initialState: {
        event,
        options: this.view.eventOptions,
        saveFn: this.saveEvent
      }
    });
  }

  saveEvent = event => {
    this.customerViewService.saveEvent(
      this.view.id,
      event,
      data => {
        this.view.events = data.events
          .filter(e =>  e.customer == this.view.id)
          .map(e => Object.assign(new SoftpakEvent(), e));
      },
      err => {}
    )
  };

  deleteEvent = event => {
    this.customerViewService.deleteEvent(
      this.view,
      event,
      data => {
        this.view.events = data.events
          .filter(e =>  e.customer == this.view.id)
          .map(e => Object.assign(new SoftpakEvent(), e));
      },
      err => {}
    )
  };

  updateDataSource(data) {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string = '') {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
