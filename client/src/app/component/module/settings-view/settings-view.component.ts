import { Component } from '@angular/core';
import {SettingsViewService} from "../../../service/settings-view.service";
import {PageService} from "../../../service/page.service";
import {UtilService} from "../../../service/util.service";
import {SearchView} from "../../../model/search-view";

@Component({
  templateUrl: './settings-view.component.html',
  styleUrls: ['./settings-view.component.scss']
})
export class SettingsViewComponent {

  daysOfTheWeek: Object = this.util.daysOfTheWeek();
  timeOfDay24: Object = this.util.timeOfDay24();
  searchType = SearchView.searchType();

  constructor(private settingsService: SettingsViewService, public pageService: PageService, private util: UtilService) {
    pageService.loading = false;
  }

  save() {
    this.settingsService.save(
      this.pageService.settings,
      () => {},
      () => {}
    )
  };

}
