import {Component} from '@angular/core';
import {Transition} from "@uirouter/core";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {QuoteView} from "../../../model/quote-view";
import {QuoteViewService} from "../../../service/quote-view.service";
import {ProspectViewService} from "../../../service/prospect-view.service";
import {QuoteServComponent} from "../../modal/quote-service/quote-serv.component";
import {QuoteService} from "../../../model/quote-service";
import {NoteFieldComponent} from "../../modal/note-field/note-field.component";
import {UtilService} from "../../../service/util.service";
import {PageService} from "../../../service/page.service";
import {TextBoxComponent} from "../../modal/text-box/text-box.component";

@Component({
  templateUrl: './quote-view.component.html',
  styleUrls: ['./quote-view.component.scss']
})
export class QuoteViewComponent {

  view: QuoteView;
  bsModalRef: BsModalRef;
  canUpdateServiceFlag = false;

  constructor(protected trans: Transition, public pageService: PageService, protected quoteViewService: QuoteViewService, protected prospectViewService: ProspectViewService, protected modalService: BsModalService, protected util: UtilService) {
    let {id, module, company, accountId} = trans.params();

    company = decodeURIComponent(company);

    this.view = new QuoteView();
    this.view.module = module;
    this.view.accountId = accountId;
    this.view.id = parseInt(id) || 0;

    if (this.view.module === 'prospect') {
      if (this.view.id)
        this.getQuote(id, accountId);
      else
        this.newQuote(company, accountId);
    } else {
      if (this.view.id)
        this.getCustomerQuote(id, accountId, company);
      else
        this.newCustomerQuote(company, accountId);
    }
  }

  getQuote(quoteId: number, prospectId: number) {
    this.quoteViewService.getQuoteView(
      quoteId,
      prospectId,
      ([quote, prospect]) => {
        this.view.quote = Object.assign(this.view.quote, quote.quote);
        this.canUpdateServiceFlag = ((!(this.view.quote.status === "9" || this.view.quote.status === "8")));
        this.view.setOptions(quote.fields);
        this.view.setPriceIncreaseYearOptions();
        this.view.quote.readOnly = this.view.quote.fileName.length > 0 || this.view.quote.status == '9'; //closed;

        this.view.prospect = Object.assign(this.view.prospect, prospect.prospect.client);

        if (this.view.prospect.readOnly)
          this.view.quote.readOnly = this.view.prospect.readOnly
      },
      err => {
        //todo
      }
    )
  }

  getCustomerQuote(quoteId: number, accountId: number, company: string) {
    this.quoteViewService.getCustomerQuoteView(
      quoteId,
      accountId,
      company,
      ([quote, customer]) => {
        this.view.quote = Object.assign(this.view.quote, quote.quote);
        this.canUpdateServiceFlag = !(this.view.quote.status === "9" || this.view.quote.status === "8");
        this.view.setOptions(quote.fields);
        this.view.setPriceIncreaseYearOptions();
        this.view.quote.readOnly = this.view.quote.fileName.length > 0 || this.view.quote.status == '9'; //closed;

        this.view.customer = Object.assign(this.view.customer, customer['customer']['client']);

        console.log('this.view.quote.status', this.view.quote.status)

        // if (this.view.prospect.readOnly)
        //   this.view.quote.readOnly = this.view.prospect.readOnly
      },
      err => {
        //todo
      }
    )
  }

  newQuote(company: string, prospectId: number) {
    this.quoteViewService.newQuote(
      company,
      prospectId,
      ([fields, prospect]) => {
        this.view.setOptions(fields.fields);
        this.view.quote.company = company;
        this.view.quote.prospectId = prospectId;
        this.view.prospect = Object.assign(this.view.prospect, prospect.prospect.client);
        if (this.view.prospect.readOnly)
          this.view.quote.readOnly = this.view.prospect.readOnly
      },
      err => {

      }
    )
  }

  newCustomerQuote(company: string, customerId: number) {
    this.quoteViewService.newQuoteCustomer(
      company,
      customerId,
      ([fields, customer, oldServices]) => {
        this.view.setOptions(fields.fields);
        this.view.quote.company = company;
        this.view.quote.customer = customerId;
        this.view.customer = Object.assign(this.view.customer, customer.customer.client);
        this.canUpdateServiceFlag = true;
        this.view.quote.oldServices = oldServices.results.map(r => {
          let qs = Object.assign(new QuoteService(), r);

          qs.monday = r.schedule.includes('M');
          qs.tuesday = r.schedule.includes('T');
          qs.wednesday = r.schedule.includes('W');
          qs.thursday = r.schedule.includes('H');
          qs.friday = r.schedule.includes('F');
          qs.saturday = r.schedule.includes('S');
          qs.sunday = r.schedule.includes('U');
          qs.serviceType = 'OLD';

          return qs;
        });
      },
      err => {
        console.log('QuoteViewComponent.newCustomerQuote', err)
      }
    )
  }

  addService() {
    this.bsModalRef = this.modalService.show(QuoteServComponent, {
      initialState: {
        newServices: this.view.quote.newServices,
        options: this.view.options,
        saveFn: qs => {
          this.view.quote.newServices.push(qs)
        }
      }
    });
  }

  editService(service) {
    this.bsModalRef = this.modalService.show(QuoteServComponent, {
      initialState: {
        options: this.view.options,
        qs: Object.assign(new QuoteService(), service),
        saveFn: (qs: QuoteService) => {
          this.view.quote.newServices = this.view.quote.newServices.map((ns: QuoteService) => {
            if (ns.lineNumber == qs.lineNumber)
              return qs;
            return ns
          });
        }
      }
    });
  }

  editOldService(service: QuoteService) {
    this.bsModalRef = this.modalService.show(TextBoxComponent, {
      initialState: {
        title: 'Edit Memo',
        text: service.memo,
        saveFn: (memo: string) => {
          this.view.quote.oldServices = this.view.quote.oldServices.map((ns: QuoteService) => {
            if (ns.lineNumber == service.lineNumber)
                ns.memo = memo;
            return ns
          });
        }
      }
    });
  }

  deleteService(service, index) {
    return () => {
      service.lineNumber != 0 && this.view.quote.deletedServices.push({
        lineNumber: service.lineNumber,
        serviceType: service.serviceType || 'NEW'
      });

      if (service.serviceType === 'NEW') {
        this.view.quote.newServices.splice(index, 1);
      } else if (service.serviceType === 'OLD') {
        this.view.quote.oldServices.splice(index, 1);
      }
    }
  }

  addNote(noteList, limit) {
    this.bsModalRef = this.modalService.show(NoteFieldComponent, {
      initialState: {
        limit,
        saveFn: note => {
          this.view.quote[noteList].push(note);
          this.view.quote[noteList] = this.view.quote[noteList].slice();
        }
      }
    });
  }

  canUpdateStatus(): boolean {
    return this.view.quote.status != "9";
  }

  canEditQuote(): boolean {
    return !this.view.quote.readOnly
  }

  redirectToAccount() {
    if (this.view.module === 'prospect')
      this.util.navigateTo('prospect-edit', this.view.prospect.prospectId);
    else
      this.util.navigateTo('customer', this.view.quote.customer, {
        company: this.view.quote.company,
        fromQuote: true
      });
  }

  get serviceName() {
    if (this.view.module === 'prospect') {
      return this.view.prospect.serviceName;
    }

    return this.view.customer.serviceName;
  }
  
  get serviceAddress() {
    if (this.view.module === 'prospect') {
      return `${this.view.prospect.serviceAddress.line1} ${this.view.prospect.serviceAddress.line2} ${this.view.prospect.serviceAddress.city} ${this.view.prospect.serviceAddress.state} ${this.view.prospect.serviceAddress.postalCode}`
    }

    let sn = this.view.customer.addressList.filter(a => a.addressType === 'SERVICE')[0];
    
    if (!sn) 
      return '';
    
    return `${sn.line1} ${sn.line2} ${sn.city} ${sn.state} ${sn.postalCode}`;
  }

  get billingName() {
    if (this.view.module === 'prospect') {
      return this.view.prospect.billingName;
    }
    
    return this.view.customer.billingName;
  }

  get billingAddress() {
    if (this.view.module === 'prospect') {
      return `${this.view.prospect.billingAddress.line1} ${this.view.prospect.billingAddress.line2} ${this.view.prospect.billingAddress.city} ${this.view.prospect.billingAddress.state} ${this.view.prospect.billingAddress.postalCode}`
    }

    let sn = this.view.customer.addressList.filter(a => a.addressType === 'BILLING')[0];

    if (!sn)
      return '';

    return `${sn.line1} ${sn.line2} ${sn.city} ${sn.state} ${sn.postalCode}`;
  }

  save() {
    if (!this.canUpdateStatus() && !this.canEditQuote())
      return;

    this.quoteViewService.save(
      this.view,
      data => {
        this.quoteViewService.viewService.toast.success("Saving Successful");
        this.redirectToAccount();
      }, err => {
        this.quoteViewService.viewService.toast.error("Saving Unsuccessful")
      }
    )
  }
}
