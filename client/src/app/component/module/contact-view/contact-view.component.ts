import {Component, OnInit, Optional} from '@angular/core';
import {ContactView} from "../../../model/contact-view";
import {ContactViewService} from "../../../service/contact-view.service";
import {Transition} from '@uirouter/core';
import {PageService} from "../../../service/page.service";
import {NoteFieldComponent} from "../../modal/note-field/note-field.component";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {ContactReference} from "../../../model/contact-reference";
import {Phone} from "../../../model/phone";
import {Address} from "../../../model/address";
import {UtilService} from "../../../service/util.service";
import {SearchView} from "../../../model/search-view";
import {Contact} from "../../../model/contact";

@Component({
  selector: 'app-contact',
  templateUrl: './contact-view.component.html',
  styleUrls: ['./contact-view.component.scss']
})
export class ContactViewComponent implements OnInit{

  view: ContactView;
  contact: Contact;
  saveFn: Function;

  constructor(private contactService: ContactViewService, @Optional() private trans: Transition, public pageService: PageService, private modalService: BsModalService, private util: UtilService, @Optional() public bsModalRef: BsModalRef) {
  }

  ngOnInit(){
    let id = this.trans && this.trans.params().id || this.contact && this.contact.contactId || 0;
    this.view = new ContactView();

    setTimeout(() => {
      this.view.id = id;
      id ? this.getViewObj(id) : this.getNewViewObj()
    })
  }

  isModal(): boolean {
    return Boolean(this.contact);
  }

  getViewObj(id: number) {
    this.contactService.getContactView(
      id,
      ([viewObj]) => {
        this.view.contact = Object.assign(this.view.contact, viewObj.contact.client);
        this.view.setOptions(viewObj.options, this.view.options);
        this.view.references = viewObj.references.map(r => Object.assign(new ContactReference(), r));
      },
      err => {}
    )
  }

  getNewViewObj() {
    this.contactService.getContactOptions(
      (options) => {
        this.view.setOptions(options.options, this.view.options);
      },
      err => {}
    )
  }

  addNote() {
    this.bsModalRef = this.modalService.show(NoteFieldComponent, {
      initialState: {
        saveFn: note => {
          this.view.contact.notesList.push(note);
          this.view.contact.notesList = this.view.contact.notesList.slice();
        }
      }
    });
  }

  addPhone() {
    this.view.contact.phoneList.push(new Phone());
  }

  addAddress() {
    this.view.contact.addressList.push(new Address());
  }

  save() {
    this.contactService.save(
      this.view,
      data => {
        this.contactService.viewService.toast.success("Contact Save Successful");

        if (this.isModal()) {
          this.contact = Object.assign(this.contact, this.view.contact);
          this.contact.contactId = parseInt(data.id);
          this.util.isFunction(this.saveFn) && this.saveFn(this.contact);
          this.bsModalRef.hide();
        } else if (!this.view.id) {
          this.util.navigateTo('contact-edit', data.id);
        }
      },
      err => {
        this.contactService.viewService.toast.error("Contact Save Not Successful");
      }
    );
  }

  close() {
    this.bsModalRef.hide();
  }

}
