import {AfterContentInit, AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {SoftpakEvent} from "../../../model/softpak-event";
import {EventEditComponent} from "../../modal/event-edit/event-edit.component";
import {CalendarComponent} from 'ng-fullcalendar';
import {CalendarFilterComponent} from "../../modal/calendar-filter/calendar-filter.component";
import {EventService} from "../../../service/event.service";
import dateFormat from '../../../model/./date-format';
import {EventView} from '../../../model/event-view';
import {EventViewService} from "../../../service/event-view.service";
import {PageService} from "../../../service/page.service";
import * as moment from "moment";
import {FilterFieldsService} from "../../../service/filter-fields.service";

@Component({
  templateUrl: './event-view.component.html',
  styleUrls: ['./event-view.component.scss']
})
export class EventViewComponent implements OnInit{

  view: EventView;
  calendarFilterModal: BsModalRef;
  editNoteModalRef: BsModalRef;
  filter: FilterFieldsService = new FilterFieldsService(this.pageService);
  calendarOptions;

  @ViewChild(CalendarComponent) ucCalendar: CalendarComponent;

  constructor(private eventViewService: EventViewService, private eventService: EventService, private modalService: BsModalService,
              public pageService: PageService) {
    this.view = new EventView();
  }

  getEvents() {
    this.eventViewService.getFilteredEventsView(
      this.filter,
      data => {
        this.loadEventData(data);
      }, err => {}
    )
  }

  calendarFilter() {
    if(this.eventService.filter){
      this.filter = this.eventService.filter;
    }else{
      this.filter = new FilterFieldsService(this.pageService);
      this.filter.userId = this.pageService.userName;
    }
    this.eventService.filter = this.filter;
    this.calendarFilterModal = this.modalService.show(CalendarFilterComponent, {
      initialState: {
        events: this.view.allEvents,
        options: this.view.options,
        filter: this.filter,
        evs: this.eventViewService,
        view: this.view,
        ucCalendar: this.ucCalendar
        // filterFn: filteredEvents => {
        //   this.ucCalendar.renderEvents(filteredEvents);
        // }
      },
      class: 'modal-lg'
    });
  }

  clearFilters() {
    this.filter = new FilterFieldsService(this.pageService);
    this.eventService.filter = new FilterFieldsService(this.pageService);
    if(this.calendarFilterModal != undefined){
      this.calendarFilterModal.content.initialState  = {
        filter: this.filter
      };
    }
    this.getEvents();
  }

  editEventModal(event: SoftpakEvent = new SoftpakEvent()) {
    if (!event.eventUser) event.eventUser = this.view.options['user'];

    this.editNoteModalRef = this.modalService.show(EventEditComponent, {
      class: "modal-lg",
      initialState: {
        event,
        options: this.view.options,
        saveFn: this.save,
        deleteFn: this.delete
      }
    })
  }

  save = event => {
    this.eventViewService.save(
      event,
      data => {
        this.eventViewService.getFilteredEventsView(
          this.filter,
          data => {
            this.loadEventData(data);
          }, err => {}
        )
      },
      err => {}
    );
  };

  loadEventData([list, options]) {
    this.view.setEvents(list['events']);
    this.view.setOptions(options);
    this.ucCalendar.renderEvents(this.view.calEvents);
  }

  delete = event => {
    this.eventViewService.delete(
      event,
      data => {
        // this.view.setEvents(data['events']);
        // this.ucCalendar.renderEvents(this.view.calEvents);
      },
      err => {}
    );
    this.eventViewService.getFilteredEventsView(
      this.filter,
      data => {
        this.loadEventData(data);
      }, err => {}
    )
  };

  clickEvent(event) {
    let e = this.eventService.getEventById(this.view.allEvents, event.event.id)
    this.editEventModal(new SoftpakEvent(e))
  }

  addEventType(type) {
    let event = new SoftpakEvent();
    event.type = type;
    this.editEventModal(event);
  }

  dropEvent(_event) {
    let e = this.eventService.getEventById(this.view.allEvents, _event.event.id),
      event = new SoftpakEvent(e);

    event.startTimestamp = _event.event.start.format(dateFormat.timeStampFormat);
    event.endTimestamp = _event.event.end.format(dateFormat.timeStampFormat);

    if (!event.eventUser)
      event.eventUser = this.view.options['user'];

    this.save(event)
  }

  ngOnInit() {
    this.calendarOptions = {
      editable: true,
      eventLimit: false,
      events: [],
      firstDay: this.pageService.settings.calendarFirstDay,
      weekends: this.pageService.settings.showWeekends,
      scrollTime: moment(this.pageService.settings.calendarFirstHour,'HH').format('HH:00:00'),
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listMonth'
      },
      defaultView: 'agendaWeek'
    };
    setTimeout(() => {
      this.getEvents();
    });
  }

  dayClick(ev){
    let event = new SoftpakEvent();
    event.isAllDay = false;
    event._startTimestamp = moment(ev.detail.date._d).add(7,"hour").toString();
    event._endTimestamp = moment(ev.detail.date._d).add(8,"hour").toString();
    this.editEventModal(event);
  }
}
