import {Component} from '@angular/core';
import {Transition} from "@uirouter/core";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {AddContactComponent} from "../../modal/add-contact/add-contact.component";
import {PageService} from "../../../service/page.service";
import {SoftpakEvent} from "../../../model/softpak-event";
import {EventEditComponent} from "../../modal/event-edit/event-edit.component";
import {ProspectView} from "../../../model/prospect-view";
import {ProspectViewService} from "../../../service/prospect-view.service";
import {Phone} from "../../../model/phone";
import {NoteFieldComponent} from "../../modal/note-field/note-field.component";
import {SearchView} from "../../../model/search-view";
import {SearchField} from "../../../model/search-field";
import {Contact} from "../../../model/contact";
import {UtilService} from "../../../service/util.service";
import {ConfirmationPromptComponent} from "../../modal/confirmation-prompt/confirmation-prompt.component";
import {ContactViewComponent} from "../contact-view/contact-view.component";
import {Quote} from "../../../model/quote";

@Component({
  templateUrl: './prospect-view.component.html',
  styleUrls: ['./prospect-view.component.scss']
})
export class ProspectViewComponent {

  view: ProspectView;
  bsModalRef: BsModalRef;

  constructor(public prospectService: ProspectViewService, private trans: Transition, public pageService: PageService, private modalService: BsModalService, private util: UtilService) {

    const {id} = this.trans.params();

    this.view = new ProspectView();
    this.view.id = id || 0;

    if (this.view.id)
      this.getProspect(id);
    else
      this.newProspect()
  }

  getProspect(id: number) {
    this.prospectService.getProspectView(
      id,
      ([prospectObj, options, events, eventOptions, quotes]) => {

        this.view.prospect = Object.assign(this.view.prospect, prospectObj.prospect.client);
        this.view.contacts = prospectObj.prospect.contactData.map(c => Object.assign(new Contact(), c));
        this.view.setOptions(options.options, this.view.options);
        this.view.setEvents(events.events, eventOptions);
        this.view.quotes = quotes.quote.map(quote => Object.assign(new Quote(), quote));
        this.view.setQuoteOptions(quotes.fields);

        this.resetPageConditions();
        this.view.oriProspectHash = this.util.queryString(this.view.prospect);
      },
      () => {
        //todo handle err.
        console.log('error: getProspect')
      }
    )
  }

  isChanged() {
    return this.view.oriProspectHash != this.util.queryString(this.view.prospect)
  }

  resetPageConditions() {
    this.view.canEditCompany = this.view.quotes.length == 0;
    this.view.canAddQuotes = this.trans.params().mode == 'edit' && this.view.prospect.company.trim() != '' && !this.view.prospect.readOnly;
  }

  newProspect() {
    this.prospectService.newProspect(
      ([options]) => {
        this.view.setOptions(options.options, this.view.options);
        this.view.prospect.salesId = this.pageService.userName;
      }, () => {
        //todo handle err.
        console.log('error: newProspect')
      }
    )
  }

  searchContact() {
    const sv: SearchView = new SearchView(new SearchField('CONTACTS'));
    sv.searchTypeDisplay = false;
    sv.actionTemplate = 'prospectAdd';
    sv.customCb = (c: Contact) => {
      if (this.view.prospect.contactList.includes(c.contactId)) {
        this.prospectService.viewService.toast.warning("Contact is already attached to Prospect")
        return;
      }
      this.view.contacts.push(c);
      this.view.prospect.contactList.push(c.contactId);
      this.serviceContactCheck();
      this.bsModalRef.hide();
    };

    this.bsModalRef = this.modalService.show(AddContactComponent, {
      initialState: {
        sv
      }
    });
  }

  addContact() {
    this.bsModalRef = this.modalService.show(ContactViewComponent, {
      class: 'modal-lg',
      initialState: {
        contact: new Contact(),
        saveFn: contact => {
          if (!this.util.isNull(this.bsModalRef.content.contact) && this.bsModalRef.content.contact.contactId) {
            this.view.contacts.push(contact);
            this.view.prospect.contactList.push(contact.contactId);
            this.view.prospect.contactList = this.view.prospect.contactList.slice();
          }
        }
      }
    });
  }

  addPhone() {
    this.view.prospect.phoneList.push(new Phone());
  }

  addNote() {
    this.bsModalRef = this.modalService.show(NoteFieldComponent, {
      initialState: {
        saveFn: note => {
          this.view.prospect.notesList.push(note);
          this.view.prospect.notesList = this.view.prospect.notesList.slice();
        }
      }
    });
  }

  addEvent() {
    let event = new SoftpakEvent();
    event.prospectId = parseInt(this.trans.params().id);
    event.eventUser = this.view.eventOptions['user'];
    event.ownerType = 'PROSPECT';

    this.bsModalRef = this.modalService.show(EventEditComponent, {
      class: "modal-lg",
      initialState: {
        event,
        options: this.view.eventOptions,
        saveFn: this.saveEvent
      }
    });
  }

  addQuote() {
    let addQuoteFn = () => {
      this.prospectService.viewService.util.navigateTo(
        'quote',
        null,
        {
          mode: 'create',
          company: this.view.prospect.company,
          accountId: this.view.prospect.prospectId,
          module: 'prospect'
        }
      )
    };

    if (this.isChanged())
      this.bsModalRef = this.modalService.show(ConfirmationPromptComponent, {
        initialState: {
          confirmationPrompt: 'There are unsaved changes!',
          confirmLabel: 'Save and continue?',
          cancelLabel: 'Go back.',
          confirmAction: () => {
            this.save(() => {
              this.bsModalRef.hide();
              addQuoteFn()
            });
          }
        }
      });
    else
      addQuoteFn()
  }

  canAddEvent(): boolean {
    return this.trans.params().mode == 'edit' && !this.view.prospect.readOnly;
  }

  navigateToCustomer() {
    this.util.navigateTo('customer', this.view.prospect.customer, {company: this.view.prospect.company})
  }

  canDeletePhone: Function = () => {
    return this.view.prospect.phoneList.length > 1 && !this.view.prospect.readOnly
  };

  saveEvent = event => {
    this.prospectService.saveEvent(
      this.view.id,
      event,
      data => {
        this.view.events = data.events.map(e => Object.assign(new SoftpakEvent(), e));
      },
      err => {
      }
    )
  };

  deleteEvent = event => {
    this.prospectService.deleteEvent(
      this.view.id,
      event,
      data => {
        this.view.events = data.events.map(e => Object.assign(new SoftpakEvent(), e));
      },
      err => {
      }
    )
  };

  copyAddressFromService() {
    let id = this.view.prospect.billingAddress.addressId;
    this.view.prospect.billingAddress = Object.assign(this.view.prospect.billingAddress, this.view.prospect.serviceAddress);
    this.view.prospect.billingAddress.addressId = id;
    this.view.prospect.billingAddress.addressType = 'BILLING';
  }

  serviceContactCheck() {
    if (this.view.contacts.length == 1) {
      this.view.prospect.serviceContact = this.view.contacts[0].contactId;
    }
  }

  save(cb = () => {
  }) {
    if (this.view.prospect.readOnly)
      return;
    this.serviceContactCheck();

    this.prospectService.save(
      this.view,
      ([data]) => {
        this.prospectService.viewService.toast.success("Saving Successful")

        this.resetPageConditions();

        if (!this.view.prospect.prospectId)
          this.util.navigateTo('prospect-edit', data.id)

        cb()
      }, err => {
        this.prospectService.viewService.toast.error("Saving Unsuccessful")
      }
    )
  }

}
